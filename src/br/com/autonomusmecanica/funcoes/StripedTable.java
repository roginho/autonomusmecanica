package br.com.autonomusmecanica.funcoes;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class StripedTable implements TableCellRenderer {

    int coluna = 0;
    public static final DefaultTableCellRenderer DEFAULT_RENDERER = new DefaultTableCellRenderer();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        ((JComponent) renderer).setOpaque(true);

        Color foreground, background;
//        if (hasFocus) { //seleciona só a celula
//            System.out.println(column);
//           foreground = Color.white;
//            background = Color.BLUE.darker();
//        }
        if (isSelected) { //seleciona a linha inteira
            foreground = Color.white;
            background = Color.BLUE.darker();

        } else if (row % 2 == 0) {
            foreground = Color.BLACK;
            background = Color.decode("#FFFAFA");

        } else {
            foreground = Color.BLACK;
            background = Color.decode("#FFDEAD");

        }
        renderer.setBackground(background);
        renderer.setForeground(foreground);
        table.setRowHeight(20);
        return renderer;
    }

    public int getColuna() {
        return coluna;

    }

    public int alinhaDireita(int coluna) {

        return coluna;

    }
}
