/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.funcoes;

import javax.swing.JOptionPane;

/**
 *
 * @author roger
 */
public enum OS {

    WINDOWS("windows"), LINUX("linux");

    public static final OS OS;

//    private static DGLog logger = new DGLog(br.com.digicon.dnserver.util.OS.class);
    private String name;

    static {
        String tempOS = System.getProperty("os.name");

        if (tempOS == null) {
            tempOS = "N/A";
        } else {
            tempOS = tempOS.trim().toLowerCase();
        }

        if (tempOS.indexOf(WINDOWS.name) != -1) {
            OS = WINDOWS;
        } else if (tempOS.indexOf(LINUX.name) != -1) {
            OS = LINUX;
        } else {
            OS = null;
            JOptionPane.showMessageDialog(null, "O sistema operacional %s não é compatível a aplicação", "Erro ", JOptionPane.ERROR_MESSAGE);
        }
    }

    OS(String os) {
        name = os;
    }

    public String toString() {
        return this.name;
    }

}
