package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Cidade;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class TableModelCidade extends AbstractTableModel {

    private String[] colunas = new String[]{"Nome", "UF", "Ibge", "Alterar", "Excluir"};
    private List<Cidade> cidades;

    public TableModelCidade() {
        cidades = new ArrayList<Cidade>();
    }

    public TableModelCidade(List<Cidade> lista) {
        this();
        cidades.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case -1:
                return Object.class;
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return ImageIcon.class;
            case 4:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return cidades.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Cidade o = cidades.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getNome();
            case 1:
                return o.getEstado().getUf();
            case 2:
                return o.getIbge();
            case 3:
                return "Alterar";
            case 4:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Cidade o = cidades.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Cidade) (valor); // mudamos objeto
                break;
            case 0:
                o.setNome(valor.toString()); // mudamos o nome
                break;
            case 1:
                o.getEstado().setUf(valor.toString());  // mudamos o estado
                break;
            case 2:
                o.setIbge(Integer.valueOf(valor.toString()));  // mudamos o estado
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Cidade o) {
        cidades.add(o);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(cidades.size() - 1, cidades.size() - 1);
    }

    public void remove(int linha) {
        cidades.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Cidade o) {
        return cidades.indexOf(o);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Cidade> lista) {
        int i = cidades.size();
        cidades.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = cidades.size();
        cidades.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(cidades, new Comparator<Cidade>() {
            @Override
            public int compare(Cidade o1, Cidade o2) {
                return o1.getIdCidade() - o2.getIdCidade();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(cidades, new Comparator<Cidade>() {

            @Override
            public int compare(Cidade o1, Cidade o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }

}
