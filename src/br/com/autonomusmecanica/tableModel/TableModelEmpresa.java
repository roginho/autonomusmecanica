package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Empresa;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;

public class TableModelEmpresa extends AbstractTableModel {

    private List<Empresa> empresas;
    private List<Pessoa> pessoas;
    private String[] colunas = new String[]{"Empresa", "Alterar"};

    public TableModelEmpresa() {
        empresas = new ArrayList<Empresa>();
    }

    public TableModelEmpresa(List<Empresa> lista) {
        this();
        empresas.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return empresas.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Empresa e = empresas.get(linha);
        switch (coluna) {
            case -1:
                return e;
            case 0:
                return e.getPessoa().getNome();
            case 1:
                return "Alterar";
            default:

                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Empresa e = empresas.get(linha);
        Pessoa p = new Pessoa();
        switch (coluna) {
            case -1:
                e = (Empresa) valor; // mudamos objeto
                break;
            case 0:
                p.setNome(valor.toString());
                e.setPessoa(p); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Empresa u) {
        empresas.add(u);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(empresas.size() - 1, empresas.size() - 1);
    }

    public void remove(int linha) {
        empresas.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Empresa u) {
        return empresas.indexOf(u);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Empresa> lista) {
        int i = empresas.size();
        empresas.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = empresas.size();
        empresas.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(empresas, new Comparator<Empresa>() {
            @Override
            public int compare(Empresa o1, Empresa o2) {
                return o1.getIdEmpresa() - o2.getIdEmpresa();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(empresas, new Comparator<Empresa>() {

            @Override
            public int compare(Empresa o1, Empresa o2) {
                return o1.getPessoa().getNome().compareTo(o2.getPessoa().getNome());
            }
        });
        fireTableDataChanged();
    }

}
