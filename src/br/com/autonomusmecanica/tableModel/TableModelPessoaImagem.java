package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.PessoaImagem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelPessoaImagem extends AbstractTableModel {

    private List<PessoaImagem> pessoasImagens;

    public TableModelPessoaImagem() {
        pessoasImagens = new ArrayList<>();
    }

    public TableModelPessoaImagem(List<PessoaImagem> lista) {
        this();
        pessoasImagens.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == 0) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int coluna
    ) {
        switch (coluna) {
            case 0:
                return "ID";
            case 1:
                return "Nome";
            case 2:
                return "Incluir";
            case 3:
                return "Alterar";
            case 4:
                return "Remover";
            default:
                return "";
        }
    }

    @Override
    public int getRowCount() {
        return pessoasImagens.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        PessoaImagem e = pessoasImagens.get(linha);
        switch (coluna) {
            case 0:
                return e;
            case 1:
                return e.getNome();
            case 2:
                return "Adicionar";
            case 3:
                return "Alterar";
            case 4:
                return "Remover";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        PessoaImagem e = pessoasImagens.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case 0:
                e.setIdPessoaImagem(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 1:
                e.setNome(valor.toString()); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(PessoaImagem e) {
        pessoasImagens.add(e);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(pessoasImagens.size() - 1, pessoasImagens.size() - 1);
    }
   

    public void remove(int linha) {
        pessoasImagens.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(PessoaImagem e) {
        return pessoasImagens.indexOf(e);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<PessoaImagem> lista) {
        int i = pessoasImagens.size();
        pessoasImagens.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = pessoasImagens.size();
        pessoasImagens.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(pessoasImagens, new Comparator<PessoaImagem>() {
            @Override
            public int compare(PessoaImagem o1, PessoaImagem o2) {
                return o1.getIdPessoaImagem() - o2.getIdPessoaImagem();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(pessoasImagens, new Comparator<PessoaImagem>() {

            @Override
            public int compare(PessoaImagem o1, PessoaImagem o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }

}
