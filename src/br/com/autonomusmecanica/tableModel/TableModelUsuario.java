package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Usuario;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;

public class TableModelUsuario extends AbstractTableModel {

    // Array com os nomes das colunas.
    private String[] colunas = new String[]{"Login", "Tipo", "Status", "Alterar"};
    private List<Usuario> usuarios;

    public TableModelUsuario() {
        usuarios = new ArrayList<Usuario>();
    }

    public TableModelUsuario(List<Usuario> lista) {
        this();
        usuarios.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case -1:
                return Object.class;
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return usuarios.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Usuario u = usuarios.get(linha);
        switch (coluna) {
            case -1:
                return u;
            case 0:
                return u.getLogin();
            case 1:
                if (u.getAdministrador() == 'a') {
                    return "Administrador";
                } else {
                    return "Usuário";
                }
            case 2:
                if (u.getPessoa().getStatus() == 'a') {
                    return "Ativo";
                } else {
                    return "Inativo";
                }
            case 3:
                return "alterar";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Usuario u = usuarios.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                u = (Usuario) valor;
                break;
            case 0:
                u.setLogin(valor.toString()); // mudamos o nome
                break;
            case 1:
                u.setAdministrador(valor.toString().charAt(0));  // mudamos o estado
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Usuario u) {
        usuarios.add(u);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(usuarios.size() - 1, usuarios.size() - 1);
    }

    public void remove(int linha) {
        usuarios.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Usuario u) {
        return usuarios.indexOf(u);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Usuario> lista) {
        int i = usuarios.size();
        usuarios.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = usuarios.size();
        usuarios.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(usuarios, new Comparator<Usuario>() {
            @Override
            public int compare(Usuario o1, Usuario o2) {
                return o1.getIdUsuario() - o2.getIdUsuario();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(usuarios, new Comparator<Usuario>() {
            @Override
            public int compare(Usuario o1, Usuario o2) {
                return o1.getLogin().compareTo(o2.getLogin());
            }
        });
        fireTableDataChanged();
    }

}
