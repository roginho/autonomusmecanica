package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Email;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelEmail extends AbstractTableModel {

    // Array com os nomes das colunas.

    private String[] colunas = new String[]{"Email", "Alterar", "Excluir"};
    private List<Email> emails;

    public TableModelEmail() {
        emails = new ArrayList<>();
    }

    public TableModelEmail(List<Email> lista) {
        this();
        emails.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return emails.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Email e = emails.get(linha);
        switch (coluna) {
            case -1:
                return e;
            case 0:
                return e.getEmail();
            case 1:
                return "Alterar";
            case 2:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Email e = emails.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                e = (Email) valor; // mudamos objeto
                break;
            case 0:
                e.setEmail(valor.toString()); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Email e) {
        emails.add(e);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(emails.size() - 1, emails.size() - 1);
    }

    public void remove(int linha) {
        emails.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Email e) {
        return emails.indexOf(e);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Email> lista) {
        int i = emails.size();
        emails.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = emails.size();
        emails.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(emails, new Comparator<Email>() {
            @Override
            public int compare(Email o1, Email o2) {
                return o1.getIdEmail() - o2.getIdEmail();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorEmail() {
        Collections.sort(emails, new Comparator<Email>() {

            @Override
            public int compare(Email o1, Email o2) {
                return o1.getEmail().compareTo(o2.getEmail());
            }
        });
        fireTableDataChanged();
    }

}
