package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Veiculo;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelVeiculo extends AbstractTableModel {

    private String[] colunas = new String[]{"Placa", "Ano", "Modelo", "Cor", "Cliente", "Alterar", "Excluir"};
    private List<Veiculo> veiculos;

    public TableModelVeiculo() {
        veiculos = new ArrayList<>();
    }

    public TableModelVeiculo(List<Veiculo> lista) {
        this();
        veiculos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else if (coluna == 3) {
            return Color.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return veiculos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Veiculo o = veiculos.get(linha);
        String cliente = "";
        if (o.getCliente() != null) {
            cliente = o.getCliente().getPessoa().getNome();
        }
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getPlaca();
            case 1:
                return o.getAno();
            case 2:
                return o.getModelo().getDescricao();
            case 3:
                return o.getCor().getBuscaCor();
            case 4:
                return cliente;
            case 5:
                return "Alterar";
            case 6:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Veiculo o = veiculos.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Veiculo) valor; // mudamos objeto
                break;
            case 0:
                o.setPlaca(valor.toString());
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Veiculo t) {
        veiculos.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(veiculos.size() - 1, veiculos.size() - 1);
    }

    public void remove(int linha) {
        veiculos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Veiculo b) {
        return veiculos.indexOf(b);
    }

    public void adicionaLista(List<Veiculo> lista) {
        int i = veiculos.size();
        veiculos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = veiculos.size();
        veiculos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(veiculos, new Comparator<Veiculo>() {
            @Override
            public int compare(Veiculo o1, Veiculo o2) {
                return o1.getIdVeiculo() - o2.getIdVeiculo();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorDescricao() {
        Collections.sort(veiculos, new Comparator<Veiculo>() {

            @Override
            public int compare(Veiculo o1, Veiculo o2) {
                return o1.getPlaca().compareTo(o2.getPlaca());
            }
        });
        fireTableDataChanged();
    }
}
