package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Pais;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class TableModelPais extends AbstractTableModel {

    private String[] colunas = new String[]{"Descrição", "Sigla", "Alterar", "Excluir"};
    private List<Pais> paises;

    public TableModelPais() {
        paises = new ArrayList<Pais>();
    }

    public TableModelPais(List<Pais> lista) {
        this();
        paises.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case -1:
                return Object.class;
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return ImageIcon.class;
            case 3:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return paises.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Pais p = paises.get(linha);
        switch (coluna) {
            case -1:
                return p;
            case 0:
                return p.getNome();
            case 1:
                return p.getSigla();
            case 2:
                return "Alterar";
            case 3:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Pais p = paises.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                p = (Pais) valor; // mudamos objeto
                break;
            case 0:
                p.setNome(valor.toString()); // mudamos o nome
                break;
            case 1:
                p.setSigla(valor.toString());  // mudamos o estado
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Pais p) {
        paises.add(p);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(paises.size() - 1, paises.size() - 1);
    }

    public void remove(int linha) {
        paises.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Pais p) {
        return paises.indexOf(p);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Pais> lista) {
        int i = paises.size();
        paises.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = paises.size();
        paises.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(paises, new Comparator<Pais>() {
            @Override
            public int compare(Pais o1, Pais o2) {
                return o1.getIdPais() - o2.getIdPais();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(paises, new Comparator<Pais>() {

            @Override
            public int compare(Pais o1, Pais o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }

}
