package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Cliente;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;

public class TableModelClientes extends AbstractTableModel {

    private List<Cliente> clientes;
    private final String[] colunas = new String[]{"Clientes", "Cpf/Cnpj","Alterar"};

    public TableModelClientes() {
        clientes = new ArrayList<>();
    }

    public TableModelClientes(List<Cliente> lista) {
        this();
        clientes.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Cliente e = clientes.get(linha);
        
        switch (coluna) {
            case -1:
                return e;
            case 0:
                return e.getPessoa().getNome();
            case 1:
                return e.getPessoa().getCpfCnpj();
            case 2:
                return "Alterar";
             default:

                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Cliente e = clientes.get(linha);
        Pessoa p = new Pessoa();
        switch (coluna) {
            case -1:
                e = (Cliente) valor; // mudamos objeto
                break;
            case 0:
                p.setNome(valor.toString());
                e.setPessoa(p); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Cliente u) {
        clientes.add(u);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(clientes.size() - 1, clientes.size() - 1);
    }

    public void remove(int linha) {
        clientes.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Cliente u) {
        return clientes.indexOf(u);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Cliente> lista) {
        int i = clientes.size();
        clientes.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = clientes.size();
        clientes.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(clientes, new Comparator<Cliente>() {
            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.getIdCliente() - o2.getIdCliente();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(clientes, new Comparator<Cliente>() {

            @Override
            public int compare(Cliente o1, Cliente o2) {
                return o1.getPessoa().getNome().compareTo(o2.getPessoa().getNome());
            }
        });
        fireTableDataChanged();
    }

}
