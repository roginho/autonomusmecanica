package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.ServicoItem;
import br.com.roger.utils.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelServicoItem extends AbstractTableModel {

    private String[] colunas = new String[]{"Objeto", "Qtde", "Descrição", "Valor Unit.", "Valor Total", "Alterar", "Excluir"};
    private List<ServicoItem> servicosItens;

    public TableModelServicoItem() {
        servicosItens = new ArrayList<>();
    }

    public TableModelServicoItem(List<ServicoItem> lista) {
        this();
        servicosItens.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == 0) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return servicosItens.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        ServicoItem o = servicosItens.get(linha);
        switch (coluna) {
            case 0:
                return o;
            case 1:
                return o.getQuantidade();
            case 2:
                return o.getDescricao();
            case 3:
                return Singleton.getInstance().converteDoubleParaString(o.getValorUnitario(),2);
            case 4:
                return Singleton.getInstance().converteDoubleParaString(o.getValorTotal(),2);
            case 5:
                return "Alterar";
            case 6:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        ServicoItem o = servicosItens.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case 0:
                o.setIdServicoItem(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 1:
                o.setQuantidade(Double.parseDouble(valor.toString()));
                break;
            case 2:
                o.setDescricao(valor.toString());
                break;
            case 3:
                o.setValorUnitario(Double.parseDouble(valor.toString()));
                break;
            case 4:
                o.setValorTotal(Double.parseDouble(valor.toString()));
                break;

        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(ServicoItem t) {
        servicosItens.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(servicosItens.size() - 1, servicosItens.size() - 1);
    }

    public void remove(int linha) {
        servicosItens.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(ServicoItem b) {
        return servicosItens.indexOf(b);
    }

    public void adicionaLista(List<ServicoItem> lista) {
        int i = servicosItens.size();
        servicosItens.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = servicosItens.size();
        servicosItens.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorId() {
        Collections.sort(servicosItens, new Comparator<ServicoItem>() {
            @Override
            public int compare(ServicoItem o1, ServicoItem o2) {
                return o1.getIdServicoItem()- o2.getIdServicoItem();
            }
        });
        fireTableDataChanged();
    }
}
