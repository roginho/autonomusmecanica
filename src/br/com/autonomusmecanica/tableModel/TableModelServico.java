package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Servico;
import br.com.roger.utils.Singleton;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelServico extends AbstractTableModel {

    private String[] colunas = new String[]{"Cod", "Placa", "Ano", "Modelo", "Cor", "Cliente", "Status", "Valor", "Alterar", "Excluir"};
    private List<Servico> servicos;

    public TableModelServico() {
        servicos = new ArrayList<>();
    }

    public TableModelServico(List<Servico> lista) {
        this();
        servicos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else if (coluna == 4) {
            return Color.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return servicos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Servico o = servicos.get(linha);
        String ano = "";
        if (o.getVeiculo().getAno() != null) {
            ano = o.getVeiculo().getAno() + "";
        }
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getIdServico();
            case 1:
                return o.getVeiculo().getPlaca();
            case 2:
                return o.getVeiculo().getAno();
            case 3:
                return o.getVeiculo().getModelo().getDescricao();
            case 4:
                return o.getVeiculo().getCor().getBuscaCor();
            case 5:
                if (o.getCliente() != null) {
                    return o.getCliente().getPessoa().getNome();
                } else {
                    return "";
                }
            case 6:
                return o.getStatusString();
            case 7:
                return Singleton.getInstance().converteDoubleParaString(o.getValorTotal(), 2);
            case 8:
                return "Alterar";
            case 9:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Servico o = servicos.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Servico) valor; // mudamos objeto
                break;
            case 0:
                o.setIdServico(Integer.parseInt(valor.toString()));
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Servico t) {
        servicos.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(servicos.size() - 1, servicos.size() - 1);
    }

    public void remove(int linha) {
        servicos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Servico b) {
        return servicos.indexOf(b);
    }

    public void adicionaLista(List<Servico> lista) {
        int i = servicos.size();
        servicos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = servicos.size();
        servicos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorId() {
        Collections.sort(servicos, new Comparator<Servico>() {
            @Override
            public int compare(Servico o1, Servico o2) {
                return o1.getIdServico() - o2.getIdServico();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorIdDesc() {
        Collections.sort(servicos, new Comparator<Servico>() {
            @Override
            public int compare(Servico o2, Servico o1) {
                return o2.getIdServico() - o1.getIdServico();
            }
        });
        fireTableDataChanged();
    }
}
