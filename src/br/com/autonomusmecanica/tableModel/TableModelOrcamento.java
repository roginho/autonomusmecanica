package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Orcamento;
import br.com.roger.utils.Singleton;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelOrcamento extends AbstractTableModel {

    private String[] colunas = new String[]{"Cod", "Placa", "Ano", "Modelo", "Cor", "Cliente", "Status", "Valor", "Alterar", "Excluir","Gerar"};
    private List<Orcamento> orcamentos;

    public TableModelOrcamento() {
        orcamentos = new ArrayList<>();
    }

    public TableModelOrcamento(List<Orcamento> lista) {
        this();
        orcamentos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else if (coluna == 4) {
            return Color.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return orcamentos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Orcamento o = orcamentos.get(linha);
        String ano = "";
        if (o.getVeiculo().getAno() != null) {
            ano = o.getVeiculo().getAno() + "";
        }
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getId();
            case 1:
                return o.getVeiculo().getPlaca();
            case 2:
                return o.getVeiculo().getAno();
            case 3:
                return o.getVeiculo().getModelo().getDescricao();
            case 4:
                return o.getVeiculo().getCor().getBuscaCor();
            case 5:
                if (o.getCliente() != null) {
                    return o.getCliente().getPessoa().getNome();
                } else {
                    return "";
                }
            case 6:
                return o.getStatusString();
            case 7:
                return Singleton.getInstance().converteDoubleParaString(o.getValorTotal(), 2);
            case 8:
                return "Alterar";
            case 9:
                return "Excluir";
            case 10:
                return "Gerar";

            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Orcamento o = orcamentos.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Orcamento) valor; // mudamos objeto
                break;
            case 0:
                o.setId(Integer.parseInt(valor.toString()));
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Orcamento t) {
        orcamentos.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(orcamentos.size() - 1, orcamentos.size() - 1);
    }

    public void remove(int linha) {
        orcamentos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Orcamento b) {
        return orcamentos.indexOf(b);
    }

    public void adicionaLista(List<Orcamento> lista) {
        int i = orcamentos.size();
        orcamentos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = orcamentos.size();
        orcamentos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorId() {
        Collections.sort(orcamentos, new Comparator<Orcamento>() {
            @Override
            public int compare(Orcamento o1, Orcamento o2) {
                return o1.getId() - o2.getId();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorIdDesc() {
        Collections.sort(orcamentos, new Comparator<Orcamento>() {
            @Override
            public int compare(Orcamento o2, Orcamento o1) {
                return o2.getId() - o1.getId();
            }
        });
        fireTableDataChanged();
    }
}
