package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Estado;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class TableModelEstado extends AbstractTableModel {

    private String[] colunas = new String[]{"Nome", "UF", "Ibge", "Regiao", "Pais", "Alterar", "Excluir"};
    private List<Estado> estados;

    public TableModelEstado() {
        estados = new ArrayList<Estado>();
    }

    public TableModelEstado(List<Estado> lista) {
        this();
        estados.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case -1:
                return Object.class;
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return ImageIcon.class;
            case 6:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return estados.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Estado o = estados.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getNome();
            case 1:
                return o.getUf();
            case 2:
                return o.getIbge();
            case 3:
                return o.getRegiao();
            case 4:
                return o.getPais().getSigla();
            case 5:
                return "Alterar";
            case 6:
                return "Excluir";

            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Estado o = estados.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o.setIdEstado(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 0:
                o.setNome(valor.toString()); // mudamos o nome
                break;
            case 1:
                o.setUf(valor.toString());  // mudamos o estado
                break;
            case 2:
                o.setIbge(Integer.valueOf(valor.toString()));  // mudamos o estado
                break;
            case 3:
                o.setRegiao(valor.toString());  // mudamos o estado
                break;
            case 4:
                o.getPais().setNome(valor.toString());  // mudamos o estado
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Estado o) {
        estados.add(o);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(estados.size() - 1, estados.size() - 1);
    }

    public void remove(int linha) {
        estados.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Estado o) {
        return estados.indexOf(o);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Estado> lista) {
        int i = estados.size();
        estados.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = estados.size();
        estados.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(estados, new Comparator<Estado>() {
            @Override
            public int compare(Estado o1, Estado o2) {
                return o1.getIdEstado() - o2.getIdEstado();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(estados, new Comparator<Estado>() {

            @Override
            public int compare(Estado o1, Estado o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }

}
