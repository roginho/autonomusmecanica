package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Cor;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelCor extends AbstractTableModel {

    private String[] colunas = new String[]{"Descrição", "Cor", "Alterar", "Excluir"};
    private List<Cor> cores;

    public TableModelCor() {
        cores = new ArrayList<>();
    }

    public TableModelCor(List<Cor> lista) {
        this();
        cores.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else if (coluna == 1) {
            return Color.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return cores.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Cor o = cores.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getDescricao();
            case 1:
                return o.getBuscaCor();
            case 2:
                return "Alterar";
            case 3:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Cor o = cores.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Cor) valor; // mudamos objeto
                break;
            case 0:
                o.setDescricao(valor.toString());
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Cor t) {
        cores.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(cores.size() - 1, cores.size() - 1);
    }

    public void remove(int linha) {
        cores.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Cor b) {
        return cores.indexOf(b);
    }

    public void adicionaLista(List<Cor> lista) {
        int i = cores.size();
        cores.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = cores.size();
        cores.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(cores, new Comparator<Cor>() {
            @Override
            public int compare(Cor o1, Cor o2) {
                return o1.getIdCor() - o2.getIdCor();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorDescricao() {
        Collections.sort(cores, new Comparator<Cor>() {

            @Override
            public int compare(Cor o1, Cor o2) {
                return o1.getDescricao().compareTo(o2.getDescricao());
            }
        });
        fireTableDataChanged();
    }
}
