package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Telefone;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelTelefone extends AbstractTableModel {

    private String[] colunas = new String[]{"Numero", "Observações", "Alterar", "Excluir"};
    private List<Telefone> telefones;

    public TableModelTelefone() {
        telefones = new ArrayList<>();
    }

    public TableModelTelefone(List<Telefone> lista) {
        this();
        telefones.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return telefones.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Telefone t = telefones.get(linha);
        switch (coluna) {
            case -1:
                return t;
            case 0:
                return t.getNumero();
            case 1:
                return t.getObs();
            case 2:
                return "Incluir";
            case 3:
                return "Alterar";
            case 5:
                return "Remover";
            default:
                return "";
        }

    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Telefone t = telefones.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                t = (Telefone) valor; // mudamos id
                break;
            case 0:
                t.setNumero(valor.toString()); // mudamos o nome
                break;
            case 1:
                t.setObs(valor.toString()); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Telefone t) {
        telefones.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(telefones.size() - 1, telefones.size() - 1);
    }

    public void remove(int linha) {
        telefones.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Telefone e) {
        return telefones.indexOf(e);
    }

    public void adicionaLista(List<Telefone> lista) {
        int i = telefones.size();
        telefones.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = telefones.size();
        telefones.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(telefones, new Comparator<Telefone>() {
            @Override
            public int compare(Telefone o1, Telefone o2) {
                return o1.getIdTelefone() - o2.getIdTelefone();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorEmail() {
        Collections.sort(telefones, new Comparator<Telefone>() {

            @Override
            public int compare(Telefone o1, Telefone o2) {
                return o1.getNumero().compareTo(o2.getNumero());
            }
        });
        fireTableDataChanged();
    }

}
