package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Fabricante;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelFabricante extends AbstractTableModel {

    private String[] colunas = new String[]{"Fabricante", "Alterar"};
    private List<Fabricante> fabricantes;

    public TableModelFabricante() {
        fabricantes = new ArrayList<>();
    }

    public TableModelFabricante(List<Fabricante> lista) {
        this();
        fabricantes.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return fabricantes.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Fabricante o = fabricantes.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getNome();
            case 1:
                return "Alterar";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Fabricante o = fabricantes.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Fabricante) valor; // mudamos objeto
                break;
            case 0:
                o.setNome(valor.toString());
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Fabricante t) {
        fabricantes.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(fabricantes.size() - 1, fabricantes.size() - 1);
    }

    public void remove(int linha) {
        fabricantes.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Fabricante b) {
        return fabricantes.indexOf(b);
    }

    public void adicionaLista(List<Fabricante> lista) {
        int i = fabricantes.size();
        fabricantes.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = fabricantes.size();
        fabricantes.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(fabricantes, new Comparator<Fabricante>() {
            @Override
            public int compare(Fabricante o1, Fabricante o2) {
                return o1.getIdFabricante() - o2.getIdFabricante();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorDescricao() {
        Collections.sort(fabricantes, new Comparator<Fabricante>() {

            @Override
            public int compare(Fabricante o1, Fabricante o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }
}
