package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.ServicoParcela;
import br.com.roger.utils.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelServicoParcela extends AbstractTableModel {

    private String[] colunas = new String[]{"Objeto", "Parcela", "Forma Pgto.", "Data Vcto", "Data Pgto", "Valor Parc", "Valor Pago", "Obs ","Alterar", "Excluir"};
    private List<ServicoParcela> servicosParcelas;

    public TableModelServicoParcela() {
        servicosParcelas = new ArrayList<>();
    }

    public TableModelServicoParcela(List<ServicoParcela> lista) {
        this();
        servicosParcelas.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == 0) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return servicosParcelas.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        ServicoParcela o = servicosParcelas.get(linha);
        switch (coluna) {
            case 0:
                return o;
            case 1:
                return o.getParcela();
            case 2:
                return o.getFormaPgto().getDescricao();
            case 3:
                return Singleton.getInstance().converteDataParaString(o.getDataVcto());
            case 4:
                return Singleton.getInstance().converteDataParaString(o.getDataPgto());
            case 5:
                return Singleton.getInstance().converteDoubleParaString(o.getValorParcela(), 2);
            case 6:
                return Singleton.getInstance().converteDoubleParaString(o.getValorPago(), 2);
            case 7:
                return o.getObs();
            case 8:
                return "Alterar";
            case 9:
                return "Excluir";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        ServicoParcela o = servicosParcelas.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case 0:
                o.setIdServicoParcela(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 3:
                o.setParcela(Integer.parseInt(valor.toString()));
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(ServicoParcela t) {
        servicosParcelas.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(servicosParcelas.size() - 1, servicosParcelas.size() - 1);
    }

    public void remove(int linha) {
        servicosParcelas.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(ServicoParcela b) {
        return servicosParcelas.indexOf(b);
    }

    public void adicionaLista(List<ServicoParcela> lista) {
        int i = servicosParcelas.size();
        servicosParcelas.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = servicosParcelas.size();
        servicosParcelas.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorId() {
        Collections.sort(servicosParcelas, new Comparator<ServicoParcela>() {
            @Override
            public int compare(ServicoParcela o1, ServicoParcela o2) {
                return o1.getIdServicoParcela() - o2.getIdServicoParcela();
            }
        });
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int row, int coluna) {
        return coluna == 7;
    }
}
