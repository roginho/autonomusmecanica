package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Endereco;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelEndereco extends AbstractTableModel {

    private List<Endereco> enderecos;
    private String[] colunas = new String[]{"Logradouro", "Nº", "Compº", "Bairro", "Cidade", "Estado", "Pais", "Alterar", "Remover"};

    public TableModelEndereco() {
        enderecos = new ArrayList<>();
    }

    public TableModelEndereco(List<Endereco> lista) {
        this();
        enderecos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return enderecos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Endereco e = enderecos.get(linha);
        switch (coluna) {
            case -1:
                return e;
            case 0:
                return e.getLogradouro();
            case 1:
                return e.getNumero();
            case 2:
                return e.getComplemento();
            case 3:
                return e.getBairro();
            case 4:
                return e.getCidade();
            case 5:
                if (e.getCidade() != null) {
                    return e.getCidade().getEstado();
                } else {
                    return "";
                }
            case 6:
                if (e.getCidade() != null) {
                    return e.getCidade().getEstado().getPais();
                } else {
                    return "";
                }
            case 7:
                return "Alterar";
            case 8:
                return "Remover";
            default:
                return "";
        }

    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Endereco e = enderecos.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                e.setIdEndereco(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 0:
                e.setLogradouro(valor.toString());
                break;
            case 1:
                e.setNumero(valor.toString());
                break;
            case 2:
                e.setComplemento(valor.toString());
                break;
            case 3:
                e.setBairro(valor.toString());
                break;
            case 4:
                e.getCidade().setNome(valor.toString());
                break;
            case 5:
                e.getCidade().getEstado().setNome(valor.toString());
                break;
            case 6:
                e.getCidade().getEstado().getPais().setNome(valor.toString());
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    // informamos os listeners que a linha (size - 1) foi adicionada  
    public void adiciona(Endereco t) {
        enderecos.add(t);
        fireTableRowsInserted(enderecos.size() - 1, enderecos.size() - 1);
    }

    public void remove(int linha) {
        enderecos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     *
     * @return
     * @param e
     */
    public int getIndice(Endereco e) {
        return enderecos.indexOf(e);
    }

    public void adicionaLista(List<Endereco> lista) {
        int i = enderecos.size();
        enderecos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = enderecos.size();
        enderecos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(enderecos, new Comparator<Endereco>() {
            @Override
            public int compare(Endereco o1, Endereco o2) {
                return o1.getIdEndereco() - o2.getIdEndereco();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorEmail() {
        Collections.sort(enderecos, new Comparator<Endereco>() {

            @Override
            public int compare(Endereco o1, Endereco o2) {
                return o1.getLogradouro().compareTo(o2.getLogradouro());
            }
        });
        fireTableDataChanged();
    }
}
