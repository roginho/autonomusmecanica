package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Modelo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class TableModelModelo extends AbstractTableModel {

    private String[] colunas = new String[]{"Descrição", "Fabricante", "Alterar", "Excluir"};
    private List<Modelo> modelos;

    public TableModelModelo() {
        modelos = new ArrayList<Modelo>();
    }

    public TableModelModelo(List<Modelo> lista) {
        this();
        modelos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case -1:
                return Object.class;
            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return ImageIcon.class;
            case 3:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return modelos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Modelo o = modelos.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getDescricao();
            case 1:
                return o.getFabricante().getNome();
            case 2:
                return "Alterar";
            case 3:
                return "Excluir";

            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Modelo o = modelos.get(linha);
        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (Modelo) valor; // mudamos id
                break;
            case 0:
                o.setDescricao(valor.toString()); // mudamos o nome
                break;
            case 1:
                o.getFabricante().setNome(valor.toString());  // mudamos o estado
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Modelo o) {
        modelos.add(o);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(modelos.size() - 1, modelos.size() - 1);
    }

    public void remove(int linha) {
        modelos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado cidade.
     */
    public int getIndice(Modelo o) {
        return modelos.indexOf(o);
    }

    /**
     * Adiciona todos os clientes na lista à este modelo.
     */
    public void adicionaLista(List<Modelo> lista) {
        int i = modelos.size();
        modelos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = modelos.size();
        modelos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(modelos, new Comparator<Modelo>() {
            @Override
            public int compare(Modelo o1, Modelo o2) {
                return o1.getIdModelo() - o2.getIdModelo();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(modelos, new Comparator<Modelo>() {

            @Override
            public int compare(Modelo o1, Modelo o2) {
                return o1.getDescricao().compareTo(o2.getDescricao());
            }
        });
        fireTableDataChanged();
    }

}
