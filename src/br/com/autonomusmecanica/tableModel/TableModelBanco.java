package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.Banco;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class TableModelBanco extends AbstractTableModel {
    private String[] colunas = new String[]{"Objeto", "Código", "Banco", "Alterar", "Excluir"};
    private List<Banco> bancos;

    public TableModelBanco() {
        bancos = new ArrayList<>();
    }

    public TableModelBanco(List<Banco> lista) {
        this();
        bancos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        switch (coluna) {
            case 0:
                return Object.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return ImageIcon.class;
            case 5:
                return ImageIcon.class;
            default:
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return bancos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        Banco b = bancos.get(linha);
        switch (coluna) {
            case 0:
                return b;
            case 1:
                return b.getCodigo();
            case 2:
                return b.getNome();
            case 3:
                return "Alterar";
            case 4:
                return "Excluir";

            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        Banco b = bancos.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case 0:
                b.setIdBanco(Integer.parseInt(valor.toString())); // mudamos id
                break;
            case 1:
                b.setCodigo(valor.toString());
                break;
            case 2:
                b.setNome(valor.toString()); // mudamos o nome
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(Banco t) {
        bancos.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(bancos.size() - 1, bancos.size() - 1);
    }

    public void remove(int linha) {
        bancos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(Banco b) {
        return bancos.indexOf(b);
    }

    public void adicionaLista(List<Banco> lista) {
        int i = bancos.size();
        bancos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = bancos.size();
        bancos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(bancos, new Comparator<Banco>() {
            @Override
            public int compare(Banco o1, Banco o2) {
                return o1.getIdBanco() - o2.getIdBanco();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNumero() {
        Collections.sort(bancos, new Comparator<Banco>() {

            @Override
            public int compare(Banco o1, Banco o2) {
                return o1.getCodigo().compareTo(o2.getCodigo());
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorNome() {
        Collections.sort(bancos, new Comparator<Banco>() {

            @Override
            public int compare(Banco o1, Banco o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });
        fireTableDataChanged();
    }
}
