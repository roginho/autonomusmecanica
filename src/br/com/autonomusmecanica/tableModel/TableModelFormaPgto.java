package br.com.autonomusmecanica.tableModel;

import br.com.autonomusmecanica.entidades.FormaPgto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModelFormaPgto extends AbstractTableModel {

    private String[] colunas = new String[]{"Forma de Pagamento", "Alterar"};
    private List<FormaPgto> formasPgtos;

    public TableModelFormaPgto() {
        formasPgtos = new ArrayList<>();
    }

    public TableModelFormaPgto(List<FormaPgto> lista) {
        this();
        formasPgtos.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        if (coluna == -1) {
            return Object.class;
        } else {
            return String.class;
        }
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public String getColumnName(int coluna) {
        return colunas[coluna];
    }

    @Override
    public int getRowCount() {
        return formasPgtos.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna
    ) {
        FormaPgto o = formasPgtos.get(linha);
        switch (coluna) {
            case -1:
                return o;
            case 0:
                return o.getDescricao();
            case 1:
                return "Alterar";
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna
    ) {
        FormaPgto o = formasPgtos.get(linha);

        // e vemos o que será atualizado  
        switch (coluna) {
            case -1:
                o = (FormaPgto) valor; // mudamos objeto
                break;
            case 0:
                o.setDescricao(valor.toString());
                break;
        }
        // é importante notificar os listeners a cada alteração  
        fireTableDataChanged();
    }

    public void adiciona(FormaPgto t) {
        formasPgtos.add(t);
        // informamos os listeners que a linha (size - 1) foi adicionada  
        fireTableRowsInserted(formasPgtos.size() - 1, formasPgtos.size() - 1);
    }

    public void remove(int linha) {
        formasPgtos.remove(linha);
        fireTableRowsDeleted(linha, linha);
    }

    /**
     * Retorna o índice de determinado objeto
     */
    public int getIndice(FormaPgto b) {
        return formasPgtos.indexOf(b);
    }

    public void adicionaLista(List<FormaPgto> lista) {
        int i = formasPgtos.size();
        formasPgtos.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    /**
     * Esvazia a lista.
     */
    public void limpaLista() {
        int i = formasPgtos.size();
        formasPgtos.clear();
        fireTableRowsDeleted(0, i - 1);
    }

    public void ordenarPorID() {
        Collections.sort(formasPgtos, new Comparator<FormaPgto>() {
            @Override
            public int compare(FormaPgto o1, FormaPgto o2) {
                return o1.getIdFormaPgto() - o2.getIdFormaPgto();
            }
        });
        fireTableDataChanged();
    }

    public void ordenarPorDescricao() {
        Collections.sort(formasPgtos, new Comparator<FormaPgto>() {

            @Override
            public int compare(FormaPgto o1, FormaPgto o2) {
                return o1.getDescricao().compareTo(o2.getDescricao());
            }
        });
        fireTableDataChanged();
    }
}
