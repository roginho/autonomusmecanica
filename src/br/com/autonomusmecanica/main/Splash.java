package br.com.autonomusmecanica.main;
//Bibliotecas

import br.com.autonomusconfig.AutonomusConfig;
import br.com.autonomusconfig.gui.DialogConfig;
import br.com.autonomusmecanica.gui.FrameInicial;
import javax.swing.*;

//Classe Splash, que extende da JFrame
public class Splash extends JFrame {

    private static JLabel lbSplash;          //label responsável por conter a imagem
    private final ImageIcon imSplash;              //imagem que será mostrada no label

    //função principal que será carregada
    public static void main(String arg[]) {
        //Criando a janela de Splash
        Splash s = new Splash();

        //definindo o tipo de fechamento, o tamanho, tirando a barra de títulos, deixando no centro, definindo um icone e mostrando a janela
        s.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        s.setSize(200, 100);
        s.setUndecorated(true);
        //s.setBackground(new Color(0f, 0f, 0f, 0f));
        s.setLocationRelativeTo(null);
        s.setVisible(true);
        //forçando a espera de 1000 milissegundos (1 segundos)
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }

        //fechando a janela
        s.dispose();
    }

    //função responsável por carregar os dados da janela (layout e imagem)
    public Splash() {
        //definindo o layout como nulo
        setLayout(null);

        //setando a imagem de splash
        imSplash = new ImageIcon(getClass().getResource("/br/com/roger/gui/imagem/aguarde.gif"));

        //adicionando a imagem no label e mudando o tamanho
        lbSplash = new JLabel(imSplash);
        lbSplash.setBounds(0, 0, 200, 100);
        //adicionando componentes na janela
        this.add(lbSplash);
    }

    //função ao fechar a splash
    @Override
    public void dispose() {
        AutonomusConfig conf = new AutonomusConfig();
        if (!conf.existeArquivoConfig()) {
            super.dispose();
            DialogConfig d = new DialogConfig(this, true);
            d.setVisible(true);
        } else {
            //criar a tela de menu, e fechar o splash
            FrameInicial f = new FrameInicial();
            f.setVisible(true);
            super.dispose();
        }
    }
}
