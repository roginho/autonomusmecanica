/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.relatorios;

import br.com.roger.utils.Singleton;
import br.com.roger.gui.util.DialogProgress;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Bianca
 */
public class GeraRelatorio {

    private JDialog viewer;
    private boolean mostrarCampo;
    
    public GeraRelatorio() {
     this.mostrarCampo = false;   
    }
    
    public void mostrarCampo(boolean mostrarCampo){
        this.mostrarCampo = mostrarCampo;
    }
    public void geraRelatorio(final List lista, final String titulo, final String nomeRelatorio) {
        final DialogProgress d = new DialogProgress(null, true);
        Thread threadRelatorio = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap map = new HashMap();
                map.put("PAR_LOGO", Singleton.getInstance().caminhoLogo());
                map.put("PAR_EMPRESA", Singleton.getInstance().dadosEmpresa());
                if(mostrarCampo == true) {
                    System.out.println("");
                    map.put("PAR_MOSTRAR", true);
                }
                viewer = new JDialog(new javax.swing.JDialog(), titulo, true);
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screenSize = toolkit.getScreenSize();
                viewer.setBounds(0, 0, screenSize.width, screenSize.height);
                JRBeanCollectionDataSource dados = new JRBeanCollectionDataSource(lista);
                String pacote = getClass().getPackage().getName();
                pacote = pacote.replace(".", "/");
                InputStream arquivoJasper = this.getClass().getResourceAsStream("/" + pacote + "/" + nomeRelatorio + ".jasper");
                try {
                    JasperPrint jp = JasperFillManager.fillReport(arquivoJasper, map, dados);
                    JasperViewer jrView = new JasperViewer(jp, false);
                    viewer.getContentPane().add(jrView.getContentPane());
                    d.dispose();
                    viewer.setVisible(true);
                } catch (JRException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Erro ao gerar relatorio\n" + ex.getMessage(), "Erro!!!" + ex.getMessage(),
                            JOptionPane.ERROR_MESSAGE);
                    d.dispose();

                }
            }
        });
        try {
            threadRelatorio.join();
            threadRelatorio.start();
            d.setVisible(true);
        } catch (InterruptedException ex) {
            System.out.println(ex.getLocalizedMessage());
            d.dispose();
        }
    }

    public void geraRelatorioPDF(final List lista, final String titulo, final String nomeRelatorio, final String aux) {
        final DialogProgress d = new DialogProgress(null, true);
        Thread threadRelatorio;
        threadRelatorio = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap map = new HashMap();
                map.put("PAR_LOGO", Singleton.getInstance().caminhoLogo());
                map.put("PAR_EMPRESA", Singleton.getInstance().dadosEmpresa());
                 if(mostrarCampo == true) {
                    map.put("MOSTRAR_CAMPO", true);
                }
                JRBeanCollectionDataSource dados = new JRBeanCollectionDataSource(lista);
                String pacote = getClass().getPackage().getName();
                pacote = pacote.replace(".", "/");
                InputStream arquivoJasper = this.getClass().getResourceAsStream("/" + pacote + "/" + nomeRelatorio + ".jasper");
                try {
                    JasperPrint jp = JasperFillManager.fillReport(arquivoJasper, map, dados);
                    String caminhoPDF = Singleton.getInstance().caminhoPDF();
                    File f = new File(caminhoPDF);
                    if (!f.exists()) {
                        f.mkdir();
                    }
                    JasperExportManager.exportReportToPdfFile(jp, f+"/"+aux+"_"+nomeRelatorio+".pdf");
                    d.dispose();
                } catch (JRException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Erro ao gerar relatorio\n" + ex.getMessage(), "Erro!!!" + ex.getMessage(),
                            JOptionPane.ERROR_MESSAGE);
                    d.dispose();

                }
            }
        });
        try {
            threadRelatorio.join();
            threadRelatorio.start();
            d.setVisible(true);
        } catch (InterruptedException ex) {
            System.out.println(ex.getLocalizedMessage());
            d.dispose();
        }
    }
}
