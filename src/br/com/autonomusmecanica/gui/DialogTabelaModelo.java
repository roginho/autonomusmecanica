package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.ModeloDAOJPA;
import br.com.autonomusmecanica.entidades.Modelo;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelModelo;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

public final class DialogTabelaModelo extends javax.swing.JDialog {

    private TableModelModelo model;
    private final TableCellRenderer renderer = new StripedTable();
    private TableRowSorter sorter;
    private final int colunaAlterar = 2;
    private final int colunaExcluir = 3;

    public void atualizaTabela() {
        ModeloDAOJPA dao = new ModeloDAOJPA();
        List<Modelo> lista = dao.getAll(Modelo.class);
        model = new TableModelModelo(lista);
        table.setModel(model);
        model.ordenarPorNome();
        table.getColumnModel().getColumn(0).setPreferredWidth(250);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.getColumnModel().getColumn(colunaAlterar).setPreferredWidth(20);
        table.getColumnModel().getColumn(colunaExcluir).setPreferredWidth(20);
        table.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(table, colunaAlterar, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(table, colunaExcluir, Singleton.getInstance().iconeJTable("Excluir"));
        sorter = new TableRowSorter<>(model);
        labelTotal.setText("" + table.getRowCount());
        table.setRowSorter(sorter);
    }

    private void alterar() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Modelo p = new Modelo();
            p = (Modelo) table.getValueAt(linha, -1);
            DialogCadastroModelo d = new DialogCadastroModelo(null, true, p);
            d.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Selecione uma linha para alterar");
        }
    }

    private void excluir() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Modelo e = (Modelo) table.getValueAt(linha, -1);
            if (e != null) {
                ModeloDAOJPA dao = new ModeloDAOJPA();
                int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja excluir o Modelo? \n" + e.getDescricao(), "Excluir", JOptionPane.YES_NO_OPTION);
                //0=sim e 1=nao 
                if (showConfirmDialog == 0) {
                    dao.remove(Modelo.class, e.getIdModelo());
                    atualizaTabela();
                }
            }
        }
    }

    public DialogTabelaModelo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Modelo");
        atualizaTabela();
        textFieldPesquisa.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        buttonAdicionar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        textFieldPesquisa = new javax.swing.JTextField();
        labelTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        table.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        buttonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeAdicionar32x32.png"))); // NOI18N
        buttonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAdicionarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        textFieldPesquisa.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro"));
        textFieldPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyReleased(evt);
            }
        });

        labelTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonFechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textFieldPesquisa)
                        .addGap(18, 18, 18)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        atualizaTabela();
    }//GEN-LAST:event_formWindowActivated

    private void buttonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAdicionarActionPerformed
        DialogCadastroModelo d = new DialogCadastroModelo(null, true, null);
        d.setVisible(true);
        atualizaTabela();
    }//GEN-LAST:event_buttonAdicionarActionPerformed

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void textFieldPesquisaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyTyped

    }//GEN-LAST:event_textFieldPesquisaKeyTyped

    private void textFieldPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyReleased
        if (table.getSelectedColumn() > 0) {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), table.getSelectedColumn());
        } else {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), 0);
        }

        labelTotal.setText("" + table.getRowCount());
    }//GEN-LAST:event_textFieldPesquisaKeyReleased

    private void tableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tableKeyPressed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colunaAlterar) {
            alterar();
        } else if (table.getSelectedColumn() == colunaExcluir) {
            excluir();
        }
    }//GEN-LAST:event_tableMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdicionar;
    private javax.swing.JButton buttonFechar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JTable table;
    private javax.swing.JTextField textFieldPesquisa;
    // End of variables declaration//GEN-END:variables
}
