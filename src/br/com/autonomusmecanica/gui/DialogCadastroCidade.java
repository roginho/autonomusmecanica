package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.CidadeDAOJPA;
import br.com.autonomusmecanica.dao.EstadoDAOJPA;
import br.com.autonomusmecanica.entidades.Cidade;
import br.com.autonomusmecanica.entidades.Estado;
import br.com.autonomusmecanica.funcoes.LimitaCaracteres;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author roger
 */
public class DialogCadastroCidade extends javax.swing.JDialog {

    private Cidade cidade;

    private void carregaComboEstado() {
        for (Estado e : new EstadoDAOJPA().getAll(Estado.class)) {
            comboBoxEstado.addItem(e);
        }
    }

    private void salvar() {
        if (textFieldNome.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o nome");
            textFieldNome.requestFocus();
            return;
        }
        Cidade c = new Cidade();
        if (cidade == null) {
            //System.out.println("inserir");
        } else {
            //System.out.println("atualizar");
            c = this.cidade;
        }
        c.setNome(textFieldNome.getText());
        c.setIbge(Integer.valueOf(textFieldIbge.getText()));
        c.setEstado((Estado) comboBoxEstado.getSelectedItem());
        /**
         * verifica se já existe
         */
        CidadeDAOJPA dao = new CidadeDAOJPA();
        List cidades;
        if (cidade == null) {
            cidades = dao.getExisteCidade(textFieldNome.getText(), 0);
        } else {
            cidades = dao.getExisteCidade(textFieldNome.getText(), cidade.getIdCidade());
        }
        System.out.println(cidades);
        if (cidades.isEmpty()) {
            dao.save(c);
            this.cidade = c;
        } else {
            JOptionPane.showMessageDialog(this, "Este estado já existe", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void refreshForm() {
        limpaTela();
        labelCodigo.setText("" + cidade.getIdCidade());
        textFieldNome.setText(cidade.getNome());
        comboBoxEstado.setSelectedItem(cidade.getEstado());
        textFieldIbge.setText("" + cidade.getIbge());
    }

    private void limpaTela() {
        textFieldNome.setText("");
        textFieldIbge.setText("");
        comboBoxEstado.setSelectedIndex(-1);
    }

    private void novo() {
        this.limpaTela();
        this.cidade = null;
    }

    public DialogCadastroCidade(java.awt.Frame parent, boolean modal, Cidade c) {
        super(parent, modal);
        initComponents();
        carregaComboEstado();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        textFieldIbge.setDocument(new LimitaCaracteres(7));
        if (c == null) {
            novo();
        } else {
//            System.out.println("altera");
            this.cidade = c;
            refreshForm();
        }
        textFieldNome.requestFocus();
    }

    private void buscaPrimeiroRegistro() {
        CidadeDAOJPA dao = new CidadeDAOJPA();
        this.cidade = (Cidade) dao.primeiroRegistro(Cidade.class);
        refreshForm();
    }

    private void buscaUltimoRegistro() {
        CidadeDAOJPA dao = new CidadeDAOJPA();
        this.cidade = (Cidade) dao.ultimoRegistro(Cidade.class, "idCidade");
        refreshForm();
    }

    private void proximo() {
        if (cidade == null) {
            buscaUltimoRegistro();
        } else {
            CidadeDAOJPA dao = new CidadeDAOJPA();
            this.cidade = (Cidade) dao.getProximo(Cidade.class, "idCidade", cidade.getIdCidade());
            if (cidade != null) {
                refreshForm();
            }
        }
    }

    private void anterior() {
        if (cidade == null) {
            buscaPrimeiroRegistro();
        } else {
            CidadeDAOJPA dao = new CidadeDAOJPA();
            this.cidade = (Cidade) dao.getAnterior(Cidade.class, "idCidade", cidade.getIdCidade());
            if (cidade != null) {
                refreshForm();
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pessoaDAOJPA1 = new br.com.autonomusmecanica.dao.PessoaDAOJPA();
        buttonGroupStatus = new javax.swing.ButtonGroup();
        buttonGroupTipo = new javax.swing.ButtonGroup();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        panel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        textFieldNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textFieldIbge = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        comboBoxEstado = new javax.swing.JComboBox();
        buttonSiteIbge = new javax.swing.JButton();
        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Salvar");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel4.setText("Nome:");

        jLabel3.setText("Ibge:");

        jLabel6.setText("Estados:");

        comboBoxEstado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxEstadoItemStateChanged(evt);
            }
        });

        buttonSiteIbge.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/ibge22x22.png"))); // NOI18N
        buttonSiteIbge.setToolTipText("Site IBGE");
        buttonSiteIbge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSiteIbgeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4))
                .addGap(3, 3, 3)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textFieldNome)
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addComponent(textFieldIbge, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonSiteIbge)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(12, 12, 12))
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(comboBoxEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(21, 21, 21))))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(textFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(textFieldIbge, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(buttonSiteIbge, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cidades"));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeCidades64x64.png"))); // NOI18N

        labelCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(48, 48, 48)
                .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(buttonPrev)
                        .addComponent(buttonNext)
                        .addComponent(buttonNext1)
                        .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel7))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange

    }//GEN-LAST:event_formPropertyChange

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void comboBoxEstadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxEstadoItemStateChanged

    }//GEN-LAST:event_comboBoxEstadoItemStateChanged

    private void buttonSiteIbgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSiteIbgeActionPerformed
        Singleton.getInstance().abrirSite("http://www.ibge.gov.br/home/geociencias/areaterritorial/area.shtm");
        
    }//GEN-LAST:event_buttonSiteIbgeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroupStatus;
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JButton buttonSiteIbge;
    private javax.swing.JComboBox comboBoxEstado;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private br.com.autonomusmecanica.dao.PessoaDAOJPA pessoaDAOJPA1;
    private javax.swing.JFormattedTextField textFieldIbge;
    private javax.swing.JTextField textFieldNome;
    // End of variables declaration//GEN-END:variables
}
