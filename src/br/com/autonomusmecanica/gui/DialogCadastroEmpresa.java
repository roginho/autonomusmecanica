package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.EmailDAOJPA;
import br.com.autonomusmecanica.dao.EmpresaDAOJPA;
import br.com.autonomusmecanica.dao.EnderecoDAOJPA;
import br.com.autonomusmecanica.dao.PessoaDAOJPA;
import br.com.autonomusmecanica.dao.TelefoneDAOJPA;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Empresa;
import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Telefone;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelEmail;
import br.com.autonomusmecanica.tableModel.TableModelEndereco;
import br.com.autonomusmecanica.tableModel.TableModelTelefone;
import br.com.roger.utils.Singleton;
import br.com.roger.validador.Validador;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author roger
 */
public final class DialogCadastroEmpresa extends javax.swing.JDialog {

    private Pessoa pessoa;
    private Empresa empresa;
    private TableModelEndereco modelEndereco;
    private PanelEndereco panelEndereco = new PanelEndereco();
    private PanelFone panelFone = new PanelFone();
    private PanelEmail panelEmail = new PanelEmail();
    private final TableCellRenderer renderer = new StripedTable();

    private boolean empresaIsNull() {
        boolean valida;
        valida = this.empresa == null;
        return valida;
    }

    private void formataMascaraJuridicaFisica() {
        try {
            if (radioButtonJuridica.isSelected()) {
                textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
            }
            if (radioButtonFisica.isSelected()) {
                textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
            }
            if (this.empresa != null) {
                textFieldCnpjCpf.setText(this.empresa.getPessoa().getCpfCnpj());
            }
        } catch (ParseException ex) {
            Logger.getLogger(DialogCadastroEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void salvar() {
        String cnpjCpf = null;
        if (textFieldNome.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o nome:");
            textFieldNome.requestFocus();
            return;
        }
        Character tipo = null;
        if (radioButtonJuridica.isSelected()) {
            tipo = 'j';
        }
        if (radioButtonFisica.isSelected()) {
            tipo = 'f';
        }

        boolean temNumeros = Singleton.getInstance().temNumeros(textFieldCnpjCpf.getText());
        Boolean validaCnpjCpf = true;
        if (temNumeros) {
            cnpjCpf = textFieldCnpjCpf.getText();
            if (tipo.equals('j')) {
                validaCnpjCpf = Validador.validaCnpj(textFieldCnpjCpf.getText());
                if (!validaCnpjCpf) {
                    JOptionPane.showMessageDialog(this, "CNPJ Inválido", "Alerta", JOptionPane.WARNING_MESSAGE);
                    textFieldCnpjCpf.setValue("");
                    formataMascaraJuridicaFisica();
                    textFieldCnpjCpf.requestFocus();
                }
            }
            if (tipo.equals('f')) {
                validaCnpjCpf = Validador.validaCpf(textFieldCnpjCpf.getText());
                if (!validaCnpjCpf) {
                    JOptionPane.showMessageDialog(this, "CPF Inválido", "Alerta", JOptionPane.WARNING_MESSAGE);
                    textFieldCnpjCpf.setValue("");
                    formataMascaraJuridicaFisica();
                    textFieldCnpjCpf.requestFocus();
                }
            }
        }
        if (validaCnpjCpf) {
            String mensagem;
            Pessoa p = new Pessoa();
            Empresa e = new Empresa();
            if (!empresaIsNull()) {
                mensagem = "Registro atualizado com sucesso";
                p = this.pessoa;
                e = this.empresa;
            } else {
                mensagem = "Registro inserido com sucesso";
            }
            p.setNome(textFieldNome.getText());
            p.setApelidoNomeFantasia(textFieldApelido.getText());
            p.setCpfCnpj(cnpjCpf);
            p.setTipo(tipo);
            p.setStatus('a');
            e.setPessoa(p);
            EmpresaDAOJPA dao = new EmpresaDAOJPA();
            PessoaDAOJPA dao2 = new PessoaDAOJPA();

            List empresas;
            if (empresa == null) {
                empresas = dao2.getExiste(textFieldNome.getText(), -1);
            } else {
                empresas = dao2.getExiste(textFieldNome.getText(), empresa.getPessoa().getIdPessoa());
            }
            if (empresas.isEmpty()) {
                p = dao2.save(p);
                e.setPessoa(p);
                e = dao.save(e);
                this.pessoa = p;
                this.empresa = e;
                JOptionPane.showMessageDialog(this, mensagem);
                dispose();
            } else {
                int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Este nome já existe, vincular ha mesma pessoa ?", "", JOptionPane.YES_NO_OPTION);
                if (showConfirmDialog == 0) {
                    p = (Pessoa) empresas.get(0);
                    p = dao2.save(p);
                    e.setPessoa(p);
                    e = dao.save(e);
                    this.pessoa = p;
                    this.empresa = e;
                    JOptionPane.showMessageDialog(this, mensagem);
                    //                  dispose();
                }
                //JOptionPane.showMessageDialog(this, "Este nome já existe, salve com um nome diferente", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
            refreshForm();
        }
    }

    private void refreshForm() {
        limpaTela();
        if (!empresaIsNull()) {
            labelCodigo.setText("" + this.empresa.getIdEmpresa());
            textFieldNome.setText(this.empresa.getPessoa().getNome());
            textFieldApelido.setText(this.empresa.getPessoa().getApelidoNomeFantasia());
            if (this.empresa.getPessoa().getTipo() == 'j') {
                radioButtonJuridica.setSelected(true);
            } else {
                radioButtonFisica.setSelected(true);
            }
            formataMascaraJuridicaFisica();
            textFieldCnpjCpf.setText(this.empresa.getPessoa().getCpfCnpj());
            panelEmail.setPessoa(this.pessoa);
            panelEmail.atualizaTabelaEmail();
            panelFone.setPessoa(this.pessoa);
            panelFone.atualizaTabelaTelefone();
            panelEndereco.setPessoa(this.pessoa);
            panelEndereco.atualizaTabelaEndereco();

        }
    }

    private void limpaTela() {
        labelCodigo.setText("");
        radioButtonJuridica.setSelected(true);
        textFieldNome.setText("");
        textFieldApelido.setText("");
        textFieldCnpjCpf.setValue("");
        new TableModelTelefone().limpaLista();
        new TableModelEndereco().limpaLista();
    }

    private void novo() {
        this.limpaTela();
        this.empresa = null;
        this.pessoa = null;
        panelEndereco.limpaTabela();
        panelFone.limpaTabela();
        panelEmail.limpaTabela();
        radioButtonJuridica.setSelected(true);
        formataMascaraJuridicaFisica();
    }

    private void buscaPrimeiroRegistro() {
        EmpresaDAOJPA dao = new EmpresaDAOJPA();
        this.empresa = (Empresa) dao.primeiroRegistro(Empresa.class);
        this.pessoa = empresa.getPessoa();
        refreshForm();
    }

    private void buscaUltimoRegistro() {
        EmpresaDAOJPA dao = new EmpresaDAOJPA();
        this.empresa = (Empresa) dao.ultimoRegistro(Empresa.class, "idEmpresa");
        this.pessoa = empresa.getPessoa();
        refreshForm();
    }

    private void proximo() {
        if (empresa == null) {
            buscaUltimoRegistro();
        } else {
            EmpresaDAOJPA dao = new EmpresaDAOJPA();
            this.empresa = (Empresa) dao.getProximo(Empresa.class, "idEmpresa", empresa.getIdEmpresa());
            this.pessoa = empresa.getPessoa();
            if (empresa != null) {
                refreshForm();
            }
        }
    }

    private void anterior() {
        if (empresa == null) {
            buscaPrimeiroRegistro();
        } else {
            EmpresaDAOJPA dao = new EmpresaDAOJPA();
            this.empresa = (Empresa) dao.getAnterior(Empresa.class, "idEmpresa", empresa.getIdEmpresa());
            this.pessoa = empresa.getPessoa();
            if (empresa != null) {
                refreshForm();
            }
        }
    }

    private void adicionaImagem() {
        JFileChooser chooser;
        chooser = new JFileChooser();
        String caminho = "";
        File fileDestino = new File("/home/roger/imagens/");
        int retorno = chooser.showSaveDialog(null); // showSaveDialog retorna um inteiro , e ele ira determinar que o chooser será para salvar.
        if (retorno == JFileChooser.SAVE_DIALOG) {
            try {
                caminho = chooser.getSelectedFile().getAbsolutePath();  // o getSelectedFile pega o arquivo e o getAbsolutePath retorna uma string contendo o endereço.
                File fileCaminho = new File(caminho);
                Singleton.getInstance().copyFile(fileCaminho, fileDestino);

            } catch (IOException ex) {
                Logger.getLogger(DialogCadastroEmpresa.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ImageIcon criarImageIcon(String caminho, String descricao) {
        java.net.URL imgURL = getClass().getResource(caminho);
        if (imgURL != null) {
            return new ImageIcon(imgURL, descricao);
        } else {
            System.err.println("Não foi possível carregar o arquivo de imagem: " + caminho);
            return null;
        }
    }

    private void adicionaTabPanelEndereco() {
        panelEndereco = new PanelEndereco();
        tabbedPane.addTab("Endereço", panelEndereco.getIconEndereco(), panelEndereco);
    }

    private void adicionaTabPanelTelefone() {
        panelFone = new PanelFone();
        tabbedPane.addTab("Telefone", panelFone.getIconTelefone(), panelFone);
    }

    private void adicionaTabPanelEmail() {
        panelEmail = new PanelEmail();
        tabbedPane.addTab("Email", panelEmail.getIconEmail(), panelEmail);
    }

    public DialogCadastroEmpresa(java.awt.Frame parent, boolean modal, Empresa e) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        Singleton.getInstance().passaCamposComEnter(tabbedPane);
        adicionaTabPanelEndereco();
        adicionaTabPanelTelefone();
        adicionaTabPanelEmail();
        if (e == null) {
            novo();
        } else {
            this.empresa = e;
            this.pessoa = e.getPessoa();
            refreshForm();
        }
        textFieldNome.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTipo = new javax.swing.ButtonGroup();
        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        panel2 = new javax.swing.JPanel();
        textFieldNome = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        textFieldApelido = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        textFieldCnpjCpf = new javax.swing.JFormattedTextField();
        radioButtonJuridica = new javax.swing.JRadioButton();
        radioButtonFisica = new javax.swing.JRadioButton();
        buttonReceitaFederal = new javax.swing.JButton();
        buttonNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Empresas"));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/empresa64x64.png"))); // NOI18N

        labelCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(198, 198, 198)
                .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonPrev)
                    .addComponent(buttonNext)
                    .addComponent(buttonNext1)
                    .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Salvar");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Gerais"));

        jLabel5.setText("Apelido:");

        jLabel8.setText("Nome/Razão Social:");

        jLabel9.setText("CNPJ/CPF:");

        try {
            textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        buttonGroupTipo.add(radioButtonJuridica);
        radioButtonJuridica.setSelected(true);
        radioButtonJuridica.setText("Juririca");
        radioButtonJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButtonJuridicaActionPerformed(evt);
            }
        });

        buttonGroupTipo.add(radioButtonFisica);
        radioButtonFisica.setText("Física");
        radioButtonFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButtonFisicaActionPerformed(evt);
            }
        });

        buttonReceitaFederal.setText("R");
        buttonReceitaFederal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonReceitaFederalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8)
                    .addComponent(radioButtonJuridica))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldNome)
                    .addComponent(textFieldApelido, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(radioButtonFisica, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textFieldCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonReceitaFederal, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldApelido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(radioButtonFisica)
                    .addComponent(radioButtonJuridica)
                    .addComponent(jLabel9)
                    .addComponent(textFieldCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonReceitaFederal))
                .addContainerGap())
        );

        buttonNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/addBlue32x32.png"))); // NOI18N
        buttonNovo.setText("Novo");
        buttonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNovoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 802, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(panel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(panel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(6, 6, 6)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange

    }//GEN-LAST:event_formPropertyChange

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated

    }//GEN-LAST:event_formWindowActivated

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void buttonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNovoActionPerformed
        novo();
    }//GEN-LAST:event_buttonNovoActionPerformed

    private void radioButtonFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButtonFisicaActionPerformed
        formataMascaraJuridicaFisica();
    }//GEN-LAST:event_radioButtonFisicaActionPerformed

    private void radioButtonJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButtonJuridicaActionPerformed
        formataMascaraJuridicaFisica();
    }//GEN-LAST:event_radioButtonJuridicaActionPerformed

    private void buttonReceitaFederalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReceitaFederalActionPerformed
        String cnpj = textFieldCnpjCpf.getText();
        cnpj = cnpj.replaceAll("[./-]", "");
        System.out.println(cnpj);
        String site = "http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/Cnpjreva_Solicitacao2.asp?cnpj=" + cnpj;
        Singleton.getInstance().abrirSite(site);
    }//GEN-LAST:event_buttonReceitaFederalActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonNovo;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private javax.swing.JButton buttonReceitaFederal;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JRadioButton radioButtonFisica;
    private javax.swing.JRadioButton radioButtonJuridica;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTextField textFieldApelido;
    private javax.swing.JFormattedTextField textFieldCnpjCpf;
    private javax.swing.JTextField textFieldNome;
    // End of variables declaration//GEN-END:variables
}
