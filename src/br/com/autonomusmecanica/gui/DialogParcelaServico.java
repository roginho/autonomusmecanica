/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.FormaPgtoDAOJPA;
import br.com.autonomusmecanica.dao.ServicoDAOJPA;
import br.com.autonomusmecanica.dao.ServicoParcelaDAOJPA;
import br.com.autonomusmecanica.entidades.FormaPgto;
import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.ServicoParcela;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import bsh.util.Util;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Bianca
 */
public class DialogParcelaServico extends javax.swing.JDialog {

    private final Servico servico;
    private final ServicoParcela servicoParcela;
    private final Integer parcela;
    private Double saldo;

    private void salvar() {
        if (servico != null) {
            Double valor = Singleton.getInstance().converteStringParaDouble(txtValorParcela.getText());
            if (servicoParcela != null) {
                saldo += servicoParcela.getValorParcela();
            }
            saldo = Singleton.getInstance().arredondar(saldo, "0.00");
            System.out.println(saldo +" - "+ valor);
            if (valor <= saldo) {
                ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
                ServicoParcela sp = new ServicoParcela();
                sp.setServico(servico);
                sp.setQuitado(1);
                sp.setFormaPgto((FormaPgto) comboBoxFormaPagto.getSelectedItem());
                sp.setDataVcto(dChooser.getDate());
                sp.setDataPgto(dChooser.getDate());
                sp.setValorParcela(valor);
                sp.setValorPago(valor);
                sp.setParcela(parcela);
                sp.setObs(txtObs.getText());
                if (servicoParcela != null) {
                    sp.setIdServicoParcela(servicoParcela.getIdServicoParcela());
                }
                dao.save(sp);
                saldo = dao.getSaldoRestanteServico(servico);
                saldo = Singleton.getInstance().arredondar(saldo, "0.00");
                if (saldo == 0) {
                    ServicoDAOJPA daoServ = new ServicoDAOJPA();
                    servico.setStatus('f');
                    daoServ.save(servico);
                    
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "O valor invalido!!! Maior que o saldo restante", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            }
        }
//        if (save != null && save.getIdServicoParcela() != null) {
//            servico.setStatus('f');
//            ServicoDAOJPA daoS = new ServicoDAOJPA();
//            daoS.save(servico);
//            JOptionPane.showMessageDialog(this, "Parcelas geradas");
//            this.dispose();
//        }
    }

    private void carregaComboFormaPgto() {
        for (FormaPgto f : new FormaPgtoDAOJPA().getAll(FormaPgto.class)) {
            comboBoxFormaPagto.addItem(f);
        }
    }

    private void refresh() {
        if (servico != null) {
            lblServicoNum.setText(servico.getIdServico() + "");
            lblServicoParc.setText(parcela + "");
            lblTotal.setText(Singleton.getInstance().converteDoubleParaString(servico.getValorTotal(), 2));
            lblSaldo.setText(Singleton.getInstance().converteDoubleParaString(saldo, 2));
            dChooser.setDate(new Date());
            txtValorParcela.setText(Singleton.getInstance().converteDoubleParaString(saldo, 2));
            if (servicoParcela != null) {
                dChooser.setDate(servico.getDataServico());
                txtValorParcela.setText(Singleton.getInstance().converteDoubleParaString(servicoParcela.getValorParcela(), 2));
                txtObs.setText(servicoParcela.getObs());
                comboBoxFormaPagto.setSelectedItem(servicoParcela.getFormaPgto());
            }
        }
    }

    public DialogParcelaServico(java.awt.Frame parent, boolean modal, Servico s, ServicoParcela sp, int parc, double sald) {
        super(parent, modal);
        this.servico = s;
        this.servicoParcela = sp;
        this.parcela = parc;
        this.saldo = sald;
        if (servicoParcela != null) {
            this.saldo = servicoParcela.getValorParcela();
        }

        initComponents();
        SelectAllTheThings.addListeners(panel);
        SelectAllTheThings.addListeners(panel2);
        Singleton.getInstance().passaCamposComEnter(panel2);
        carregaComboFormaPgto();
        refresh();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        lblServicoNum = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblSaldo = new javax.swing.JLabel();
        lblServicoParc = new javax.swing.JLabel();
        panel2 = new javax.swing.JPanel();
        txtValorParcela = new javax.swing.JFormattedTextField();
        comboBoxFormaPagto = new javax.swing.JComboBox();
        dChooser = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObs = new javax.swing.JTextArea();
        btnSalvar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblServicoNum.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblServicoNum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblServicoNum.setToolTipText("");
        lblServicoNum.setBorder(javax.swing.BorderFactory.createTitledBorder("Nº do Serviço"));

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("0,00");
        lblTotal.setToolTipText("");
        lblTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        lblSaldo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblSaldo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSaldo.setText("0,00");
        lblSaldo.setToolTipText("");
        lblSaldo.setBorder(javax.swing.BorderFactory.createTitledBorder("Saldo"));

        lblServicoParc.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblServicoParc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblServicoParc.setToolTipText("");
        lblServicoParc.setBorder(javax.swing.BorderFactory.createTitledBorder("Nº da Parcela"));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lblServicoNum, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblServicoParc, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(102, Short.MAX_VALUE))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblServicoParc, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblServicoNum, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotal)
                            .addComponent(lblSaldo))))
                .addGap(51, 51, 51))
        );

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        txtValorParcela.setBorder(javax.swing.BorderFactory.createTitledBorder("Valor da parcela"));
        txtValorParcela.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txtValorParcela.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtValorParcela.setText("0,00");
        txtValorParcela.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtValorParcela.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtValorParcelaFocusLost(evt);
            }
        });

        comboBoxFormaPagto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        comboBoxFormaPagto.setBorder(javax.swing.BorderFactory.createTitledBorder("Forma de pagamento"));
        comboBoxFormaPagto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxFormaPagtoItemStateChanged(evt);
            }
        });
        comboBoxFormaPagto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxFormaPagtoActionPerformed(evt);
            }
        });

        dChooser.setBorder(javax.swing.BorderFactory.createTitledBorder("Data"));

        txtObs.setColumns(20);
        txtObs.setRows(5);
        jScrollPane1.setViewportView(txtObs);

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboBoxFormaPagto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(dChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtValorParcela, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValorParcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboBoxFormaPagto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Apply.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Close.png"))); // NOI18N
        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20, 20, 20))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxFormaPagtoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoItemStateChanged

    }//GEN-LAST:event_comboBoxFormaPagtoItemStateChanged

    private void comboBoxFormaPagtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoActionPerformed

    }//GEN-LAST:event_comboBoxFormaPagtoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void txtValorParcelaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtValorParcelaFocusLost
    }//GEN-LAST:event_txtValorParcelaFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox comboBoxFormaPagto;
    private com.toedter.calendar.JDateChooser dChooser;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblSaldo;
    private javax.swing.JLabel lblServicoNum;
    private javax.swing.JLabel lblServicoParc;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel panel;
    private javax.swing.JPanel panel2;
    private javax.swing.JTextArea txtObs;
    private javax.swing.JFormattedTextField txtValorParcela;
    // End of variables declaration//GEN-END:variables
}
