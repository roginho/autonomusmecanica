package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.ClienteDAOJPA;
import br.com.autonomusmecanica.dao.EmailDAOJPA;
import br.com.autonomusmecanica.dao.EnderecoDAOJPA;
import br.com.autonomusmecanica.dao.PessoaDAOJPA;
import br.com.autonomusmecanica.dao.TelefoneDAOJPA;
import br.com.autonomusmecanica.dao.VeiculoDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Telefone;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.ColorRenderer;
import br.com.autonomusmecanica.tableModel.TableModelEmail;
import br.com.autonomusmecanica.tableModel.TableModelEndereco;
import br.com.autonomusmecanica.tableModel.TableModelTelefone;
import br.com.autonomusmecanica.tableModel.TableModelVeiculo;
import br.com.roger.validador.Validador;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author roger
 */
public final class DialogCadastroCliente extends javax.swing.JDialog {

    private Pessoa pessoa;
    private Cliente cliente;
    private TableModelEmail modelEmail;
    private TableModelTelefone modelTelefone;
    private TableModelEndereco modelEndereco;
    private TableModelVeiculo modelVeiculo;
    private final Integer colAltEmail = 1;
    private final Integer colExcEmail = 2;
    private final Integer colAltTel = 2;
    private final Integer colExcTel = 3;
    private final Integer colAltEnd = 7;
    private final Integer colExcEnd = 8;
    private final Integer colCor = 3;
    private final Integer colAltVeic = 5;
    private final Integer colExcVeic = 6;

    private final TableCellRenderer renderer = new StripedTable();

    private boolean pessoaIsNull() {
        Boolean valida;
        valida = this.pessoa == null;
        return valida;
    }

    private boolean clienteIsNull() {
        boolean valida;
        valida = this.cliente == null;
        return valida;
    }

    private void formataMascaraJuridicaFisica() {
        try {
            if (radioButtonJuridica.isSelected()) {
                textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
            }
            if (radioButtonFisica.isSelected()) {
                textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
            }
            if (this.cliente != null) {
                textFieldCnpjCpf.setText(this.cliente.getPessoa().getCpfCnpj());
            }
        } catch (ParseException ex) {
            Logger.getLogger(DialogCadastroCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void atualizaTabelaEmail() {
        EmailDAOJPA dao = new EmailDAOJPA();
        List<Email> lista = dao.getPessoaEmail(pessoa);
        modelEmail = new TableModelEmail(lista);
        tableEmail.setModel(modelEmail);
        //model.ordenarPorEmail();
        tableEmail.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableEmail.getColumnModel().getColumn(colAltEmail).setPreferredWidth(30);
        tableEmail.getColumnModel().getColumn(colExcEmail).setPreferredWidth(30);
        tableEmail.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEmail, colAltEmail, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEmail, colExcEmail, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void atualizaTabelaTelefone() {
        TelefoneDAOJPA dao = new TelefoneDAOJPA();
        List<Telefone> lista = dao.getPessoaTelefone(pessoa);
        modelTelefone = new TableModelTelefone(lista);
        tableTelefone.setModel(modelTelefone);
        tableTelefone.getColumnModel().getColumn(0).setPreferredWidth(80);
        tableTelefone.getColumnModel().getColumn(1).setPreferredWidth(270);
        tableTelefone.getColumnModel().getColumn(colAltTel).setPreferredWidth(30);
        tableTelefone.getColumnModel().getColumn(colExcTel).setPreferredWidth(30);
        tableTelefone.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableTelefone, colAltTel, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableTelefone, colExcTel, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void atualizaTabelaEndereco() {
        EnderecoDAOJPA dao = new EnderecoDAOJPA();
        List<Endereco> lista = dao.getPessoaEndereco(pessoa);
        modelEndereco = new TableModelEndereco(lista);
        tableEndereco.setModel(modelEndereco);
        tableEndereco.getColumnModel().getColumn(0).setPreferredWidth(200);
        tableEndereco.getColumnModel().getColumn(1).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(2).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(3).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setPreferredWidth(0);
        tableEndereco.getColumnModel().getColumn(4).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(5).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(6).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setPreferredWidth(0);
        tableEndereco.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEndereco, colAltEnd, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEndereco, colExcEnd, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void atualizaTabelaVeiculo() {
        VeiculoDAOJPA dao = new VeiculoDAOJPA();
        List<Veiculo> lista = dao.getVeiculosCliente(cliente);
        modelVeiculo = new TableModelVeiculo(lista);
        tableVeiculo.setModel(modelVeiculo);
        tableVeiculo.getColumnModel().getColumn(0).setPreferredWidth(30);
        tableVeiculo.getColumnModel().getColumn(1).setPreferredWidth(20);
        tableVeiculo.getColumnModel().getColumn(2).setPreferredWidth(20);
        tableVeiculo.getColumnModel().getColumn(colCor).setPreferredWidth(20);
        tableVeiculo.getColumnModel().getColumn(4).setPreferredWidth(200);
        tableVeiculo.getColumnModel().getColumn(colAltVeic).setMinWidth(0);
        tableVeiculo.getColumnModel().getColumn(colAltVeic).setMaxWidth(0);
        tableVeiculo.getColumnModel().getColumn(colAltVeic).setPreferredWidth(0);
        tableVeiculo.getColumnModel().getColumn(colExcVeic).setPreferredWidth(20);
        tableVeiculo.setDefaultRenderer(Object.class, renderer);
        ColorRenderer colorRenderer = new ColorRenderer(true);
        TableColumn column = tableVeiculo.getColumnModel().getColumn(colCor);
        column.setCellRenderer(colorRenderer);
        new ButtonColumn(tableVeiculo, colExcVeic, Singleton.getInstance().iconeJTable("Excluir"));
    }

    private void salvar() {
        String cnpjCpf = null;
        if (textFieldNome.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o nome:");
            textFieldNome.requestFocus();
            return;
        }
        Character tipo = null;
        if (radioButtonJuridica.isSelected()) {
            tipo = 'j';
        }
        if (radioButtonFisica.isSelected()) {
            tipo = 'f';
        }

        boolean temNumeros = Singleton.getInstance().temNumeros(textFieldCnpjCpf.getText());
        Boolean validaCnpjCpf = true;
        if (temNumeros) {
            cnpjCpf = textFieldCnpjCpf.getText();
            if (tipo.equals('j')) {
                validaCnpjCpf = Validador.validaCnpj(textFieldCnpjCpf.getText());
                if (!validaCnpjCpf) {
                    JOptionPane.showMessageDialog(this, "CNPJ Inválido", "Alerta", JOptionPane.WARNING_MESSAGE);
                    textFieldCnpjCpf.setValue("");
                    formataMascaraJuridicaFisica();
                    textFieldCnpjCpf.requestFocus();
                }
            }
            if (tipo.equals('f')) {
                validaCnpjCpf = Validador.validaCpf(textFieldCnpjCpf.getText());
                if (!validaCnpjCpf) {
                    JOptionPane.showMessageDialog(this, "CPF Inválido", "Alerta", JOptionPane.WARNING_MESSAGE);
                    textFieldCnpjCpf.setValue("");
                    formataMascaraJuridicaFisica();
                    textFieldCnpjCpf.requestFocus();
                }
            }
        }
        if (validaCnpjCpf) {
            String mensagem;
            Pessoa p = new Pessoa();
            Cliente e = new Cliente();
            if (!clienteIsNull()) {
                mensagem = "Registro atualizado com sucesso";
                p = this.pessoa;
                e = this.cliente;
            } else {
                mensagem = "Registro inserido com sucesso";
            }
            p.setNome(textFieldNome.getText());
            p.setApelidoNomeFantasia(textFieldApelido.getText());
            p.setCpfCnpj(cnpjCpf);
            p.setTipo(tipo);
            p.setStatus('a');
            e.setPessoa(p);
            ClienteDAOJPA dao = new ClienteDAOJPA();
            PessoaDAOJPA dao2 = new PessoaDAOJPA();

            List clientes;
            if (cliente == null) {
                clientes = dao2.getExiste(textFieldNome.getText(), -1);
            } else {
                clientes = dao2.getExiste(textFieldNome.getText(), cliente.getPessoa().getIdPessoa());
            }
            if (clientes.isEmpty()) {
                p = dao2.save(p);
                e.setPessoa(p);
                e = dao.save(e);
                this.pessoa = p;
                this.cliente = e;
                JOptionPane.showMessageDialog(this, mensagem);
//                dispose();
            } else {
                int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Este nome já existe, vincular ha mesma pessoa ?", "", JOptionPane.YES_NO_OPTION);
                if (showConfirmDialog == 0) {
                    p = (Pessoa) clientes.get(0);
                    p = dao2.save(p);
                    e.setPessoa(p);
                    e = dao.save(e);
                    this.pessoa = p;
                    this.cliente = e;
                    JOptionPane.showMessageDialog(this, mensagem);
  //                  dispose();
                }
                //JOptionPane.showMessageDialog(this, "Este nome já existe, salve com um nome diferente", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
            refreshForm();
        }
    }

    private void refreshForm() {
        limpaTela();
        if (!clienteIsNull()) {
            labelCodigo.setText("" + this.cliente.getIdCliente());
            textFieldNome.setText(this.cliente.getPessoa().getNome());
            textFieldApelido.setText(this.cliente.getPessoa().getApelidoNomeFantasia());
            if (this.cliente.getPessoa().getTipo() == 'j') {
                radioButtonJuridica.setSelected(true);
            } else {
                radioButtonFisica.setSelected(true);
            }
            formataMascaraJuridicaFisica();
            textFieldCnpjCpf.setText(this.cliente.getPessoa().getCpfCnpj());
            atualizaTabelaTelefone();
            atualizaTabelaEmail();
            atualizaTabelaEndereco();
            atualizaTabelaVeiculo();
        }
    }

    private void limpaTela() {
        labelCodigo.setText("");
        radioButtonFisica.setSelected(true);
        textFieldNome.setText("");
        textFieldApelido.setText("");
        textFieldCnpjCpf.setValue("");
        new TableModelEmail().limpaLista();
        new TableModelTelefone().limpaLista();
        new TableModelEndereco().limpaLista();
    }

    private void novo() {
        this.limpaTela();
        this.cliente = null;
        this.pessoa = null;
        atualizaTabelaEndereco();
        atualizaTabelaEmail();
        atualizaTabelaTelefone();
        atualizaTabelaVeiculo();
        radioButtonFisica.setSelected(true);
        formataMascaraJuridicaFisica();
    }

    private void buscaPrimeiroRegistro() {
        ClienteDAOJPA dao = new ClienteDAOJPA();
        this.cliente = (Cliente) dao.primeiroRegistro(Cliente.class);
        this.pessoa = cliente.getPessoa();
        refreshForm();
    }

    private void buscaUltimoRegistro() {
        ClienteDAOJPA dao = new ClienteDAOJPA();
        this.cliente = (Cliente) dao.ultimoRegistro(Cliente.class, "idCliente");
        this.pessoa = cliente.getPessoa();
        refreshForm();
    }

    private void proximo() {
        if (cliente == null) {
            buscaUltimoRegistro();
        } else {
            ClienteDAOJPA dao = new ClienteDAOJPA();
            this.cliente = (Cliente) dao.getProximo(Cliente.class, "idCliente", cliente.getIdCliente());
            this.pessoa = cliente.getPessoa();
            if (cliente != null) {
                refreshForm();
            }
        }
    }

    private void anterior() {
        if (cliente == null) {
            buscaPrimeiroRegistro();
        } else {
            ClienteDAOJPA dao = new ClienteDAOJPA();
            this.cliente = (Cliente) dao.getAnterior(Cliente.class, "idCliente", cliente.getIdCliente());
            this.pessoa = cliente.getPessoa();
            if (cliente != null) {
                refreshForm();
            }
        }
    }

    private void adicionaEmail() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail:", "Inserir");
            d.setTitle("Cadastrar Email");
            d.setVisible(true);
            EmailDAOJPA dao = new EmailDAOJPA();
            Email e = new Email();
            e.setPessoa((Pessoa) d.resultado);
            e.setEmail(d.descricao);
            if (e.getEmail() != null) {
                dao.save(e);
                atualizaTabelaEmail();
            }
        }
    }

    private void alteraEmail() {
        if (!pessoaIsNull()) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail", "Salvar");
                    d.setDescricao(email.getEmail());
                    d.setTitle("Cadastrar Email");
                    d.setVisible(true);
                    EmailDAOJPA dao = new EmailDAOJPA();
                    email.setPessoa((Pessoa) d.resultado);
                    email.setEmail(d.descricao);
                    dao.save(email);
                    atualizaTabelaEmail();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEmail() {
        if (!pessoaIsNull()) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o e-mail " + tableEmail.getValueAt(linha, 0) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EmailDAOJPA dao = new EmailDAOJPA();
                        dao.remove(Email.class, email.getIdEmail());
                        atualizaTabelaEmail();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void adicionaTelefone() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            Telefone telefone = new Telefone();
            telefone.setPessoa(pessoa);
            DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Inserir");
            d.setTitle("Cadastrar Telefone");
            d.setVisible(true);
            TelefoneDAOJPA dao = new TelefoneDAOJPA();
            telefone.setPessoa(pessoa);
            telefone.setNumero(d.t.getNumero());
            telefone.setObs(d.t.getObs());
            if (d.t.getNumero() != null) {
                dao.save(telefone);
                atualizaTabelaTelefone();
            }
        }
    }

    private void alteraTelefone() {
        if (!pessoaIsNull()) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Salvar");
                    d.setTitle("Cadastrar Telefone");
                    d.setVisible(true);
                    TelefoneDAOJPA dao = new TelefoneDAOJPA();
                    telefone.setPessoa(telefone.getPessoa());
                    telefone.setNumero(d.t.getNumero());
                    telefone.setObs(d.t.getObs());
                    dao.save(telefone);
                    atualizaTabelaTelefone();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiTelefone() {
        if (!pessoaIsNull()) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o telefone " + tableTelefone.getValueAt(linha, 0) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        TelefoneDAOJPA dao = new TelefoneDAOJPA();
                        dao.remove(Telefone.class, telefone.getIdTelefone());
                        atualizaTabelaTelefone();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void adicionaEndereco() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            Endereco e = new Endereco();
            e.setPessoa(this.pessoa);
            DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Inserir");
            d.setTitle("Cadastrar Endereco");
            d.setVisible(true);
            EnderecoDAOJPA dao = new EnderecoDAOJPA();
            e.setPessoa(pessoa);
            e.setLogradouro(d.endereco.getLogradouro());
            e.setNumero(d.endereco.getNumero());
            e.setComplemento(d.endereco.getComplemento());
            e.setBairro(d.endereco.getBairro());
            e.setCep(d.endereco.getCep());
            e.setCidade(d.endereco.getCidade());
            if (e.getLogradouro() != null && e.getCidade() != null) {
                dao.save(e);
            }
        }
        atualizaTabelaEndereco();
    }

    private void alteraEndereco() {
        if (!pessoaIsNull()) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco e = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (e.getIdEndereco() != null) {
                    DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Salvar");
                    d.setTitle("Alterar Endereco");
                    d.setVisible(true);
                    EnderecoDAOJPA dao = new EnderecoDAOJPA();
                    e.setPessoa(pessoa);
                    e.setLogradouro(d.endereco.getLogradouro());
                    e.setNumero(d.endereco.getNumero());
                    e.setComplemento(d.endereco.getComplemento());
                    e.setBairro(d.endereco.getBairro());
                    e.setCep(d.endereco.getCep());
                    e.setCidade(d.endereco.getCidade());
                    dao.save(e);
                    atualizaTabelaEndereco();

                } else {
                    JOptionPane.showMessageDialog(this, "Sem endereco para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEndereco() {
        if (!pessoaIsNull()) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco endereco = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (endereco.getIdEndereco() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o endereço " + tableEndereco.getValueAt(linha, 0) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EnderecoDAOJPA dao = new EnderecoDAOJPA();
                        dao.remove(Endereco.class, endereco.getIdEndereco());
//                        JOptionPane.showMessageDialog(this,"Registro removido");
                        atualizaTabelaEndereco();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem Endereco para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void adicionaVeiculo() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            DialogTabelaVeiculo d = new DialogTabelaVeiculo(null, true, cliente);
            d.setVisible(true);
        }
        atualizaTabelaVeiculo();
    }

    private void excluiVeiculo() {
        if (!pessoaIsNull()) {
            int linha = tableVeiculo.getSelectedRow();
            if (linha >= 0) {
                Veiculo v = (Veiculo) tableVeiculo.getValueAt(linha, -1);
                if (v.getIdVeiculo() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o veículo " + tableVeiculo.getValueAt(linha, 0) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        VeiculoDAOJPA dao = new VeiculoDAOJPA();
                        v.setCliente(null);
                        dao.save(v);
                        atualizaTabelaVeiculo();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem veículo para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    public DialogCadastroCliente(java.awt.Frame parent, boolean modal, Cliente e) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        Singleton.getInstance().passaCamposComEnter(tabbedPane);
        if (e == null) {
            novo();
        } else {
            this.cliente = e;
            this.pessoa = e.getPessoa();
            refreshForm();
        }
        textFieldNome.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTipo = new javax.swing.ButtonGroup();
        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        panel2 = new javax.swing.JPanel();
        textFieldNome = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        textFieldApelido = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        textFieldCnpjCpf = new javax.swing.JFormattedTextField();
        radioButtonJuridica = new javax.swing.JRadioButton();
        radioButtonFisica = new javax.swing.JRadioButton();
        buttonNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();
        panelVeic = new javax.swing.JPanel();
        ScrollPaneEnd1 = new javax.swing.JScrollPane();
        tableVeiculo = new javax.swing.JTable();
        buttonAddEnd1 = new javax.swing.JButton();
        panelEnd = new javax.swing.JPanel();
        ScrollPaneEnd = new javax.swing.JScrollPane();
        tableEndereco = new javax.swing.JTable();
        buttonAddEnd = new javax.swing.JButton();
        paneFone = new javax.swing.JPanel();
        ScrollPaneFone = new javax.swing.JScrollPane();
        tableTelefone = new javax.swing.JTable();
        buttonAddFone = new javax.swing.JButton();
        paneEmail = new javax.swing.JPanel();
        ScrollPaneEmail = new javax.swing.JScrollPane();
        tableEmail = new javax.swing.JTable();
        buttonAddEmail = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Clientes"));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeClientes64x64.png"))); // NOI18N

        labelCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(198, 198, 198)
                .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonPrev)
                    .addComponent(buttonNext)
                    .addComponent(buttonNext1)
                    .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Salvar");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Gerais"));

        jLabel5.setText("Apelido:");

        jLabel8.setText("Nome/Razão Social:");

        jLabel9.setText("CNPJ/CPF:");

        try {
            textFieldCnpjCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        buttonGroupTipo.add(radioButtonJuridica);
        radioButtonJuridica.setText("Juririca");
        radioButtonJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButtonJuridicaActionPerformed(evt);
            }
        });

        buttonGroupTipo.add(radioButtonFisica);
        radioButtonFisica.setSelected(true);
        radioButtonFisica.setText("Física");
        radioButtonFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButtonFisicaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textFieldApelido)
                            .addComponent(textFieldNome)))
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(radioButtonFisica, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radioButtonJuridica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addGap(4, 4, 4)
                        .addComponent(textFieldCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldApelido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldCnpjCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(radioButtonJuridica)
                    .addComponent(radioButtonFisica))
                .addContainerGap())
        );

        buttonNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/addBlue32x32.png"))); // NOI18N
        buttonNovo.setText("Novo");
        buttonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNovoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        tableVeiculo.setBackground(java.awt.Color.lightGray);
        tableVeiculo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "Placa", "Modelo", "Ano", "Cor", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableVeiculo.setFocusable(false);
        tableVeiculo.setGridColor(java.awt.Color.lightGray);
        tableVeiculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableVeiculoMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableVeiculoMouseClicked(evt);
            }
        });
        ScrollPaneEnd1.setViewportView(tableVeiculo);

        buttonAddEnd1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEnd1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEnd1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVeicLayout = new javax.swing.GroupLayout(panelVeic);
        panelVeic.setLayout(panelVeicLayout);
        panelVeicLayout.setHorizontalGroup(
            panelVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneEnd1, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
            .addGroup(panelVeicLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddEnd1))
        );
        panelVeicLayout.setVerticalGroup(
            panelVeicLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVeicLayout.createSequentialGroup()
                .addComponent(buttonAddEnd1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneEnd1, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Veículos", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/icone_car.24.png")), panelVeic); // NOI18N

        tableEndereco.setBackground(java.awt.Color.lightGray);
        tableEndereco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "Logradouro", "numero", "Complemento", "Bairro", "Cidade", "Estado", "Pais", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableEndereco.setFocusable(false);
        tableEndereco.setGridColor(java.awt.Color.lightGray);
        tableEndereco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseClicked(evt);
            }
        });
        ScrollPaneEnd.setViewportView(tableEndereco);

        buttonAddEnd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEndActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelEndLayout = new javax.swing.GroupLayout(panelEnd);
        panelEnd.setLayout(panelEndLayout);
        panelEndLayout.setHorizontalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
            .addGroup(panelEndLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddEnd))
        );
        panelEndLayout.setVerticalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEndLayout.createSequentialGroup()
                .addComponent(buttonAddEnd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Endereços", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Home.png")), panelEnd); // NOI18N

        tableTelefone.setBackground(java.awt.Color.lightGray);
        tableTelefone.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "Telefone", "Observações", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableTelefone.setFocusable(false);
        tableTelefone.setGridColor(java.awt.Color.lightGray);
        tableTelefone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseClicked(evt);
            }
        });
        ScrollPaneFone.setViewportView(tableTelefone);

        buttonAddFone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddFone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddFoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneFoneLayout = new javax.swing.GroupLayout(paneFone);
        paneFone.setLayout(paneFoneLayout);
        paneFoneLayout.setHorizontalGroup(
            paneFoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneFone, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
            .addGroup(paneFoneLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddFone))
        );
        paneFoneLayout.setVerticalGroup(
            paneFoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneFoneLayout.createSequentialGroup()
                .addComponent(buttonAddFone)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneFone, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Telefones", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Phone number.png")), paneFone); // NOI18N

        tableEmail.setBackground(java.awt.Color.lightGray);
        tableEmail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "E-mail", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableEmail.setAutoscrolls(false);
        tableEmail.setFocusable(false);
        tableEmail.setGridColor(java.awt.Color.lightGray);
        tableEmail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEmailMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEmailMouseClicked(evt);
            }
        });
        ScrollPaneEmail.setViewportView(tableEmail);

        buttonAddEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEmailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneEmailLayout = new javax.swing.GroupLayout(paneEmail);
        paneEmail.setLayout(paneEmailLayout);
        paneEmailLayout.setHorizontalGroup(
            paneEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 797, Short.MAX_VALUE)
            .addGroup(paneEmailLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddEmail))
        );
        paneEmailLayout.setVerticalGroup(
            paneEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneEmailLayout.createSequentialGroup()
                .addComponent(buttonAddEmail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Email", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/E-mail.png")), paneEmail); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbedPane)
                .addContainerGap())
        );

        tabbedPane.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(1, 1, 1))
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(panel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange

    }//GEN-LAST:event_formPropertyChange

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated

    }//GEN-LAST:event_formWindowActivated

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void buttonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNovoActionPerformed
        novo();
    }//GEN-LAST:event_buttonNovoActionPerformed

    private void radioButtonFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButtonFisicaActionPerformed
        formataMascaraJuridicaFisica();
    }//GEN-LAST:event_radioButtonFisicaActionPerformed

    private void radioButtonJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButtonJuridicaActionPerformed
        formataMascaraJuridicaFisica();
    }//GEN-LAST:event_radioButtonJuridicaActionPerformed

    private void tableEmailMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseReleased

    }//GEN-LAST:event_tableEmailMouseReleased

    private void tableEmailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseClicked
        if (tableEmail.getSelectedColumn() == colAltEmail) {
            alteraEmail();
        } else if (tableEmail.getSelectedColumn() == colExcEmail) {
            excluiEmail();
        }
    }//GEN-LAST:event_tableEmailMouseClicked

    private void tableTelefoneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableTelefoneMouseReleased

    private void tableTelefoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseClicked
        if (tableTelefone.getSelectedColumn() == colAltTel) {
            alteraTelefone();
        } else if (tableTelefone.getSelectedColumn() == colExcTel) {
            excluiTelefone();
        }
    }//GEN-LAST:event_tableTelefoneMouseClicked

    private void tableEnderecoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableEnderecoMouseReleased

    private void tableEnderecoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseClicked
        if (tableEndereco.getSelectedColumn() == colAltEnd) {
            alteraEndereco();
        } else if (tableEndereco.getSelectedColumn() == colExcEnd) {
            excluiEndereco();
        }
    }//GEN-LAST:event_tableEnderecoMouseClicked

    private void buttonAddEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEndActionPerformed
        adicionaEndereco();
    }//GEN-LAST:event_buttonAddEndActionPerformed

    private void buttonAddFoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddFoneActionPerformed
        adicionaTelefone();
    }//GEN-LAST:event_buttonAddFoneActionPerformed

    private void buttonAddEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEmailActionPerformed
        adicionaEmail();
    }//GEN-LAST:event_buttonAddEmailActionPerformed

    private void tableVeiculoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableVeiculoMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableVeiculoMouseReleased

    private void tableVeiculoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableVeiculoMouseClicked
        if (tableVeiculo.getSelectedColumn() == colExcVeic) {
            excluiVeiculo();
        }
    }//GEN-LAST:event_tableVeiculoMouseClicked

    private void buttonAddEnd1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEnd1ActionPerformed
        adicionaVeiculo();
    }//GEN-LAST:event_buttonAddEnd1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPaneEmail;
    private javax.swing.JScrollPane ScrollPaneEnd;
    private javax.swing.JScrollPane ScrollPaneEnd1;
    private javax.swing.JScrollPane ScrollPaneFone;
    private javax.swing.JButton buttonAddEmail;
    private javax.swing.JButton buttonAddEnd;
    private javax.swing.JButton buttonAddEnd1;
    private javax.swing.JButton buttonAddFone;
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonNovo;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JPanel paneEmail;
    private javax.swing.JPanel paneFone;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JPanel panelEnd;
    private javax.swing.JPanel panelVeic;
    private javax.swing.JRadioButton radioButtonFisica;
    private javax.swing.JRadioButton radioButtonJuridica;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTable tableEmail;
    private javax.swing.JTable tableEndereco;
    private javax.swing.JTable tableTelefone;
    private javax.swing.JTable tableVeiculo;
    private javax.swing.JTextField textFieldApelido;
    private javax.swing.JFormattedTextField textFieldCnpjCpf;
    private javax.swing.JTextField textFieldNome;
    // End of variables declaration//GEN-END:variables
}
