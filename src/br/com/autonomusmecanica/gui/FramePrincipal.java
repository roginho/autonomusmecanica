package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.ConnectionFactory;
import br.com.autonomusmecanica.dao.EmpresaDAOJPA;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Empresa;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Usuario;
import br.com.autonomusmecanica.funcoes.JImagePanel;
import br.com.roger.utils.Singleton;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author roger
 */
public class FramePrincipal extends javax.swing.JFrame {

    private String isAdmin = "";

    private void setImgFundo() {
        String logo = Singleton.getInstance().caminhoLogo();
        if (logo != null && !logo.isEmpty()) {
            try {
                panelFundo = new JImagePanel(logo);
                this.getContentPane().add(panelFundo);
//                this.getGlassPane().setBackground();
            } catch (IOException ex) {
                Logger.getLogger(FramePrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public FramePrincipal(Usuario u) {
        this.setExtendedState(FramePrincipal.MAXIMIZED_BOTH);
        initComponents();
        if (u != null) {
            if (u.getAdministrador().equals('a')) {
                isAdmin = "Administrador";
            } else {
                isAdmin = "Usuário";
            }
            labelUsuario.setText("Usuário: " + u.getLogin() + " ---- " + isAdmin);
            EmpresaDAOJPA dao = new EmpresaDAOJPA();
            Empresa empresa = (Empresa) dao.primeiroRegistro(Empresa.class);
            Pessoa pessoa = empresa.getPessoa();
            String dados = "";
            if (pessoa.getApelidoNomeFantasia() != null) {
                dados = "<font size='15'>"+pessoa.getApelidoNomeFantasia() + "</font><br>";
            }
            if (pessoa.getEnderecoCompletoPessoa() != null) {
                dados += pessoa.getEnderecoCompletoPessoa() + "<br>";
            }
            dados += pessoa.getTelefonePessoa()+"<br>";
            if (!pessoa.getEmailCollection().isEmpty()) {
                for (Email e : pessoa.getEmailCollection()) {
                    dados += e.getEmail() + "<br>";
                }
            }
            Singleton.getInstance().setDadosEmpresa(dados);
            setImgFundo();
        }

    }

    private static BufferedImage loadImage(String file) throws IOException {
        return ImageIO.read(new File(file));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelCadastro = new javax.swing.JPanel();
        btnUsuario = new javax.swing.JButton();
        btnEmpresa = new javax.swing.JButton();
        btnFabricante = new javax.swing.JButton();
        btnModelo = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        btnVeiculos = new javax.swing.JButton();
        btnClientes = new javax.swing.JButton();
        btnOrcamentos = new javax.swing.JButton();
        btnServicos = new javax.swing.JButton();
        panelFinanceiro = new javax.swing.JPanel();
        btnFormaPgto = new javax.swing.JButton();
        panelRelatorio = new javax.swing.JPanel();
        btnRelatorioServico = new javax.swing.JButton();
        btnRelatorioServico1 = new javax.swing.JButton();
        panelConfiguracao = new javax.swing.JPanel();
        buttonConfiguracao = new javax.swing.JButton();
        jBtnFazerBackup = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        labelUsuario = new javax.swing.JLabel();
        panelFundo = new javax.swing.JPanel();
        buttonSair = new org.jdesktop.swingx.JXHyperlink();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeClientes64x64.png"))); // NOI18N
        btnUsuario.setText("Usuário");
        btnUsuario.setToolTipText("Cadastro de Usuários");
        btnUsuario.setBorder(null);
        btnUsuario.setContentAreaFilled(false);
        btnUsuario.setDefaultCapable(false);
        btnUsuario.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnUsuario.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnUsuario.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsuarioActionPerformed(evt);
            }
        });

        btnEmpresa.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnEmpresa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/empresa64x64.png"))); // NOI18N
        btnEmpresa.setText("Empresa");
        btnEmpresa.setToolTipText("Cadastro de Empresas");
        btnEmpresa.setBorder(null);
        btnEmpresa.setContentAreaFilled(false);
        btnEmpresa.setDefaultCapable(false);
        btnEmpresa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEmpresa.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnEmpresa.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmpresaActionPerformed(evt);
            }
        });

        btnFabricante.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnFabricante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/marcasCarros64.png"))); // NOI18N
        btnFabricante.setText("Fabricante");
        btnFabricante.setToolTipText("Fabricante");
        btnFabricante.setBorder(null);
        btnFabricante.setContentAreaFilled(false);
        btnFabricante.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFabricante.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnFabricante.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFabricante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFabricanteActionPerformed(evt);
            }
        });

        btnModelo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/modelosCarros64.png"))); // NOI18N
        btnModelo.setText("Modelos");
        btnModelo.setToolTipText("Modelos");
        btnModelo.setBorder(null);
        btnModelo.setContentAreaFilled(false);
        btnModelo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModelo.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnModelo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModeloActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/cor64.png"))); // NOI18N
        jButton4.setText("Cores");
        jButton4.setToolTipText("Cores");
        jButton4.setBorder(null);
        jButton4.setContentAreaFilled(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        btnVeiculos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/placa64.png"))); // NOI18N
        btnVeiculos.setText("Veiculos");
        btnVeiculos.setToolTipText("Veiculos");
        btnVeiculos.setBorder(null);
        btnVeiculos.setContentAreaFilled(false);
        btnVeiculos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVeiculos.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnVeiculos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVeiculosActionPerformed(evt);
            }
        });

        btnClientes.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeClientes64x64.png"))); // NOI18N
        btnClientes.setText("Clientes");
        btnClientes.setToolTipText("Cadastro de Clientes");
        btnClientes.setBorder(null);
        btnClientes.setContentAreaFilled(false);
        btnClientes.setDefaultCapable(false);
        btnClientes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnClientes.setIconTextGap(1);
        btnClientes.setName(""); // NOI18N
        btnClientes.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnClientes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        btnOrcamentos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnOrcamentos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/orcamento.png"))); // NOI18N
        btnOrcamentos.setText("Orçamentos");
        btnOrcamentos.setToolTipText("Orçamentos");
        btnOrcamentos.setBorder(null);
        btnOrcamentos.setContentAreaFilled(false);
        btnOrcamentos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOrcamentos.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnOrcamentos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOrcamentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrcamentosActionPerformed(evt);
            }
        });

        btnServicos.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnServicos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/carMecanic64.png"))); // NOI18N
        btnServicos.setText("Serviços");
        btnServicos.setToolTipText("Serviços");
        btnServicos.setBorder(null);
        btnServicos.setContentAreaFilled(false);
        btnServicos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnServicos.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnServicos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnServicos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCadastroLayout = new javax.swing.GroupLayout(panelCadastro);
        panelCadastro.setLayout(panelCadastroLayout);
        panelCadastroLayout.setHorizontalGroup(
            panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnVeiculos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(btnServicos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOrcamentos, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFabricante, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEmpresa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(134, Short.MAX_VALUE))
        );
        panelCadastroLayout.setVerticalGroup(
            panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelCadastroLayout.createSequentialGroup()
                        .addGroup(panelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnFabricante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnModelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnVeiculos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnEmpresa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnServicos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUsuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnOrcamentos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Cadastro", panelCadastro);

        btnFormaPgto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnFormaPgto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/formaPgto.png"))); // NOI18N
        btnFormaPgto.setText("Formas de Pagamento");
        btnFormaPgto.setToolTipText("Formas de pagamentos");
        btnFormaPgto.setBorder(null);
        btnFormaPgto.setContentAreaFilled(false);
        btnFormaPgto.setDefaultCapable(false);
        btnFormaPgto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFormaPgto.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnFormaPgto.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFormaPgto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFormaPgtoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFinanceiroLayout = new javax.swing.GroupLayout(panelFinanceiro);
        panelFinanceiro.setLayout(panelFinanceiroLayout);
        panelFinanceiroLayout.setHorizontalGroup(
            panelFinanceiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFinanceiroLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(btnFormaPgto, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(894, Short.MAX_VALUE))
        );
        panelFinanceiroLayout.setVerticalGroup(
            panelFinanceiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFinanceiroLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFormaPgto)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Financeiro", panelFinanceiro);

        btnRelatorioServico.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRelatorioServico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeRelatorio64x64.png"))); // NOI18N
        btnRelatorioServico.setText("Relatórios Serviços");
        btnRelatorioServico.setToolTipText("Relatórios Serviços");
        btnRelatorioServico.setBorder(null);
        btnRelatorioServico.setContentAreaFilled(false);
        btnRelatorioServico.setDefaultCapable(false);
        btnRelatorioServico.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRelatorioServico.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnRelatorioServico.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRelatorioServico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioServicoActionPerformed(evt);
            }
        });

        btnRelatorioServico1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRelatorioServico1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeRelatorioFinanceiro64x64.png"))); // NOI18N
        btnRelatorioServico1.setText("Relatórios Parcelas Pagas");
        btnRelatorioServico1.setToolTipText("Relatórios Parcelas Pagas");
        btnRelatorioServico1.setBorder(null);
        btnRelatorioServico1.setContentAreaFilled(false);
        btnRelatorioServico1.setDefaultCapable(false);
        btnRelatorioServico1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRelatorioServico1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnRelatorioServico1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRelatorioServico1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioServico1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelRelatorioLayout = new javax.swing.GroupLayout(panelRelatorio);
        panelRelatorio.setLayout(panelRelatorioLayout);
        panelRelatorioLayout.setHorizontalGroup(
            panelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRelatorioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRelatorioServico, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRelatorioServico1, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(708, Short.MAX_VALUE))
        );
        panelRelatorioLayout.setVerticalGroup(
            panelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRelatorioLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnRelatorioServico1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRelatorioServico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Relatórios", panelRelatorio);

        buttonConfiguracao.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        buttonConfiguracao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeServicos64x64.png"))); // NOI18N
        buttonConfiguracao.setText("Configurações");
        buttonConfiguracao.setToolTipText("Configurações");
        buttonConfiguracao.setBorder(null);
        buttonConfiguracao.setContentAreaFilled(false);
        buttonConfiguracao.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonConfiguracao.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        buttonConfiguracao.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonConfiguracao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonConfiguracaoActionPerformed(evt);
            }
        });

        jBtnFazerBackup.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jBtnFazerBackup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeCopyDatabase64x64.png"))); // NOI18N
        jBtnFazerBackup.setText("Fazer Cópia");
        jBtnFazerBackup.setBorder(null);
        jBtnFazerBackup.setContentAreaFilled(false);
        jBtnFazerBackup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBtnFazerBackup.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jBtnFazerBackup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBtnFazerBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnFazerBackupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConfiguracaoLayout = new javax.swing.GroupLayout(panelConfiguracao);
        panelConfiguracao.setLayout(panelConfiguracaoLayout);
        panelConfiguracaoLayout.setHorizontalGroup(
            panelConfiguracaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfiguracaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonConfiguracao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jBtnFazerBackup, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(798, Short.MAX_VALUE))
        );
        panelConfiguracaoLayout.setVerticalGroup(
            panelConfiguracaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConfiguracaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConfiguracaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBtnFazerBackup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonConfiguracao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Configurações", panelConfiguracao);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        labelUsuario.setText("usuario");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(labelUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelUsuario))
        );

        buttonSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeDesligar64x64.png"))); // NOI18N
        buttonSair.setText("");
        buttonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFundoLayout = new javax.swing.GroupLayout(panelFundo);
        panelFundo.setLayout(panelFundoLayout);
        panelFundoLayout.setHorizontalGroup(
            panelFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFundoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonSair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        panelFundoLayout.setVerticalGroup(
            panelFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFundoLayout.createSequentialGroup()
                .addContainerGap(461, Short.MAX_VALUE)
                .addComponent(buttonSair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
            .addComponent(panelFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsuarioActionPerformed
        DialogTabelaUsuario d = new DialogTabelaUsuario(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnUsuarioActionPerformed

    private void buttonConfiguracaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonConfiguracaoActionPerformed
        DialogConfiguracaoGeral d = new DialogConfiguracaoGeral(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_buttonConfiguracaoActionPerformed

    private void btnEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmpresaActionPerformed
        DialogTabelaEmpresa d = new DialogTabelaEmpresa(this, true);
        d.setVisible(true);

    }//GEN-LAST:event_btnEmpresaActionPerformed

    private void btnFormaPgtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFormaPgtoActionPerformed
        DialogTabelaFormaPgto d = new DialogTabelaFormaPgto(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnFormaPgtoActionPerformed

    private void btnFabricanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFabricanteActionPerformed
        DialogTabelaFabricante d = new DialogTabelaFabricante(this, true);
        d.setVisible(true);

    }//GEN-LAST:event_btnFabricanteActionPerformed

    private void btnModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModeloActionPerformed
        DialogTabelaModelo d = new DialogTabelaModelo(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnModeloActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        DialogTabelaCor d = new DialogTabelaCor(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btnVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVeiculosActionPerformed
        DialogTabelaVeiculo d = new DialogTabelaVeiculo(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnVeiculosActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        DialogTabelaClientes d = new DialogTabelaClientes(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnClientesActionPerformed

    private void btnServicosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicosActionPerformed
        DialogTabelaServico d = new DialogTabelaServico(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnServicosActionPerformed

    private void jBtnFazerBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnFazerBackupActionPerformed
        ConnectionFactory conn = new ConnectionFactory();
        Integer fazerBackUp = conn.fazerBackUp();
        if (fazerBackUp!=null && fazerBackUp == 0) {
            System.exit(0);
        }
    }//GEN-LAST:event_jBtnFazerBackupActionPerformed

    private void btnRelatorioServicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioServicoActionPerformed
        DialogRelatorioServico d = new DialogRelatorioServico(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnRelatorioServicoActionPerformed

    private void btnRelatorioServico1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioServico1ActionPerformed
        DialogRelatorioPagamento d = new DialogRelatorioPagamento(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnRelatorioServico1ActionPerformed

    private void buttonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_buttonSairActionPerformed

    private void btnOrcamentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrcamentosActionPerformed
        DialogTabelaOrcamento d = new DialogTabelaOrcamento(this, true);
        d.setVisible(true);
    }//GEN-LAST:event_btnOrcamentosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClientes;
    private javax.swing.JButton btnEmpresa;
    private javax.swing.JButton btnFabricante;
    private javax.swing.JButton btnFormaPgto;
    private javax.swing.JButton btnModelo;
    private javax.swing.JButton btnOrcamentos;
    private javax.swing.JButton btnRelatorioServico;
    private javax.swing.JButton btnRelatorioServico1;
    private javax.swing.JButton btnServicos;
    private javax.swing.JButton btnUsuario;
    private javax.swing.JButton btnVeiculos;
    private javax.swing.JButton buttonConfiguracao;
    private org.jdesktop.swingx.JXHyperlink buttonSair;
    private javax.swing.JButton jBtnFazerBackup;
    private javax.swing.JButton jButton4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelUsuario;
    private javax.swing.JPanel panelCadastro;
    private javax.swing.JPanel panelConfiguracao;
    private javax.swing.JPanel panelFinanceiro;
    private javax.swing.JPanel panelFundo;
    private javax.swing.JPanel panelRelatorio;
    // End of variables declaration//GEN-END:variables
}
