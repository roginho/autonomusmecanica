/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.FormaPgtoDAOJPA;
import br.com.autonomusmecanica.dao.ServicoParcelaDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Fabricante;
import br.com.autonomusmecanica.entidades.FormaPgto;
import br.com.autonomusmecanica.entidades.Modelo;
import br.com.autonomusmecanica.entidades.ServicoParcela;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.relatorios.GeraRelatorio;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Bianca
 */
public class DialogRelatorioPagamento extends javax.swing.JDialog {

    private Cliente cliente;
    private Veiculo veiculo;
    private Fabricante fabricante;
    private Modelo modelo;

    private void limpar() {
        cliente = null;
        veiculo = null;
        lblNome.setText("");
        lblVeiculo.setText("");
    }

    private void abreBuscaCliente(String nome) {
        DialogTabelaClientes d = new DialogTabelaClientes(null, true, nome);
        d.setVisible(true);
        this.cliente = d.getCli();
        if (cliente != null) {
            lblNome.setText(cliente.getPessoa().getNome());
        }
    }

    private void abreBuscaVeiculo() {
        DialogTabelaVeiculo d = new DialogTabelaVeiculo(null, true);
        d.setVisible(true);
        this.veiculo = d.getVeiculo();
        if (veiculo != null) {
            lblVeiculo.setText(veiculo.getDescricaoCompleta());
        }
    }

    public void gerarRelatorio() {
        ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
        List<ServicoParcela> lista = null;
        FormaPgto formaPgto = (FormaPgto) (comboBoxFormaPagto.getSelectedItem());
        lista = dao.getServicosParcelas(dChooserInicial.getDate(), dChooserFinal.getDate(), cliente, veiculo, null, formaPgto);
        if (lista != null && !lista.isEmpty()) {
            GeraRelatorio g = new GeraRelatorio();
            g.geraRelatorio(lista, "Relatório de Serviços", "reportServicosParcelas");
        } else {
            JOptionPane.showMessageDialog(this, "Nenhuma parcela encontrado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void carregaComboFormaPgto() {
        for (FormaPgto f : new FormaPgtoDAOJPA().getAll(FormaPgto.class)) {
            comboBoxFormaPagto.addItem(f);
        }
    }

    public DialogRelatorioPagamento(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Date hj = new Date();
        Date ant = Singleton.getInstance().somaMes(hj, -1);
        dChooserInicial.setDate(ant);
        dChooserFinal.setDate(hj);
        carregaComboFormaPgto();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btnRelatorio = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        btnBuscaCli = new javax.swing.JButton();
        dChooserInicial = new com.toedter.calendar.JDateChooser();
        dChooserFinal = new com.toedter.calendar.JDateChooser();
        jLabel12 = new javax.swing.JLabel();
        btnBuscaVeic = new javax.swing.JButton();
        lblVeiculo = new javax.swing.JLabel();
        comboBoxFormaPagto = new javax.swing.JComboBox();
        btnRelatorio1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorio.setText("Imprimir");
        btnRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioActionPerformed(evt);
            }
        });

        panel.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        jLabel11.setText("Nome:");

        lblNome.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnBuscaCli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBuscaCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaCliActionPerformed(evt);
            }
        });

        dChooserInicial.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Inicial"));

        dChooserFinal.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Final"));

        jLabel12.setText("Veículo:");

        btnBuscaVeic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBuscaVeic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaVeicActionPerformed(evt);
            }
        });

        lblVeiculo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        comboBoxFormaPagto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        comboBoxFormaPagto.setBorder(javax.swing.BorderFactory.createTitledBorder("Forma de pagamento"));
        comboBoxFormaPagto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxFormaPagtoItemStateChanged(evt);
            }
        });
        comboBoxFormaPagto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxFormaPagtoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscaVeic))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscaCli)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblVeiculo, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(dChooserInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dChooserFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(comboBoxFormaPagto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel11)
                    .addComponent(btnBuscaCli, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel12)
                    .addComponent(btnBuscaVeic, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboBoxFormaPagto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dChooserFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dChooserInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnRelatorio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/New document.png"))); // NOI18N
        btnRelatorio1.setText("Limpar");
        btnRelatorio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorio1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRelatorio1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRelatorio)
                    .addComponent(btnRelatorio1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioActionPerformed
        gerarRelatorio();
    }//GEN-LAST:event_btnRelatorioActionPerformed

    private void btnBuscaCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaCliActionPerformed
        abreBuscaCliente("");
    }//GEN-LAST:event_btnBuscaCliActionPerformed

    private void btnBuscaVeicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaVeicActionPerformed
        abreBuscaVeiculo();
    }//GEN-LAST:event_btnBuscaVeicActionPerformed

    private void btnRelatorio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorio1ActionPerformed
        limpar();
    }//GEN-LAST:event_btnRelatorio1ActionPerformed

    private void comboBoxFormaPagtoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoItemStateChanged

    }//GEN-LAST:event_comboBoxFormaPagtoItemStateChanged

    private void comboBoxFormaPagtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoActionPerformed

    }//GEN-LAST:event_comboBoxFormaPagtoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscaCli;
    private javax.swing.JButton btnBuscaVeic;
    private javax.swing.JButton btnRelatorio;
    private javax.swing.JButton btnRelatorio1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox comboBoxFormaPagto;
    private com.toedter.calendar.JDateChooser dChooserFinal;
    private com.toedter.calendar.JDateChooser dChooserInicial;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblVeiculo;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
