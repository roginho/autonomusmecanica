/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.TelefoneDAOJPA;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Telefone;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelTelefone;
import br.com.roger.utils.Singleton;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Roger
 */
public class PanelFone extends javax.swing.JPanel {

    private TableModelTelefone model = new TableModelTelefone();
    private final TableCellRenderer renderer = new StripedTable();
    private Pessoa pessoa;

        public void limpaTabela() {
            this.pessoa=null;
        model.limpaLista();
    }

    public void atualizaTabelaTelefone() {
        TelefoneDAOJPA dao = new TelefoneDAOJPA();
        List<Telefone> lista = dao.getPessoaTelefone(pessoa);
        model = new TableModelTelefone(lista);
        tableTelefone.setModel(model);
        tableTelefone.getColumnModel().getColumn(0).setPreferredWidth(80);
        tableTelefone.getColumnModel().getColumn(1).setPreferredWidth(270);
        tableTelefone.getColumnModel().getColumn(2).setPreferredWidth(30);
        tableTelefone.getColumnModel().getColumn(3).setPreferredWidth(30);
        tableTelefone.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableTelefone, 2, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableTelefone, 3, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ImageIcon getIconTelefone() {
        ImageIcon tab1Icon = new ImageIcon(this.getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Phone number.png"));
        return tab1Icon;
    }

    private void adicionaTelefone() {
        if (this.pessoa != null) {
            Telefone telefone = new Telefone();
            telefone.setPessoa(pessoa);
            DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Inserir");
            d.setTitle("Cadastrar Telefone");
            d.setVisible(true);
            TelefoneDAOJPA dao = new TelefoneDAOJPA();
            telefone.setPessoa(pessoa);
            telefone.setNumero(d.t.getNumero());
            telefone.setObs(d.t.getObs());
            if (d.t.getNumero() != null) {
                dao.save(telefone);
                atualizaTabelaTelefone();
            }
        }
    }

    private void alteraTelefone() {
        if (this.pessoa != null) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Salvar");
                    d.setTitle("Cadastrar Telefone");
                    d.setVisible(true);
                    TelefoneDAOJPA dao = new TelefoneDAOJPA();
                    telefone.setPessoa(telefone.getPessoa());
                    telefone.setNumero(d.t.getNumero());
                    telefone.setObs(d.t.getObs());
                    dao.save(telefone);
                    atualizaTabelaTelefone();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiTelefone() {
        if (this.pessoa != null) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o telefone " + tableTelefone.getValueAt(linha, 1) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        TelefoneDAOJPA dao = new TelefoneDAOJPA();
                        dao.remove(Telefone.class, telefone.getIdTelefone());
                        atualizaTabelaTelefone();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    public PanelFone() {
        initComponents();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ScrollPaneFone = new javax.swing.JScrollPane();
        tableTelefone = new javax.swing.JTable();
        buttonAddFone = new javax.swing.JButton();

        tableTelefone.setBackground(java.awt.Color.lightGray);
        tableTelefone.setFocusable(false);
        tableTelefone.setGridColor(java.awt.Color.lightGray);
        tableTelefone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseReleased(evt);
            }
        });
        ScrollPaneFone.setViewportView(tableTelefone);

        buttonAddFone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddFone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddFoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(ScrollPaneFone, javax.swing.GroupLayout.DEFAULT_SIZE, 660, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonAddFone, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(buttonAddFone)
                .addGap(0, 123, Short.MAX_VALUE))
            .addComponent(ScrollPaneFone, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableTelefoneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableTelefoneMouseReleased

    private void tableTelefoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseClicked
        if (tableTelefone.getSelectedColumn() == 2) {
            alteraTelefone();
        } else if (tableTelefone.getSelectedColumn() == 3) {
            excluiTelefone();
        }
    }//GEN-LAST:event_tableTelefoneMouseClicked

    private void buttonAddFoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddFoneActionPerformed
        adicionaTelefone();
    }//GEN-LAST:event_buttonAddFoneActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPaneFone;
    private javax.swing.JButton buttonAddFone;
    private javax.swing.JTable tableTelefone;
    // End of variables declaration//GEN-END:variables
}
