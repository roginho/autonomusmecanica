/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.FabricanteDAOJPA;
import br.com.autonomusmecanica.dao.ModeloDAOJPA;
import br.com.autonomusmecanica.dao.ServicoDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Fabricante;
import br.com.autonomusmecanica.entidades.Modelo;
import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.relatorios.GeraRelatorio;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Bianca
 */
public class DialogRelatorioServico extends javax.swing.JDialog {

    private Cliente cliente;
    private Veiculo veiculo;
    private Fabricante fabricante;
    private Modelo modelo;

    private void limpar() {
        cliente = null;
        veiculo = null;
        lblNome.setText("");
        lblVeiculo.setText("");
        modelo = null;
        fabricante = null;
        cmbFabricante.setSelectedIndex(0);
        cmbModelo.setSelectedIndex(0);

    }

    private void abreBuscaCliente(String nome) {
        DialogTabelaClientes d = new DialogTabelaClientes(null, true, nome);
        d.setVisible(true);
        this.cliente = d.getCli();
        if (cliente != null) {
            lblNome.setText(cliente.getPessoa().getNome());
        }
    }

    private void abreBuscaVeiculo() {
        DialogTabelaVeiculo d = new DialogTabelaVeiculo(null, true);
        d.setVisible(true);
        this.veiculo = d.getVeiculo();
        if (veiculo != null) {
            lblVeiculo.setText(veiculo.getDescricaoCompleta());
        }
    }

    public void gerarRelatorio() {
        ServicoDAOJPA dao = new ServicoDAOJPA();
        if (cmbFabricante.getSelectedIndex() > 0) {
            this.fabricante = (Fabricante) cmbFabricante.getSelectedItem();
        } else {
            this.fabricante = null;
        }
        if (cmbModelo.getSelectedIndex() > 0) {
            this.modelo = (Modelo) cmbModelo.getSelectedItem();
        } else {
            this.modelo = null;
        }
        List<Servico> lista = null;
        if (rdoTodos.isSelected()) {
            lista = dao.getServicos(dChooserInicial.getDate(), dChooserFinal.getDate(), null, cliente, veiculo, fabricante, modelo);
        } else if (rdoAbertos.isSelected()) {
            lista = dao.getServicos(dChooserInicial.getDate(), dChooserFinal.getDate(), "a", cliente, veiculo, fabricante, modelo);
        } else if (rdoFechados.isSelected()) {
            lista = dao.getServicos(dChooserInicial.getDate(), dChooserFinal.getDate(), "f", cliente, veiculo, fabricante, modelo);
        }
        if (lista != null && !lista.isEmpty()) {
            GeraRelatorio g = new GeraRelatorio();
            g.geraRelatorio(lista, "Relatório de Serviços", "reportServicos");
        } else {
            JOptionPane.showMessageDialog(this, "Nenhum serviço encontrado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void carregaComboModelo() {
        cmbModelo.removeAllItems();
        Modelo falsoObj = new Modelo();
        falsoObj.setDescricao("TODOS");
        cmbModelo.addItem(falsoObj);
        if (cmbFabricante.getSelectedIndex() > 0) {
            this.fabricante = (Fabricante) cmbFabricante.getSelectedItem();
            if (fabricante != null) {
                for (Modelo f : new ModeloDAOJPA().getModeloFabricante(fabricante)) {
                    cmbModelo.addItem(f);
                }
            }
        }
    }

    private void carregaComboFabricante() {
        Fabricante falsoObj = new Fabricante();
        falsoObj.setNome("TODOS");
        cmbFabricante.addItem(falsoObj);
        List<Fabricante> all = new FabricanteDAOJPA().getAll(Fabricante.class);
        for (Fabricante f : all) {
            cmbFabricante.addItem(f);
        }
    }

    public DialogRelatorioServico(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        carregaComboFabricante();
        carregaComboModelo();
        Date hj = new Date();
        Date ant = Singleton.getInstance().somaMes(hj, -1);
        dChooserInicial.setDate(ant);
        dChooserFinal.setDate(hj);
        AutoCompleteDecorator.decorate(this.cmbFabricante);
        AutoCompleteDecorator.decorate(this.cmbModelo);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btnRelatorio = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        btnBuscaCli = new javax.swing.JButton();
        dChooserInicial = new com.toedter.calendar.JDateChooser();
        dChooserFinal = new com.toedter.calendar.JDateChooser();
        rdoTodos = new javax.swing.JRadioButton();
        rdoFechados = new javax.swing.JRadioButton();
        rdoAbertos = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        btnBuscaVeic = new javax.swing.JButton();
        lblVeiculo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbFabricante = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cmbModelo = new javax.swing.JComboBox();
        btnRelatorio1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorio.setText("Imprimir");
        btnRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioActionPerformed(evt);
            }
        });

        panel.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros"));

        jLabel11.setText("Nome:");

        lblNome.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnBuscaCli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBuscaCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaCliActionPerformed(evt);
            }
        });

        dChooserInicial.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Inicial"));

        dChooserFinal.setBorder(javax.swing.BorderFactory.createTitledBorder("Data Final"));

        buttonGroup1.add(rdoTodos);
        rdoTodos.setText("Todos");

        buttonGroup1.add(rdoFechados);
        rdoFechados.setText("Fechados");
        rdoFechados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoFechadosMouseClicked(evt);
            }
        });
        rdoFechados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoFechadosActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdoAbertos);
        rdoAbertos.setSelected(true);
        rdoAbertos.setText("Abertos");
        rdoAbertos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoAbertosMouseClicked(evt);
            }
        });
        rdoAbertos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoAbertosActionPerformed(evt);
            }
        });

        jLabel12.setText("Veículo:");

        btnBuscaVeic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBuscaVeic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaVeicActionPerformed(evt);
            }
        });

        lblVeiculo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("Fabricante:");

        cmbFabricante.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFabricanteItemStateChanged(evt);
            }
        });

        jLabel7.setText("Modelo:");

        cmbModelo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbModeloItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscaVeic))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscaCli)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblVeiculo, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(dChooserInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dChooserFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rdoTodos)
                                .addGap(5, 5, 5)
                                .addComponent(rdoAbertos)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoFechados)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(cmbModelo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbFabricante, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel11)
                    .addComponent(btnBuscaCli, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel12)
                    .addComponent(btnBuscaVeic, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFabricante, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(rdoTodos)
                    .addComponent(rdoAbertos)
                    .addComponent(rdoFechados)
                    .addComponent(dChooserFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dChooserInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnRelatorio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/New document.png"))); // NOI18N
        btnRelatorio1.setText("Limpar");
        btnRelatorio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorio1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRelatorio1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRelatorio)
                    .addComponent(btnRelatorio1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioActionPerformed
        gerarRelatorio();
    }//GEN-LAST:event_btnRelatorioActionPerformed

    private void rdoAbertosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoAbertosMouseClicked
    }//GEN-LAST:event_rdoAbertosMouseClicked

    private void rdoAbertosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoAbertosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoAbertosActionPerformed

    private void rdoFechadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoFechadosMouseClicked
    }//GEN-LAST:event_rdoFechadosMouseClicked

    private void rdoFechadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoFechadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoFechadosActionPerformed

    private void btnBuscaCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaCliActionPerformed
        abreBuscaCliente("");
    }//GEN-LAST:event_btnBuscaCliActionPerformed

    private void btnBuscaVeicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaVeicActionPerformed
        abreBuscaVeiculo();
    }//GEN-LAST:event_btnBuscaVeicActionPerformed

    private void btnRelatorio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorio1ActionPerformed
        limpar();
    }//GEN-LAST:event_btnRelatorio1ActionPerformed

    private void cmbModeloItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbModeloItemStateChanged

    }//GEN-LAST:event_cmbModeloItemStateChanged

    private void cmbFabricanteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFabricanteItemStateChanged
        carregaComboModelo();
    }//GEN-LAST:event_cmbFabricanteItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscaCli;
    private javax.swing.JButton btnBuscaVeic;
    private javax.swing.JButton btnRelatorio;
    private javax.swing.JButton btnRelatorio1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cmbFabricante;
    private javax.swing.JComboBox cmbModelo;
    private com.toedter.calendar.JDateChooser dChooserFinal;
    private com.toedter.calendar.JDateChooser dChooserInicial;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblVeiculo;
    private javax.swing.JPanel panel;
    private javax.swing.JRadioButton rdoAbertos;
    private javax.swing.JRadioButton rdoFechados;
    private javax.swing.JRadioButton rdoTodos;
    // End of variables declaration//GEN-END:variables
}
