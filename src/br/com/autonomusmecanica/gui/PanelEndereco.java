/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.EnderecoDAOJPA;
import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelEndereco;
import br.com.autonomusmecanica.tableModel.TableModelTelefone;
import br.com.roger.utils.Singleton;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

public class PanelEndereco extends javax.swing.JPanel {

    private TableModelEndereco model = new TableModelEndereco();
    private final TableCellRenderer renderer = new StripedTable();
    private Pessoa pessoa;

    public void limpaTabela() {
        this.pessoa = null;
        model.limpaLista();
    }

    public ImageIcon getIconEndereco() {
        ImageIcon tab1Icon = new ImageIcon(this.getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Home.png"));
        return tab1Icon;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    private void adicionaEndereco() {
        if (this.pessoa != null) {
            Endereco e = new Endereco();
            e.setPessoa(this.pessoa);
            DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Inserir");
            d.setTitle("Cadastrar Endereco");
            d.setVisible(true);
            EnderecoDAOJPA dao = new EnderecoDAOJPA();
            e.setPessoa(pessoa);
            e.setLogradouro(d.endereco.getLogradouro());
            e.setNumero(d.endereco.getNumero());
            e.setComplemento(d.endereco.getComplemento());
            e.setBairro(d.endereco.getBairro());
            e.setCep(d.endereco.getCep());
            e.setCidade(d.endereco.getCidade());
            if (e.getLogradouro() != null && e.getCidade() != null) {
                dao.save(e);
            }
        }
        atualizaTabelaEndereco();
    }

    private void alteraEndereco() {
        if (this.pessoa != null) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco e = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (e.getIdEndereco() != null) {
                    DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Salvar");
                    d.setTitle("Alterar Endereco");
                    d.setVisible(true);
                    EnderecoDAOJPA dao = new EnderecoDAOJPA();
                    e.setPessoa(pessoa);
                    e.setLogradouro(d.endereco.getLogradouro());
                    e.setNumero(d.endereco.getNumero());
                    e.setComplemento(d.endereco.getComplemento());
                    e.setBairro(d.endereco.getBairro());
                    e.setCep(d.endereco.getCep());
                    e.setCidade(d.endereco.getCidade());
                    dao.save(e);
                    atualizaTabelaEndereco();

                } else {
                    JOptionPane.showMessageDialog(this, "Sem endereco para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEndereco() {
        if (this.pessoa != null) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco endereco = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (endereco.getIdEndereco() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o endereço " + tableEndereco.getValueAt(linha, 1) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EnderecoDAOJPA dao = new EnderecoDAOJPA();
                        dao.remove(Endereco.class, endereco.getIdEndereco());
//                        JOptionPane.showMessageDialog(this,"Registro removido");
                        atualizaTabelaEndereco();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem Endereco para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    public void atualizaTabelaEndereco() {
        EnderecoDAOJPA dao = new EnderecoDAOJPA();
        List<Endereco> lista = dao.getPessoaEndereco(pessoa);
        model = new TableModelEndereco(lista);
        tableEndereco.setModel(model);
        tableEndereco.getColumnModel().getColumn(0).setPreferredWidth(200);
        tableEndereco.getColumnModel().getColumn(1).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(2).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(3).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setPreferredWidth(0);
        tableEndereco.getColumnModel().getColumn(4).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(5).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(6).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setPreferredWidth(0);
        tableEndereco.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEndereco, 7, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEndereco, 8, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public PanelEndereco() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonAddEnd = new javax.swing.JButton();
        ScrollPaneEnd = new javax.swing.JScrollPane();
        tableEndereco = new javax.swing.JTable();

        buttonAddEnd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEndActionPerformed(evt);
            }
        });

        tableEndereco.setBackground(java.awt.Color.lightGray);
        tableEndereco.setFocusable(false);
        tableEndereco.setGridColor(java.awt.Color.lightGray);
        tableEndereco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseReleased(evt);
            }
        });
        ScrollPaneEnd.setViewportView(tableEndereco);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 659, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonAddEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(buttonAddEnd)
                .addContainerGap(122, Short.MAX_VALUE))
            .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableEnderecoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableEnderecoMouseReleased

    private void tableEnderecoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseClicked
        if (tableEndereco.getSelectedColumn() == 7) {
            alteraEndereco();
        } else if (tableEndereco.getSelectedColumn() == 8) {
            excluiEndereco();
        }
    }//GEN-LAST:event_tableEnderecoMouseClicked

    private void buttonAddEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEndActionPerformed
        adicionaEndereco();
    }//GEN-LAST:event_buttonAddEndActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPaneEnd;
    private javax.swing.JButton buttonAddEnd;
    private javax.swing.JTable tableEndereco;
    // End of variables declaration//GEN-END:variables
}
