package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.OrcamentoDAOJPA;
import br.com.autonomusmecanica.dao.OrcamentoItemDAOJPA;
import br.com.autonomusmecanica.dao.ServicoDAOJPA;
import br.com.autonomusmecanica.dao.ServicoItemDAOJPA;
import br.com.autonomusmecanica.entidades.Orcamento;
import br.com.autonomusmecanica.entidades.OrcamentoItem;
import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.ServicoItem;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.relatorios.GeraRelatorio;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.ColorRenderer;
import br.com.autonomusmecanica.tableModel.TableModelOrcamento;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

public final class DialogTabelaOrcamento extends javax.swing.JDialog {

    private TableModelOrcamento model;
    private TableRowSorter sorter;
    private int colAlt = 8;
    private int colExc = 9;
    private int colGerar = 10;
    private int colCor = 4;

    public void gerarRelatorioTodos() {
        OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
        List<Orcamento> lista = null;
        if (rdoTodos.isSelected()) {
            lista = dao.getAll(Orcamento.class);
        } else if (rdoAbertos.isSelected()) {
            lista = dao.getOrcamentos("a");
        } else if (rdoFechados.isSelected()) {
            lista = dao.getOrcamentos("f");
        }
        if (lista != null && !lista.isEmpty()) {
            GeraRelatorio g = new GeraRelatorio();
            g.geraRelatorio(lista, "Relatório de Orçamentos", "reportOrcamentos");
        } else {
            JOptionPane.showMessageDialog(this, "Nenhum orçamento encontrado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void atualizaTabela() {
        OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
        List<Orcamento> lista = null;
        if (rdoTodos.isSelected()) {
            lista = dao.getAll(Orcamento.class);
        } else if (rdoAbertos.isSelected()) {
            lista = dao.getOrcamentos("a");
        } else if (rdoFechados.isSelected()) {
            lista = dao.getOrcamentos("f");
        }
        if (lista != null) {
            model = new TableModelOrcamento(lista);
            table.setModel(model);
            model.ordenarPorIdDesc();
            table.getColumnModel().getColumn(0).setPreferredWidth(50);
            table.getColumnModel().getColumn(0).setMaxWidth(50);
            table.getColumnModel().getColumn(1).setPreferredWidth(70);
            table.getColumnModel().getColumn(1).setMaxWidth(70);
            table.getColumnModel().getColumn(2).setPreferredWidth(40);
            table.getColumnModel().getColumn(2).setMaxWidth(40);
            table.getColumnModel().getColumn(3).setPreferredWidth(100);
            table.getColumnModel().getColumn(colCor).setPreferredWidth(30);
            table.getColumnModel().getColumn(colCor).setMaxWidth(30);
            table.getColumnModel().getColumn(5).setPreferredWidth(200);
            table.getColumnModel().getColumn(6).setPreferredWidth(70);
            table.getColumnModel().getColumn(6).setMaxWidth(70);
            table.getColumnModel().getColumn(7).setPreferredWidth(70);
            table.getColumnModel().getColumn(7).setMaxWidth(70);
            table.getColumnModel().getColumn(colAlt).setPreferredWidth(70);
            table.getColumnModel().getColumn(colAlt).setMaxWidth(70);
            table.getColumnModel().getColumn(colExc).setPreferredWidth(70);
            table.getColumnModel().getColumn(colExc).setMaxWidth(70);
            table.getColumnModel().getColumn(colGerar).setPreferredWidth(70);
            table.getColumnModel().getColumn(colGerar).setMaxWidth(70);

            TableCellRenderer renderer = new StripedTable();
            table.setDefaultRenderer(Object.class, renderer);
            ColorRenderer colorRenderer = new ColorRenderer(true);
            TableColumn column = table.getColumnModel().getColumn(colCor);
            column.setCellRenderer(colorRenderer);
            new ButtonColumn(table, colAlt, Singleton.getInstance().iconeJTable("alterar"));
            new ButtonColumn(table, colExc, Singleton.getInstance().iconeJTable("excluir"));
            new ButtonColumn(table, colGerar, Singleton.getInstance().iconeJTable("gerar"));
            sorter = new TableRowSorter<>(model);
            labelTotal.setText("" + table.getRowCount());
            table.setRowSorter(sorter);
        }
    }

    private void alterar() {
        Orcamento s = getTable();
        if (s != null && s.getStatus() == 'a') {
            DialogCadastroOrcamento d = new DialogCadastroOrcamento(null, true, s, 0);
            d.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void excluir() {
        Orcamento p = getTable();
        if (p != null && p.getStatus() == 'a') {
            String nome = "";
            if (p.getCliente() != null) {
                nome = "Cliente: " + p.getCliente().getPessoa().getNome();
            }
            String msg = "Deseja excluir o orçamento " + p.getId() + "\n"
                    + nome;
            OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
            OrcamentoItemDAOJPA daoI = new OrcamentoItemDAOJPA();
            List<OrcamentoItem> itemOrcamento = daoI.getItemOrcamento(p);
            if (itemOrcamento != null && !itemOrcamento.isEmpty()) {
                msg += "\n e seus itens?";
            }
            int showConfirmDialog = JOptionPane.showConfirmDialog(this, msg, "Excluir", JOptionPane.YES_NO_OPTION);
            //0=sim e 1=nao 
            if (showConfirmDialog == 0) {
                p.setStatus('c');
                dao.save(p);
                atualizaTabela();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }

    }

    private Orcamento getTable() {
        Orcamento obj = null;
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            obj = (Orcamento) table.getValueAt(linha, -1);
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um orçamento", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
        return obj;
    }

    private void imprimirOrcamento() {
        Orcamento p = getTable();
        if (p != null) {
            OrcamentoItemDAOJPA dao = new OrcamentoItemDAOJPA();
            List<OrcamentoItem> lista = dao.getItemOrcamento(p);
            GeraRelatorio g = new GeraRelatorio();
            g.geraRelatorio(lista, "Relatorio de Orçamento", "reportOrcamentoCliente");
        }
    }

    private Servico transformaOrcamentoEmServico(Orcamento o) {
        Servico s = null;
        if (o != null) {
            s = new Servico();
            s.setCliente(o.getCliente());
            s.setDataServico(new Date());
            s.setDesconto(o.getDesconto());
            s.setDescricao(o.getDescricao());
            s.setKilometragem(0);
            s.setStatus('a');
            s.setValorMO(o.getValorMO());
            s.setValorMateriais(o.getValorMateriais());
            s.setValorTotal(o.getValorTotal());
            s.setVeiculo(o.getVeiculo());
            ServicoDAOJPA daoS = new ServicoDAOJPA();
            s = daoS.save(s);
        }
        return s;
    }

    private ServicoItem transformaOrcamentoItemServicoItem(OrcamentoItem i) {
        ServicoItem si = null;
        if (i != null) {
            si = new ServicoItem();
            si.setDescricao(i.getDescricao());
            si.setQuantidade(i.getQuantidade());
            si.setServico(i.getOrcamento().getServico());
            si.setValorTotal(i.getValorTotal());
            si.setValorUnitario(i.getValorUnitario());
            ServicoItemDAOJPA daoS = new ServicoItemDAOJPA();
            si = daoS.save(si);
        }
        return si;
    }

    private void gerarServico(Orcamento o) {
        if (o.getServico() != null) {
            this.dispose();
            DialogCadastroServico d = new DialogCadastroServico(null, true, o.getServico(), 0);
            d.setVisible(true);
        } else {
            Servico s = transformaOrcamentoEmServico(o);
            if (s != null) {
                o.setServico(s);
                OrcamentoDAOJPA daoO = new OrcamentoDAOJPA();
                o.setStatus('f');
                o = daoO.save(o);
                OrcamentoItemDAOJPA daoI = new OrcamentoItemDAOJPA();
                List<OrcamentoItem> itemOrcamento = daoI.getItemOrcamento(o);
                for (OrcamentoItem i : itemOrcamento) {
                    transformaOrcamentoItemServicoItem(i);
                }
                this.dispose();
                DialogCadastroServico d = new DialogCadastroServico(null, true, s, 0);
                d.setVisible(true);
            }
        }
    }

    private void executarGerarServico() {
        Orcamento o = getTable();
        if (o != null && o.getStatus() == 'a') {
            String msg = "Gerar Servico?";
            int showConfirmDialog = JOptionPane.showConfirmDialog(this, msg, "Gerar", JOptionPane.YES_NO_OPTION);
            //0=sim e 1=nao 
            if (showConfirmDialog == 0) {
                gerarServico(o);
                atualizaTabela();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);

        }
    }

    public DialogTabelaOrcamento(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
//        this.setTitle("Orcamentos");
        atualizaTabela();
        textFieldPesquisa.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        menuItemImprimir = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        buttonAdicionar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        textFieldPesquisa = new javax.swing.JTextField();
        labelTotal = new javax.swing.JLabel();
        rdoTodos = new javax.swing.JRadioButton();
        rdoAbertos = new javax.swing.JRadioButton();
        rdoFechados = new javax.swing.JRadioButton();
        btnRelatorio = new javax.swing.JButton();

        menuItemImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        menuItemImprimir.setText("Imprimir Orçamento");
        menuItemImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemImprimirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuItemImprimir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Orçamento");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        table.setComponentPopupMenu(jPopupMenu1);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        buttonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeAdicionar32x32.png"))); // NOI18N
        buttonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAdicionarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        textFieldPesquisa.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro"));
        textFieldPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyReleased(evt);
            }
        });

        labelTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        buttonGroup1.add(rdoTodos);
        rdoTodos.setText("Todos");
        rdoTodos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoTodosMouseClicked(evt);
            }
        });
        rdoTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoTodosActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdoAbertos);
        rdoAbertos.setSelected(true);
        rdoAbertos.setText("Abertos");
        rdoAbertos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoAbertosMouseClicked(evt);
            }
        });
        rdoAbertos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoAbertosActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdoFechados);
        rdoFechados.setText("Fechados");
        rdoFechados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoFechadosMouseClicked(evt);
            }
        });
        rdoFechados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoFechadosActionPerformed(evt);
            }
        });

        btnRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorio.setText("Relatório Todos");
        btnRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(buttonAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonFechar)
                        .addGap(285, 285, 285)
                        .addComponent(btnRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rdoTodos)
                        .addGap(5, 5, 5)
                        .addComponent(rdoAbertos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoFechados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(textFieldPesquisa, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(rdoTodos)
                    .addComponent(rdoAbertos)
                    .addComponent(rdoFechados)
                    .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRelatorio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        atualizaTabela();
    }//GEN-LAST:event_formWindowActivated

    private void buttonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAdicionarActionPerformed
        DialogCadastroOrcamento d = new DialogCadastroOrcamento(null, true, null, 0);
        d.setVisible(true);
        atualizaTabela();
    }//GEN-LAST:event_buttonAdicionarActionPerformed

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void textFieldPesquisaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyTyped

    }//GEN-LAST:event_textFieldPesquisaKeyTyped

    private void textFieldPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyReleased
        if (table.getSelectedColumn() > 0) {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), table.getSelectedColumn());
        } else {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), 0);
        }

        labelTotal.setText("" + table.getRowCount());
    }//GEN-LAST:event_textFieldPesquisaKeyReleased

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colAlt) {
            alterar();
        } else if (table.getSelectedColumn() == colExc) {
            excluir();
        } else if (table.getSelectedColumn() == colGerar) {
            executarGerarServico();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void rdoTodosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoTodosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoTodosMouseClicked

    private void rdoAbertosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoAbertosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoAbertosMouseClicked

    private void rdoAbertosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoAbertosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoAbertosActionPerformed

    private void rdoFechadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoFechadosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoFechadosMouseClicked

    private void rdoFechadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoFechadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoFechadosActionPerformed

    private void menuItemImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemImprimirActionPerformed
        imprimirOrcamento();
    }//GEN-LAST:event_menuItemImprimirActionPerformed

    private void rdoTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoTodosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoTodosActionPerformed

    private void btnRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioActionPerformed
        gerarRelatorioTodos();
    }//GEN-LAST:event_btnRelatorioActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRelatorio;
    private javax.swing.JButton buttonAdicionar;
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JMenuItem menuItemImprimir;
    private javax.swing.JRadioButton rdoAbertos;
    private javax.swing.JRadioButton rdoFechados;
    private javax.swing.JRadioButton rdoTodos;
    private javax.swing.JTable table;
    private javax.swing.JTextField textFieldPesquisa;
    // End of variables declaration//GEN-END:variables
}
