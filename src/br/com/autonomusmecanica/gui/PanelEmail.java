/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.EmailDAOJPA;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelEmail;
import br.com.roger.utils.Singleton;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author roger
 */
public class PanelEmail extends javax.swing.JPanel {

    private Pessoa pessoa;
    private TableModelEmail model = new TableModelEmail();
    private final TableCellRenderer renderer = new StripedTable();

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public void limpaTabela() {
        this.pessoa = null;
        model.limpaLista();
    }
    public ImageIcon getIconEmail() {
        ImageIcon tab1Icon = new ImageIcon(this.getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/E-mail.png"));
        return tab1Icon;
    }

    public void atualizaTabelaEmail() {
        EmailDAOJPA dao = new EmailDAOJPA();
        List<Email> lista = dao.getPessoaEmail(pessoa);

        model = new TableModelEmail(lista);
        tableEmail.setModel(model);
        //model.ordenarPorEmail();
        tableEmail.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableEmail.getColumnModel().getColumn(1).setPreferredWidth(30);
        tableEmail.getColumnModel().getColumn(2).setPreferredWidth(30);
        tableEmail.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEmail, 1, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEmail, 2, Singleton.getInstance().iconeJTable("Excluir"));
    }

    private void adicionaEmail() {
        if (this.pessoa != null) {
            DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail:", "Inserir");
            d.setTitle("Cadastrar Email");
            d.setVisible(true);
            EmailDAOJPA dao = new EmailDAOJPA();
            Email e = new Email();
            e.setPessoa((Pessoa) d.resultado);
            e.setEmail(d.descricao);
            if (e.getEmail() != null) {
                dao.save(e);
                atualizaTabelaEmail();
            }
        } else {
            System.out.println("sem pessoa");
        }
    }

    private void alteraEmail() {
        if (this.pessoa != null) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail", "Salvar");
                    d.setDescricao(email.getEmail());
                    d.setTitle("Cadastrar Email");
                    d.setVisible(true);
                    EmailDAOJPA dao = new EmailDAOJPA();
                    email.setPessoa((Pessoa) d.resultado);
                    email.setEmail(d.descricao);
                    dao.save(email);
                    atualizaTabelaEmail();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEmail() {
        if (this.pessoa != null) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o e-mail " + tableEmail.getValueAt(linha, 0) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EmailDAOJPA dao = new EmailDAOJPA();
                        dao.remove(Email.class, email.getIdEmail());
                        atualizaTabelaEmail();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        } else {
            System.out.println("sem pessoa");
        }
    }

    public PanelEmail() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonAddEmail = new javax.swing.JButton();
        ScrollPaneEmail = new javax.swing.JScrollPane();
        tableEmail = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        buttonAddEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEmailActionPerformed(evt);
            }
        });

        tableEmail.setBackground(java.awt.Color.lightGray);
        tableEmail.setAutoscrolls(false);
        tableEmail.setFocusable(false);
        tableEmail.setGridColor(java.awt.Color.lightGray);
        tableEmail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEmailMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEmailMouseReleased(evt);
            }
        });
        ScrollPaneEmail.setViewportView(tableEmail);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonAddEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(buttonAddEmail)
                .addContainerGap(117, Short.MAX_VALUE))
            .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableEmailMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseReleased

    }//GEN-LAST:event_tableEmailMouseReleased

    private void tableEmailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseClicked
        if (tableEmail.getSelectedColumn() == 1) {
            alteraEmail();
        } else if (tableEmail.getSelectedColumn() == 2) {
            excluiEmail();
        }
    }//GEN-LAST:event_tableEmailMouseClicked

    private void buttonAddEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEmailActionPerformed
        adicionaEmail();
    }//GEN-LAST:event_buttonAddEmailActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPaneEmail;
    private javax.swing.JButton buttonAddEmail;
    private javax.swing.JTable tableEmail;
    // End of variables declaration//GEN-END:variables
}
