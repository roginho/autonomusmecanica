package br.com.autonomusmecanica.gui;

import br.com.autonomusconfig.gui.DialogConfig;
import br.com.autonomusmecanica.dao.CidadeDAOJPA;
import br.com.autonomusmecanica.dao.ConfiguracaoDAOJPA;
import br.com.autonomusmecanica.dao.EstadoDAOJPA;
import br.com.autonomusmecanica.dao.FormaPgtoDAOJPA;
import br.com.autonomusmecanica.dao.PaisDAOJPA;
import br.com.autonomusmecanica.entidades.Cidade;
import br.com.autonomusmecanica.entidades.Configuracao;
import br.com.autonomusmecanica.entidades.Estado;
import br.com.autonomusmecanica.entidades.FormaPgto;
import br.com.autonomusmecanica.entidades.Pais;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import java.io.File;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author roger
 */
public class DialogConfiguracaoGeral extends javax.swing.JDialog {

    private Pais pais;
    private Estado estado;
    private Cidade cidade;
    private FormaPgto formaPgto;
    private Configuracao configuracao = null;

    private void salvar() {
        Configuracao c = new Configuracao();
        if (this.configuracao != null) {
            c = this.configuracao;
        }
        if (comboBoxPais.getSelectedIndex() > 0) {
            c.setPais((Pais) comboBoxPais.getSelectedItem());
        } else {
            c.setPais(null);
        }
        if (comboBoxEstado.getSelectedIndex() > 0) {
            c.setEstado((Estado) comboBoxEstado.getSelectedItem());
        } else {
            c.setEstado(null);
        }
        if (comboBoxCidade.getSelectedIndex() > 0) {
            this.configuracao.setCidade((Cidade) comboBoxCidade.getSelectedItem());
        } else {
            c.setCidade(null);
        }
        if (comboBoxFormaPagto.getSelectedIndex() > 0) {
            c.setFormaPgto((FormaPgto) comboBoxFormaPagto.getSelectedItem());
        } else {
            c.setFormaPgto(null);
        }
        ConfiguracaoDAOJPA dao = new ConfiguracaoDAOJPA();
        dao.save(c);
        JOptionPane.showMessageDialog(this, "Registro salvo");
        dispose();
    }

    private Pais buscaPais() {
        this.pais = (Pais) comboBoxPais.getSelectedItem();
        return pais;
    }

    private Estado buscaEstado() {
        if (comboBoxEstado.getSelectedIndex() > 0) {
            this.estado = (Estado) comboBoxEstado.getSelectedItem();
        }
        return estado;
    }

    private Cidade buscaCidade() {
        this.cidade = (Cidade) comboBoxCidade.getSelectedItem();
        return cidade;
    }

    private void carregaComboPais() {
        for (Pais p : new PaisDAOJPA().getAll(Pais.class)) {
            comboBoxPais.addItem(p);
        }
    }

    private void carregaComboEstado() {
        comboBoxEstado.removeAllItems();
        Object falso = "Selecione";
        comboBoxEstado.addItem(falso);
        Pais p = buscaPais();
        if (p != null) {
            comboBoxEstado.setEnabled(true);
            for (Estado e : new EstadoDAOJPA().getEstadoPais(p)) {
                comboBoxEstado.addItem(e);
            }
        } else {
            comboBoxEstado.setEnabled(false);
        }
    }

    private void carregaComboCidade() {
        comboBoxCidade.removeAllItems();
        Object falso = "Selecione";
        comboBoxCidade.addItem(falso);
        Estado e = buscaEstado();
        if (e != null) {
            for (Cidade c : new CidadeDAOJPA().getCidadeEstado(e)) {
                comboBoxCidade.addItem(c);
            }
            comboBoxCidade.setEnabled(true);
        } else {
            comboBoxCidade.setEnabled(false);
        }
    }

    private void carregaComboFormaPgto() {
        for (FormaPgto f : new FormaPgtoDAOJPA().getAll(FormaPgto.class)) {
            comboBoxFormaPagto.addItem(f);
        }
    }

    private void refresh() {
        ConfiguracaoDAOJPA dao = new ConfiguracaoDAOJPA();
        Configuracao c = (Configuracao) dao.primeiroRegistro(Configuracao.class);
        if (c != null) {
            this.configuracao = c;
            comboBoxPais.setSelectedItem(configuracao.getPais());
            comboBoxEstado.setSelectedItem(configuracao.getEstado());
            comboBoxCidade.setSelectedItem(configuracao.getCidade());
            comboBoxFormaPagto.setSelectedItem(configuracao.getFormaPgto());
        }
    }

    public DialogConfiguracaoGeral(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        AutoCompleteDecorator.decorate(this.comboBoxPais);
        AutoCompleteDecorator.decorate(this.comboBoxEstado);
        AutoCompleteDecorator.decorate(this.comboBoxCidade);
        AutoCompleteDecorator.decorate(this.comboBoxFormaPagto);
        Singleton.getInstance().passaCamposComEnter(panel2);
        Singleton.getInstance().passaCamposComEnter(jPanel1);
        SelectAllTheThings.addListeners(panel2);
        SelectAllTheThings.addListeners(jPanel1);
        carregaComboPais();
        carregaComboFormaPgto();
        refresh();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panel2 = new javax.swing.JPanel();
        labelPais = new javax.swing.JLabel();
        comboBoxPais = new javax.swing.JComboBox();
        labelEstado = new javax.swing.JLabel();
        comboBoxEstado = new javax.swing.JComboBox();
        comboBoxCidade = new javax.swing.JComboBox();
        labelCidade = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        comboBoxFormaPagto = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Configurações padrão do sistema"));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeServicos64x64.png"))); // NOI18N

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 333, Short.MAX_VALUE))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
        );

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Salvar");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Pais/Estado/Cidade"));

        labelPais.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelPais.setText("Pais*");

        comboBoxPais.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxPais.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxPais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxPaisItemStateChanged(evt);
            }
        });

        labelEstado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelEstado.setText("Estado*");

        comboBoxEstado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxEstado.setEnabled(false);
        comboBoxEstado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxEstadoItemStateChanged(evt);
            }
        });

        comboBoxCidade.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxCidade.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxCidade.setEnabled(false);
        comboBoxCidade.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxCidadeItemStateChanged(evt);
            }
        });

        labelCidade.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelCidade.setText("Cidade*");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Forma de Pagamento Padrao"));

        comboBoxFormaPagto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxFormaPagto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxFormaPagto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxFormaPagtoItemStateChanged(evt);
            }
        });
        comboBoxFormaPagto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxFormaPagtoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(comboBoxFormaPagto, 0, 300, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(comboBoxFormaPagto, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("x");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPais, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelEstado, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelCidade, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBoxPais, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addGap(0, 44, Short.MAX_VALUE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxPais, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPais))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCidade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(23, 23, 23))))
        );

        jTabbedPane1.addTab("Geral", panel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxPaisItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxPaisItemStateChanged
        carregaComboEstado();
    }//GEN-LAST:event_comboBoxPaisItemStateChanged

    private void comboBoxEstadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxEstadoItemStateChanged
        carregaComboCidade();
    }//GEN-LAST:event_comboBoxEstadoItemStateChanged

    private void comboBoxCidadeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxCidadeItemStateChanged

    }//GEN-LAST:event_comboBoxCidadeItemStateChanged

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void comboBoxFormaPagtoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoItemStateChanged
    }//GEN-LAST:event_comboBoxFormaPagtoItemStateChanged

    private void comboBoxFormaPagtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxFormaPagtoActionPerformed
    }//GEN-LAST:event_comboBoxFormaPagtoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         DialogConfig d = new DialogConfig(null, true);
        d.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonFechar;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JComboBox comboBoxCidade;
    private javax.swing.JComboBox comboBoxEstado;
    private javax.swing.JComboBox comboBoxFormaPagto;
    private javax.swing.JComboBox comboBoxPais;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel labelCidade;
    private javax.swing.JLabel labelEstado;
    private javax.swing.JLabel labelPais;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    // End of variables declaration//GEN-END:variables
}
