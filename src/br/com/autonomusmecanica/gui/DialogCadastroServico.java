package br.com.autonomusmecanica.gui;

import br.com.autonomusconfig.gui.DialogEnvioEmail;
import br.com.autonomusmecanica.dao.ServicoDAOJPA;
import br.com.autonomusmecanica.dao.ServicoItemDAOJPA;
import br.com.autonomusmecanica.dao.ServicoParcelaDAOJPA;
import br.com.autonomusmecanica.dao.VeiculoDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.ServicoItem;
import br.com.autonomusmecanica.entidades.ServicoParcela;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.relatorios.GeraRelatorio;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelServicoItem;
import br.com.autonomusmecanica.tableModel.TableModelServicoParcela;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

public class DialogCadastroServico extends javax.swing.JDialog {

    private Servico servico;
    private Veiculo veiculo;
    private Cliente cliente;
    private TableModelServicoItem model;
    private TableModelServicoParcela modelParcela;
    private final TableCellRenderer renderer = new StripedTable();
    private int colAlterar = 5;
    private int colExcluir = 6;
    private int colAlterarParc = 8;
    private int colExcluirParc = 9;

    public void gerarRel(boolean mostrar) {
        if (servico != null) {
            ServicoItemDAOJPA dao = new ServicoItemDAOJPA();
            List<ServicoItem> lista = dao.getItemServico(servico);
            GeraRelatorio g = new GeraRelatorio();
            g.mostrarCampo(mostrar);
            g.geraRelatorio(lista, "Relatório de Serviço", "reportServicoCliente");
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um serviço", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public File exportaRelPdf() {
        if (servico != null) {
            ServicoItemDAOJPA dao = new ServicoItemDAOJPA();
            List<ServicoItem> lista = dao.getItemServico(servico);
            GeraRelatorio g = new GeraRelatorio();
            String nomeRelatorio = "reportServicoCliente";
            g.geraRelatorioPDF(lista, "Relatório de Serviço", nomeRelatorio, servico.getIdServico() + "");
            File fi = new File(Singleton.getInstance().caminhoPDF() + "/" + servico.getIdServico() + "_" + nomeRelatorio+".pdf");
            return fi;
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um serviço", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
        return null;
    }

    private void adicionarItens() {
        if (servico == null) {
            salvar(true);
        }
        if (servico != null && servico.getStatus().equals('a')) {
            DialogCadastraItens d = new DialogCadastraItens(null, true, null, servico);
            d.setVisible(true);
            atualizaTabela();
            salvar(false);
        } else {
            JOptionPane.showMessageDialog(this, "Não foi possivel incluir. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void adicionarParcela() {
        if (servico == null) {
            salvar(true);
        }
        if (servico != null && servico.getStatus().equals('a')) {
            int rowCount = tableParcela.getRowCount() + 1;

            ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
            Double saldo = dao.getSaldoRestanteServico(servico);
            DialogParcelaServico d = new DialogParcelaServico(null, true, servico, null, rowCount, saldo);
            d.setVisible(true);
            atualizaTabelaParcela();
            salvar(false);

        } else {
            JOptionPane.showMessageDialog(this, "Não foi possivel incluir. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void atualizaTabela() {
        ServicoItemDAOJPA dao = new ServicoItemDAOJPA();
        List<ServicoItem> lista = dao.getItemServico(servico);
        model = new TableModelServicoItem(lista);
        table.setModel(model);
        table.getColumnModel().getColumn(0).setMinWidth(0);
        table.getColumnModel().getColumn(0).setMaxWidth(0);
        table.getColumnModel().getColumn(0).setPreferredWidth(0);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.getColumnModel().getColumn(2).setPreferredWidth(250);
        table.getColumnModel().getColumn(3).setPreferredWidth(20);
        table.getColumnModel().getColumn(4).setPreferredWidth(20);
        table.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(table, colAlterar, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(table, colExcluir, Singleton.getInstance().iconeJTable("Excluir"));
        table.setRowHeight(20);
        somaTotal();
    }

    public void atualizaTabelaParcela() {
        ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
        List<ServicoParcela> lista = dao.getParcelasServico(servico);
        modelParcela = new TableModelServicoParcela(lista);
        tableParcela.setModel(modelParcela);
        tableParcela.getColumnModel().getColumn(0).setMinWidth(0);
        tableParcela.getColumnModel().getColumn(0).setMaxWidth(0);
        tableParcela.getColumnModel().getColumn(0).setPreferredWidth(0);
        tableParcela.getColumnModel().getColumn(1).setPreferredWidth(10);
        tableParcela.getColumnModel().getColumn(2).setPreferredWidth(100);
        tableParcela.getColumnModel().getColumn(3).setPreferredWidth(20);
        tableParcela.getColumnModel().getColumn(4).setPreferredWidth(20);
        tableParcela.getColumnModel().getColumn(5).setPreferredWidth(20);
        tableParcela.getColumnModel().getColumn(6).setPreferredWidth(20);
        tableParcela.getColumnModel().getColumn(7).setPreferredWidth(80);
        tableParcela.setDefaultRenderer(Object.class, renderer);
        tableParcela.getColumnModel().getColumn(colAlterarParc).setPreferredWidth(20);
        tableParcela.getColumnModel().getColumn(colExcluirParc).setPreferredWidth(20);
        new ButtonColumn(tableParcela, colAlterarParc, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableParcela, colExcluirParc, Singleton.getInstance().iconeJTable("Excluir"));
        tableParcela.setRowHeight(20);
    }

    private void excluirItens() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            ServicoItem e = (ServicoItem) table.getValueAt(linha, 0);
            if (e != null) {
                if (e.getServico().getStatus().equals('a')) {
                    ServicoItemDAOJPA dao = new ServicoItemDAOJPA();
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja excluir o item \n" + e.getDescricao() + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    //0=sim e 1=nao 
                    if (showConfirmDialog == 0) {
                        dao.remove(ServicoItem.class, e.getIdServicoItem());
                        atualizaTabela();
                        salvar(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Não foi possivel excluir. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    private void alterarItens() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            ServicoItem si = (ServicoItem) table.getValueAt(linha, 0);
            if (si != null) {
                if (si.getServico().getStatus().equals('a')) {
                    DialogCadastraItens d = new DialogCadastraItens(null, true, si, si.getServico());
                    d.setVisible(true);
                    atualizaTabela();
                    salvar(false);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Não é possível alterar. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void alterarParcela() {
        int linha = tableParcela.getSelectedRow();
        if (linha >= 0) {
            ServicoParcela sp = (ServicoParcela) tableParcela.getValueAt(linha, 0);
            ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
            Double saldo = dao.getSaldoRestanteServico(servico);
            if (sp != null) {
                if (sp.getServico().getStatus().equals('a')) {
                    DialogParcelaServico d = new DialogParcelaServico(null, true, servico, sp, sp.getParcela(), saldo);
                    d.setVisible(true);
                    atualizaTabelaParcela();
                    salvar(false);

                } else {
                    JOptionPane.showMessageDialog(this, "Não é possível alterar. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    private void excluirParcela() {
        int linha = tableParcela.getSelectedRow();
        if (linha >= 0) {
            ServicoParcela e = (ServicoParcela) tableParcela.getValueAt(linha, 0);
            if (e != null) {
                if (e.getServico().getStatus().equals('a')) {
                    ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja excluir a parcela " + e.getParcela() + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    //0=sim e 1=nao 
                    if (showConfirmDialog == 0) {
                        dao.remove(ServicoParcela.class, e.getIdServicoParcela());
                        atualizaTabelaParcela();
                        salvar(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Não foi possivel excluir. Serviço já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    private void abreBuscaVeiculo() {
        DialogTabelaVeiculo d = new DialogTabelaVeiculo(null, true, null);
        d.setVisible(true);
        this.veiculo = d.getVeiculo();
        refreshVeiculo();
    }

    private void buscaVeiculo() {
        VeiculoDAOJPA dao = new VeiculoDAOJPA();
        List<Veiculo> veiculoPlaca = dao.getVeiculoPlaca(txtPlaca.getText());
        if (veiculoPlaca != null && !veiculoPlaca.isEmpty()) {
            this.veiculo = veiculoPlaca.get(0);
            if (veiculo.getCliente() != null) {
                refreshVeiculo();
            } else {
                JOptionPane.showMessageDialog(this, "Veículo sem cliente! Vincule o cliente ao veículo primeiro", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Placa não encontrada", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            limpaTela();
        }
    }

    private void refreshVeiculo() {
        if (this.veiculo != null) {
            txtPlaca.setText(veiculo.getPlaca());
            lblModelo.setText(veiculo.getModelo().getDescricao() + " - " + veiculo.getModelo().getFabricante().getNome());
            lblAno.setText(veiculo.getAno() + "");
            if (veiculo.getCor().getCodigo() != null && !veiculo.getCor().getCodigo().isEmpty()) {
                lblCor.setBackground(Color.decode(veiculo.getCor().getCodigo()));
            } else {
                lblCor.setBackground(null);
            }
            if (veiculo.getCliente() != null) {
                lblNome.setText(veiculo.getCliente().getPessoa().getNome());
                lblCpfCnpj.setText(veiculo.getCliente().getPessoa().getCpfCnpj());
            }
            txtKm.requestFocus();
        }
    }

    private void salvar(Boolean msg) {
        if (veiculo == null) {
            JOptionPane.showMessageDialog(this, "Preencha o veículo");
            txtPlaca.requestFocus();
        } else if (dChooser.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Preencha a data");
            dChooser.requestFocus();
        } else {
            Servico s = new Servico();
            if (this.servico == null) {
                s.setStatus('a');
                //System.out.println("inserir");
            } else {
                //System.out.println("atualizar");
                s = this.servico;
            }
            s.setVeiculo(veiculo);
            s.setCliente(veiculo.getCliente());
            if (txtKm.getText() != null && !txtKm.getText().trim().isEmpty()) {
                s.setKilometragem(Integer.valueOf(txtKm.getText()));
            }
            s.setDataServico(dChooser.getDate());
            s.setValorTotal(Singleton.getInstance().converteStringParaDouble(lblTotal.getText()));
            s.setValorMO(Singleton.getInstance().converteStringParaDouble(txtMO.getText()));
            s.setDescricao(txtDescricao.getText());
            ServicoDAOJPA dao = new ServicoDAOJPA();
            this.servico = dao.save(s);
            refreshForm();
            if (msg) {
                JOptionPane.showMessageDialog(this, "Registro salvo");
            }
        }
    }

    private void refreshForm() {
        limpaTela();
        refreshVeiculo();
        if (servico.getDataServico() != null) {
            dChooser.setDate(servico.getDataServico());
        }
        lblCodigo.setText(servico.getIdServico() + "");
        lblStatus.setText(servico.getStatusString());
        txtKm.setText("" + servico.getKilometragem());
        txtDescricao.setText(servico.getDescricao());
        txtMO.setText(Singleton.getInstance().converteDoubleParaString(servico.getValorMO(), 2));
        lblTotal.setText(Singleton.getInstance().converteDoubleParaString(servico.getValorTotal(), 2));
        atualizaTabela();
        atualizaTabelaParcela();
    }

    private void limpaTela() {
        lblModelo.setText("");
        lblAno.setText("");
        lblCor.setText("");
        lblCpfCnpj.setText("");
        lblNome.setText("");
        lblTotalPecas.setText("0,00");
        lblTotal.setText("0,00");
        txtMO.setText("0,00");
        txtDescricao.setText("");
    }

    private void novo() {
        this.limpaTela();
        dChooser.setDate(new Date());
        this.servico = null;
        this.cliente = null;
    }

    private void buscaPrimeiroRegistro() {
        ServicoDAOJPA dao = new ServicoDAOJPA();
        this.servico = (Servico) dao.primeiroRegistro(Servico.class);
        refreshForm();
    }

    private void buscaUltimoRegistro() {
        ServicoDAOJPA dao = new ServicoDAOJPA();
        this.servico = (Servico) dao.ultimoRegistro(Servico.class, "idServico");
        refreshForm();
    }

    private void proximo() {
        if (this.servico == null) {
            buscaUltimoRegistro();
        } else {
            ServicoDAOJPA dao = new ServicoDAOJPA();
            this.servico = (Servico) dao.getProximo(Servico.class, "idServico", this.servico.getIdServico());
            if (this.servico != null) {
                refreshForm();
            }
        }
    }

    private void anterior() {
        if (this.servico == null) {
            buscaPrimeiroRegistro();
        } else {
            ServicoDAOJPA dao = new ServicoDAOJPA();
            this.servico = (Servico) dao.getAnterior(Servico.class, "idServico", this.servico.getIdServico());
            if (this.servico != null) {
                refreshForm();
            }
        }
    }

    private void somaTotal() {
        Double mo = Singleton.getInstance().converteStringParaDouble(txtMO.getText());
        Double itens = 0d;
        for (int x = 0; x < table.getRowCount(); x++) {
            ServicoItem item = (ServicoItem) table.getValueAt(x, 0);
            itens += item.getValorTotal();
        }
        lblTotalPecas.setText(Singleton.getInstance().converteDoubleParaString(itens, 2));
        Double tot = (mo + itens);
        lblTotal.setText(Singleton.getInstance().converteDoubleParaString(tot, 2));
    }

    private void enviarEmail() {
        if (servico == null) {
            salvar(true);
        }
        if (servico != null) {
            if (servico.getCliente().getPessoa().getEmailCollection() != null) {
                String email = null;
                for (Email e : servico.getCliente().getPessoa().getEmailCollection()) {
                    email = e.getEmail();
                }
                File exportaRelPdf = exportaRelPdf();
                DialogEnvioEmail d = new DialogEnvioEmail(null, true, email, exportaRelPdf);
                d.setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um serviço.", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public DialogCadastroServico(java.awt.Frame parent, boolean modal, Servico s, int tabPane) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        if (s == null) {
            novo();
        } else {
//            System.out.println("altera");
            this.servico = s;
            this.veiculo = s.getVeiculo();
            refreshForm();
        }
        txtPlaca.requestFocus();
        tabbedPane.setSelectedIndex(tabPane);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnBusca = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtPlaca = new javax.swing.JFormattedTextField();
        lblModelo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblCpfCnpj = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblAno = new javax.swing.JLabel();
        lblCor = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dChooser = new com.toedter.calendar.JDateChooser();
        txtKm = new javax.swing.JTextField();
        lblTotal = new javax.swing.JLabel();
        btnRelatorio = new javax.swing.JButton();
        tabbedPane = new javax.swing.JTabbedPane();
        panel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        btnAddItem = new javax.swing.JButton();
        txtMO = new javax.swing.JFormattedTextField();
        lblTotalPecas = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableParcela = new javax.swing.JTable();
        btnAddItem1 = new javax.swing.JButton();
        btnRelatorio1 = new javax.swing.JButton();
        btnRelatorioComCusto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Serviços"));

        lblCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/Servico64.png"))); // NOI18N

        lblStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setText("Status:");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(291, 291, 291))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonPrev)
                    .addComponent(buttonNext)
                    .addComponent(buttonNext1)
                    .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1, lblCodigo});

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Apply.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Close.png"))); // NOI18N
        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/New document.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));

        btnBusca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaActionPerformed(evt);
            }
        });

        jLabel5.setText("Placa:");

        try {
            txtPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-#A##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtPlaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlacaKeyPressed(evt);
            }
        });

        lblModelo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("KM:");

        jLabel8.setText("Veículo:");

        jLabel9.setText("Cor:");

        lblCpfCnpj.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Ano:");

        jLabel11.setText("Nome:");

        lblNome.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setText("Cpf/Cnpj:");

        lblAno.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblCor.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblCor.setOpaque(true);

        jLabel2.setText("Data:");

        txtKm.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtKmKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5))
                        .addGap(2, 2, 2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel11)))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBusca)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblModelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtKm, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(dChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblAno, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(12, 12, 12)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblCpfCnpj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel5)
                    .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(lblModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dChooser, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtKm)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(lblAno, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(lblCor, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblCpfCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(12, 12, 12))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblAno, lblCor, lblCpfCnpj, lblModelo, lblNome, txtPlaca});

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("0,00");
        lblTotal.setToolTipText("");
        lblTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        btnRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorio.setText("Relatório");
        btnRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        btnAddItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        btnAddItem.setToolTipText("Adicionar");
        btnAddItem.setBorder(null);
        btnAddItem.setBorderPainted(false);
        btnAddItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItemActionPerformed(evt);
            }
        });
        btnAddItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddItemKeyPressed(evt);
            }
        });

        txtMO.setBorder(javax.swing.BorderFactory.createTitledBorder("M.O"));
        txtMO.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txtMO.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtMO.setText("0,00");
        txtMO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMO.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMOFocusLost(evt);
            }
        });

        lblTotalPecas.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotalPecas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalPecas.setText("0,00");
        lblTotalPecas.setToolTipText("");
        lblTotalPecas.setBorder(javax.swing.BorderFactory.createTitledBorder("Total Peças"));

        txtDescricao.setColumns(20);
        txtDescricao.setRows(3);
        txtDescricao.setBorder(javax.swing.BorderFactory.createTitledBorder("Descrição:"));
        jScrollPane3.setViewportView(txtDescricao);

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblTotalPecas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMO, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddItem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addComponent(btnAddItem, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addComponent(lblTotalPecas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tabbedPane.addTab("Materiais", panel2);

        tableParcela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Parcela", "Data", "Forma Pagamento", "Valor"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableParcela.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableParcelaMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableParcelaMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(tableParcela);

        btnAddItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        btnAddItem1.setToolTipText("Adicionar");
        btnAddItem1.setBorder(null);
        btnAddItem1.setBorderPainted(false);
        btnAddItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItem1ActionPerformed(evt);
            }
        });
        btnAddItem1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddItem1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddItem1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 932, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnAddItem1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Parcelas", jPanel2);

        btnRelatorio1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/E-mail.png"))); // NOI18N
        btnRelatorio1.setText("Enviar E-mail");
        btnRelatorio1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorio1ActionPerformed(evt);
            }
        });

        btnRelatorioComCusto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorioComCusto.setText("Relatório Custo");
        btnRelatorioComCusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioComCustoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRelatorioComCusto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRelatorio1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tabbedPane))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabbedPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvar)
                        .addComponent(btnFechar)
                        .addComponent(btnNovo)
                        .addComponent(btnRelatorio)
                        .addComponent(btnRelatorio1)
                        .addComponent(btnRelatorioComCusto))
                    .addComponent(lblTotal))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void btnAddItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItemActionPerformed
        adicionarItens();
    }//GEN-LAST:event_btnAddItemActionPerformed

    private void btnBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaActionPerformed
        abreBuscaVeiculo();
    }//GEN-LAST:event_btnBuscaActionPerformed

    private void txtPlacaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            buscaVeiculo();
        }
    }//GEN-LAST:event_txtPlacaKeyPressed

    private void btnAddItemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddItemKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnAddItemActionPerformed(null);
        }
    }//GEN-LAST:event_btnAddItemKeyPressed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        novo();
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        salvar(true);
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtMOFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMOFocusLost
        somaTotal();
    }//GEN-LAST:event_txtMOFocusLost

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colAlterar) {
            alterarItens();
        } else if (table.getSelectedColumn() == colExcluir) {
            excluirItens();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void btnRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioActionPerformed
        gerarRel(false);
    }//GEN-LAST:event_btnRelatorioActionPerformed

    private void txtKmKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtKmKeyTyped
        String caracteres = "0987654321";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtKmKeyTyped

    private void tableParcelaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableParcelaMouseClicked
        if (tableParcela.getSelectedColumn() == colAlterarParc) {
            alterarParcela();
        } else if (tableParcela.getSelectedColumn() == colExcluirParc) {
            excluirParcela();
        }
    }//GEN-LAST:event_tableParcelaMouseClicked

    private void tableParcelaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableParcelaMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableParcelaMouseReleased

    private void btnAddItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItem1ActionPerformed
        adicionarParcela();
    }//GEN-LAST:event_btnAddItem1ActionPerformed

    private void btnAddItem1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddItem1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAddItem1KeyPressed

    private void btnRelatorio1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorio1ActionPerformed
        enviarEmail();
    }//GEN-LAST:event_btnRelatorio1ActionPerformed

    private void btnRelatorioComCustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioComCustoActionPerformed
                gerarRel(true);
    }//GEN-LAST:event_btnRelatorioComCustoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddItem;
    private javax.swing.JButton btnAddItem1;
    private javax.swing.JButton btnBusca;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnRelatorio;
    private javax.swing.JButton btnRelatorio1;
    private javax.swing.JButton btnRelatorioComCusto;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private com.toedter.calendar.JDateChooser dChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblAno;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblCor;
    private javax.swing.JLabel lblCpfCnpj;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalPecas;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTable table;
    private javax.swing.JTable tableParcela;
    private javax.swing.JTextArea txtDescricao;
    private javax.swing.JTextField txtKm;
    private javax.swing.JFormattedTextField txtMO;
    private javax.swing.JFormattedTextField txtPlaca;
    // End of variables declaration//GEN-END:variables
}
