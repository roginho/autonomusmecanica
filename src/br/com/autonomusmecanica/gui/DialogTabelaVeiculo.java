package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.VeiculoDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.ColorRenderer;
import br.com.autonomusmecanica.tableModel.TableModelVeiculo;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

public final class DialogTabelaVeiculo extends javax.swing.JDialog {

    private TableModelVeiculo model;
    private final TableCellRenderer renderer = new StripedTable();
    private TableRowSorter sorter;
    private final int colunaCor = 3;
    private final int colunaAlterar = 5;
    private final int colunaExcluir = 6;
    private Cliente cliente;
    private Veiculo veiculo;

    public Veiculo getVeiculo() {
        return veiculo;
    }
    

    private void atualizaTabela() {
        VeiculoDAOJPA dao = new VeiculoDAOJPA();
        List<Veiculo> lista = null;
        if (rdoVinculados.isSelected()) {
            lista = dao.getVeiculosVinculados();
        } else if (rdoNaoVinculados.isSelected()) {
            lista = dao.getVeiculosNaoVinculados();
        } else {
            lista = dao.getAll(Veiculo.class);
        }
        model = new TableModelVeiculo(lista);
        table.setModel(model);
        model.ordenarPorDescricao();
        table.getColumnModel().getColumn(0).setPreferredWidth(30);
        table.getColumnModel().getColumn(1).setPreferredWidth(20);
        table.getColumnModel().getColumn(2).setPreferredWidth(20);
        table.getColumnModel().getColumn(colunaCor).setPreferredWidth(20);
        table.getColumnModel().getColumn(4).setPreferredWidth(200);
        table.getColumnModel().getColumn(colunaAlterar).setPreferredWidth(20);
        table.getColumnModel().getColumn(colunaExcluir).setPreferredWidth(20);
        table.setDefaultRenderer(Object.class, renderer);
        ColorRenderer colorRenderer = new ColorRenderer(true);
        TableColumn column = table.getColumnModel().getColumn(colunaCor);
        column.setCellRenderer(colorRenderer);
        new ButtonColumn(table, colunaAlterar, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(table, colunaExcluir, Singleton.getInstance().iconeJTable("Excluir"));
        sorter = new TableRowSorter<>(model);
        labelTotal.setText("" + table.getRowCount());
        table.setRowSorter(sorter);
    }

    private void alterar() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Veiculo f = new Veiculo();
            f = (Veiculo) table.getValueAt(linha, -1);
            DialogCadastroVeiculo d = new DialogCadastroVeiculo(null, true, f);
            d.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(this, "Selecione uma linha para alterar");
        }
    }

    private void excluir() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Veiculo f = (Veiculo) table.getValueAt(linha, -1);
            if (f != null) {
                VeiculoDAOJPA dao = new VeiculoDAOJPA();
                int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja excluir o veículo? \n" + f.getPlaca(), "Excluir", JOptionPane.YES_NO_OPTION);
                //0=sim e 1=nao 
                if (showConfirmDialog == 0) {
                    dao.remove(Veiculo.class, f.getIdVeiculo());
                    atualizaTabela();
                }
            }
        }
    }

    private void vincularVeiculo() {
        boolean fechar = false;
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Veiculo f = (Veiculo) table.getValueAt(linha, -1);
            if (f != null) {
                if (f.getCliente() == null) {
                    if (cliente == null) {
                        DialogTabelaClientes d = new DialogTabelaClientes(null, true);
                        d.setVisible(true);
                        cliente = d.getCli();
                    } else {
                        fechar = true;
                    }
                    if (cliente != null) {
                        VeiculoDAOJPA dao = new VeiculoDAOJPA();
                        int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja vincular o veículo placa: ? " + f.getPlaca() + "\n"
                                + "ao cliente: " + cliente.getPessoa().getNome(), "Vincular", JOptionPane.YES_NO_OPTION);
                        //0=sim e 1=nao 
                        if (showConfirmDialog == 0) {
                            f.setCliente(cliente);
                            dao.save(f);
                            atualizaTabela();
                            if (fechar) {
                                dispose();
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "nenhum cliente selecionado");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Veículo já vinculado ao cliente: " + f.getCliente().getPessoa().getNome() + "");
                }
            }
        }
    }

    private void removerVeiculo() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Veiculo f = (Veiculo) table.getValueAt(linha, -1);
            if (f != null) {
                if (f.getCliente() != null) {
                    VeiculoDAOJPA dao = new VeiculoDAOJPA();
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja remover o veículo placa: ? " + f.getPlaca() + "\n"
                            + "do cliente: " + f.getCliente().getPessoa().getNome(), "Remover", JOptionPane.YES_NO_OPTION);
                    //0=sim e 1=nao 
                    if (showConfirmDialog == 0) {
                        f.setCliente(null);
                        dao.save(f);
                        atualizaTabela();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Nenhum cliente vinculado ao veículo", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    public DialogTabelaVeiculo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Veículo");
        rdoTodos.setSelected(true);
        atualizaTabela();
        textFieldPesquisa.requestFocus();
    }

    public DialogTabelaVeiculo(java.awt.Frame parent, boolean modal, Cliente cli) {
        super(parent, modal);
        this.cliente = cli;
        initComponents();
        this.setTitle("Veículos");
        if(cliente != null){
        rdoNaoVinculados.setSelected(true);
        }else{
            rdoVinculados.setSelected(true);
        }
        atualizaTabela();
        textFieldPesquisa.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenu = new javax.swing.JPopupMenu();
        mnutemVincular = new javax.swing.JMenuItem();
        mnuItemRemover = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        buttonAdicionar = new javax.swing.JButton();
        textFieldPesquisa = new javax.swing.JTextField();
        buttonFechar = new javax.swing.JButton();
        labelTotal = new javax.swing.JLabel();
        rdoTodos = new javax.swing.JRadioButton();
        rdoVinculados = new javax.swing.JRadioButton();
        rdoNaoVinculados = new javax.swing.JRadioButton();

        mnutemVincular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Male.png"))); // NOI18N
        mnutemVincular.setText("Vincular");
        mnutemVincular.setToolTipText("");
        mnutemVincular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnutemVincularActionPerformed(evt);
            }
        });
        popupMenu.add(mnutemVincular);

        mnuItemRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Erase.png"))); // NOI18N
        mnuItemRemover.setText("remover");
        mnuItemRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemRemoverActionPerformed(evt);
            }
        });
        popupMenu.add(mnuItemRemover);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Objeto", "Descricao", "Ativo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.setComponentPopupMenu(popupMenu);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        buttonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeAdicionar32x32.png"))); // NOI18N
        buttonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAdicionarActionPerformed(evt);
            }
        });

        textFieldPesquisa.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro"));
        textFieldPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyReleased(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        labelTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        buttonGroup1.add(rdoTodos);
        rdoTodos.setText("Todos");
        rdoTodos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoTodosMouseClicked(evt);
            }
        });

        buttonGroup1.add(rdoVinculados);
        rdoVinculados.setText("Vinculados");
        rdoVinculados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoVinculadosMouseClicked(evt);
            }
        });
        rdoVinculados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoVinculadosActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdoNaoVinculados);
        rdoNaoVinculados.setText("Não Vinculados");
        rdoNaoVinculados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rdoNaoVinculadosMouseClicked(evt);
            }
        });
        rdoNaoVinculados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdoNaoVinculadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(buttonAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonFechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textFieldPesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                        .addGap(7, 7, 7)
                        .addComponent(rdoTodos)
                        .addGap(5, 5, 5)
                        .addComponent(rdoVinculados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdoNaoVinculados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(textFieldPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoTodos)
                        .addComponent(rdoVinculados)
                        .addComponent(rdoNaoVinculados))
                    .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAdicionarActionPerformed
        DialogCadastroVeiculo d = new DialogCadastroVeiculo(null, true, null);
        d.setVisible(true);
    }//GEN-LAST:event_buttonAdicionarActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        atualizaTabela();
    }//GEN-LAST:event_formWindowActivated

    private void textFieldPesquisaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyTyped

    }//GEN-LAST:event_textFieldPesquisaKeyTyped

    private void textFieldPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyReleased
        if (table.getSelectedColumn() > 0) {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), table.getSelectedColumn());
        } else {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), 0);
        }

        labelTotal.setText("" + table.getRowCount());
    }//GEN-LAST:event_textFieldPesquisaKeyReleased

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colunaAlterar) {
            alterar();
        } else if (table.getSelectedColumn() == colunaExcluir) {
            excluir();
        } else if (evt.getClickCount() == 2) {
            int linha = table.getSelectedRow();
            Veiculo f = (Veiculo) table.getValueAt(linha, -1);
            if (f != null) {
                this.veiculo = f;
                dispose();
            }
        }
    }//GEN-LAST:event_tableMouseClicked

    private void mnutemVincularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnutemVincularActionPerformed
        vincularVeiculo();
    }//GEN-LAST:event_mnutemVincularActionPerformed

    private void mnuItemRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemRemoverActionPerformed
        removerVeiculo();
    }//GEN-LAST:event_mnuItemRemoverActionPerformed

    private void rdoVinculadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoVinculadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoVinculadosActionPerformed

    private void rdoNaoVinculadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdoNaoVinculadosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdoNaoVinculadosActionPerformed

    private void rdoTodosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoTodosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoTodosMouseClicked

    private void rdoVinculadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoVinculadosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoVinculadosMouseClicked

    private void rdoNaoVinculadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rdoNaoVinculadosMouseClicked
        atualizaTabela();
    }//GEN-LAST:event_rdoNaoVinculadosMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdicionar;
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JMenuItem mnuItemRemover;
    private javax.swing.JMenuItem mnutemVincular;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JRadioButton rdoNaoVinculados;
    private javax.swing.JRadioButton rdoTodos;
    private javax.swing.JRadioButton rdoVinculados;
    private javax.swing.JTable table;
    private javax.swing.JTextField textFieldPesquisa;
    // End of variables declaration//GEN-END:variables
}
