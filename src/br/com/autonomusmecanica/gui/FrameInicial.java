package br.com.autonomusmecanica.gui;

import br.com.autonomusconfig.utils.Arquivos;
import br.com.autonomusmecanica.dao.UsuarioDAOJPA;
import br.com.autonomusmecanica.dao.UsuarioLogDAOJPA;
import br.com.autonomusmecanica.entidades.Usuario;
import br.com.autonomusmecanica.entidades.UsuarioLog;
import br.com.roger.utils.Singleton;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author roger
 */
public class FrameInicial extends javax.swing.JFrame {

    public void threadLogar() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                labelCarregando.setVisible(true);
                logar();
                labelCarregando.setVisible(false);
            }
        });
        thread.start();
    }

    private void lembrarUsuarioSenha() {
    }

    private void salvarLembrarSenha(Usuario u) {
        File f = new File(Singleton.getInstance().caminhoLembrarSenha());
        if (f.exists()) {
            f.delete();
        }
        if (checkBoxLembrar.isSelected()) {
            Arquivos a = new Arquivos(f);
            String[] obj = new String[2];
            obj[0] = textFieldLogin.getText();
            char[] senhaCripto = textFieldSenha.getPassword();
            String senha = new String(senhaCripto);
            obj[1] = senha;
            a.gravaDados(obj, f.getAbsolutePath());
        }
    }

    private void logar() {
        if (textFieldLogin.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o login");
            textFieldLogin.requestFocus();
        } else {
            UsuarioDAOJPA dao = new UsuarioDAOJPA();
            Usuario u = (Usuario) dao.getUsuariosByLogin(textFieldLogin.getText(), textFieldSenha.getPassword());
            if (u != null) {
                if (u.getPessoa().getStatus().equals('a')) {
                    salvarLembrarSenha(u);
                    UsuarioLog log = new UsuarioLog();
                    log.setUsuario(u);
                    log.setDataHora(new Date());
                    String[] computador = Singleton.getInstance().Computador();
                    UsuarioLogDAOJPA daoLog = new UsuarioLogDAOJPA();
                    log.setIp(computador[0]);
                    log.setHost(computador[1]);
                    log.setDataHora(new Date());
                    //            daoLog.save(log);
                    FramePrincipal f = new FramePrincipal(u);
                    f.setVisible(true);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Usuário inativo");
                    textFieldSenha.setText("");
                    textFieldLogin.requestFocus();
                }
            } else {
                JOptionPane.showMessageDialog(this, "Usuário ou senha invalida");
                textFieldSenha.setText("");
                textFieldLogin.requestFocus();
            }
        }
        labelCarregando.setVisible(false);
    }

    public FrameInicial() {
        initComponents();
        labelCarregando.setVisible(false);
        Singleton.getInstance().passaCamposComEnter(panel);
        File f = Singleton.getInstance().lembrarSenha();
        if (f.exists()) {
            checkBoxLembrar.setSelected(true);
            Arquivos a = new Arquivos(f);
            String[] obj = (String[]) a.leDados();
            if (obj != null) {
                textFieldLogin.setText(obj[0]);
                textFieldSenha.setText(obj[1]);
                buttonEntrar.requestFocus();
            }
        } else {
            textFieldLogin.requestFocus();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonEntrar = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        textFieldLogin = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textFieldSenha = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        checkBoxLembrar = new javax.swing.JCheckBox();
        buttonSair = new javax.swing.JButton();
        labelCarregando = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login");
        setResizable(false);
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });

        buttonEntrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonEntrar.setText("Entrar");
        buttonEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEntrarActionPerformed(evt);
            }
        });
        buttonEntrar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                buttonEntrarKeyPressed(evt);
            }
        });

        panel.setBorder(javax.swing.BorderFactory.createTitledBorder("Login"));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Login:");

        textFieldLogin.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Senha:");

        textFieldSenha.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Key.png"))); // NOI18N

        checkBoxLembrar.setText("Lembrar usuário/senha");

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textFieldSenha, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                    .addComponent(textFieldLogin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkBoxLembrar)
                .addContainerGap())
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(textFieldLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(textFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkBoxLembrar)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        buttonSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonSair.setText("Sair");
        buttonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSairActionPerformed(evt);
            }
        });

        labelCarregando.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/processando.gif"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(buttonEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(buttonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(116, 116, 116)
                            .addComponent(labelCarregando))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelCarregando)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEntrarActionPerformed
        threadLogar();
    }//GEN-LAST:event_buttonEntrarActionPerformed

    private void buttonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_buttonSairActionPerformed

    private void buttonEntrarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buttonEntrarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonEntrarActionPerformed(null);
        }
    }//GEN-LAST:event_buttonEntrarKeyPressed

    private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange

    }//GEN-LAST:event_formPropertyChange

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrameInicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonEntrar;
    private javax.swing.JButton buttonSair;
    private javax.swing.JCheckBox checkBoxLembrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel labelCarregando;
    private javax.swing.JPanel panel;
    private javax.swing.JTextField textFieldLogin;
    private javax.swing.JPasswordField textFieldSenha;
    // End of variables declaration//GEN-END:variables
}
