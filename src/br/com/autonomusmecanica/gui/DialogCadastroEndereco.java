/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.CidadeDAOJPA;
import br.com.autonomusmecanica.dao.ConfiguracaoDAOJPA;
import br.com.autonomusmecanica.dao.EstadoDAOJPA;
import br.com.autonomusmecanica.dao.PaisDAOJPA;
import br.com.autonomusmecanica.entidades.Cidade;
import br.com.autonomusmecanica.entidades.Configuracao;
import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Estado;
import br.com.autonomusmecanica.entidades.Pais;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author roger
 */
public class DialogCadastroEndereco extends javax.swing.JDialog {

    public Endereco endereco;
    private Pais pais;
    private Estado estado;
    private Cidade cidade;
    private Configuracao config;

    private void inserir() {
        if (txtLogradouro.getText().trim().equals("")) {
            txtLogradouro.requestFocus();
            JOptionPane.showMessageDialog(this, "Preencha o logradouro");
            return;
        }
        if (txtNumero.getText().trim().equals("")) {
            txtNumero.requestFocus();
            JOptionPane.showMessageDialog(this, "Preencha o número");
            return;
        }
        if (comboBoxCidade.getSelectedIndex() == 0) {
            comboBoxCidade.requestFocus();
            JOptionPane.showMessageDialog(this, "Preencha a cidade");
            return;
        }
        Endereco e = new Endereco();
        e.setPessoa(this.endereco.getPessoa());
        e.setLogradouro(txtLogradouro.getText());
        e.setNumero(txtNumero.getText());
        e.setComplemento(txtCompl.getText());
        e.setBairro(txtBairro.getText());
        e.setCep(txtCep.getText());
        e.setCidade((Cidade) comboBoxCidade.getSelectedItem());
        this.endereco = e;
        dispose();
    }

    public void refresh() {
        if (endereco.getIdEndereco() != null) {
            txtLogradouro.setText(this.endereco.getLogradouro());
            txtNumero.setText(this.endereco.getNumero());
            txtCompl.setText(this.endereco.getComplemento());
            txtBairro.setText(this.endereco.getBairro());
            txtCep.setText(this.endereco.getCep());
            comboBoxPais.setSelectedItem(this.endereco.getCidade().getEstado().getPais());
            comboBoxEstado.setSelectedItem(this.endereco.getCidade().getEstado());
            comboBoxCidade.setSelectedItem(this.endereco.getCidade());
        } else {
            ConfiguracaoDAOJPA dao = new ConfiguracaoDAOJPA();
            this.config = (Configuracao) dao.primeiroRegistro(Configuracao.class);
            if (config != null) {
                if (this.config.getPais() != null) {
                    comboBoxPais.setSelectedItem(this.config.getPais());
                }
                if (this.config.getEstado() != null) {
                    comboBoxEstado.setSelectedItem(this.config.getEstado());
                }
                if (this.config.getCidade() != null) {
                    comboBoxCidade.setSelectedItem(this.config.getCidade());
                }
            }
        }
    }

    private Pais buscaPais() {
        this.pais = (Pais) comboBoxPais.getSelectedItem();
        return pais;
    }

    private Estado buscaEstado() {
        if (comboBoxEstado.getSelectedIndex() > 0) {
            this.estado = (Estado) comboBoxEstado.getSelectedItem();
        }
        return estado;
    }

    private Cidade buscaCidade() {
        this.cidade = (Cidade) comboBoxCidade.getSelectedItem();
        return cidade;
    }

    private void carregaComboPais() {
        for (Pais p : new PaisDAOJPA().getAll(Pais.class)) {
            comboBoxPais.addItem(p);
        }
    }

    private void carregaComboEstado() {
        comboBoxEstado.removeAllItems();
        Object falso = "Selecione";
        comboBoxEstado.addItem(falso);
        Pais p = buscaPais();
        if (p != null) {
            comboBoxEstado.setEnabled(true);
            for (Estado e : new EstadoDAOJPA().getEstadoPais(p)) {
                comboBoxEstado.addItem(e);
            }
        } else {
            comboBoxEstado.setEnabled(false);
        }
    }

    private void carregaComboCidade() {
        comboBoxCidade.removeAllItems();
        Object falso = "Selecione";
        comboBoxCidade.addItem(falso);
        Estado e = buscaEstado();
        if (e != null) {
            for (Cidade c : new CidadeDAOJPA().getCidadeEstado(e)) {
                comboBoxCidade.addItem(c);
            }
            comboBoxCidade.setEnabled(true);
        } else {
            comboBoxCidade.setEnabled(false);
        }
    }

//    PaisDAOJPA dao = new PaisDAOJPA();
//    List<Pais> lista = dao.getAll(Pais.class);
    public DialogCadastroEndereco(java.awt.Frame parent, boolean modal, Endereco o, String textoButton) {
        super(parent, modal);
        initComponents();

        AutoCompleteDecorator.decorate(this.comboBoxPais);
        AutoCompleteDecorator.decorate(this.comboBoxEstado);
        AutoCompleteDecorator.decorate(this.comboBoxCidade);
        Singleton.getInstance().passaCamposComEnter(panel1);
        SelectAllTheThings.addListeners(panel1);
        buttonSalvar.setText(textoButton);
        if (o != null) {
            System.out.println("altera");
            this.endereco = o;
            refresh();
        }
        carregaComboPais();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        txtLogradouro = new javax.swing.JTextField();
        labelLogradouro = new javax.swing.JLabel();
        labelNumero = new javax.swing.JLabel();
        labelCompl = new javax.swing.JLabel();
        txtCompl = new javax.swing.JTextField();
        labelBairro = new javax.swing.JLabel();
        txtBairro = new javax.swing.JTextField();
        labelCidade = new javax.swing.JLabel();
        comboBoxCidade = new javax.swing.JComboBox();
        txtNumero = new javax.swing.JFormattedTextField();
        labelEstado = new javax.swing.JLabel();
        comboBoxEstado = new javax.swing.JComboBox();
        labelPais = new javax.swing.JLabel();
        comboBoxPais = new javax.swing.JComboBox();
        labelCamposObrig = new javax.swing.JLabel();
        labelPais1 = new javax.swing.JLabel();
        txtCep = new javax.swing.JFormattedTextField();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        txtLogradouro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        labelLogradouro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelLogradouro.setText("LOGRADOURO *");

        labelNumero.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelNumero.setText("NÚMERO *");

        labelCompl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelCompl.setText("COMPL.");

        txtCompl.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        labelBairro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelBairro.setText("BAIRRO");

        txtBairro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        labelCidade.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelCidade.setText("Cidade*");

        comboBoxCidade.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxCidade.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxCidade.setEnabled(false);
        comboBoxCidade.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxCidadeItemStateChanged(evt);
            }
        });

        txtNumero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txtNumero.setFocusLostBehavior(javax.swing.JFormattedTextField.COMMIT);
        txtNumero.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        labelEstado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelEstado.setText("Estado*");

        comboBoxEstado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxEstado.setEnabled(false);
        comboBoxEstado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxEstadoItemStateChanged(evt);
            }
        });

        labelPais.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelPais.setText("Pais*");

        comboBoxPais.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        comboBoxPais.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione:" }));
        comboBoxPais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxPaisItemStateChanged(evt);
            }
        });

        labelCamposObrig.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelCamposObrig.setText("* Campos obrigatorios");

        labelPais1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        labelPais1.setText("Cep*");

        try {
            txtCep.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelLogradouro)
                    .addComponent(labelNumero)
                    .addComponent(labelPais)
                    .addComponent(labelEstado)
                    .addComponent(labelCidade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtLogradouro)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(labelCompl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCompl, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelBairro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBairro))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBoxPais, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxCidade, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelPais1)
                        .addGap(9, 9, 9)
                        .addComponent(txtCep, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(labelCamposObrig)))
                .addContainerGap())
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelLogradouro)
                    .addComponent(txtLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCompl, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelCompl)
                            .addComponent(labelBairro)
                            .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNumero))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxPais, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPais))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCidade)
                    .addComponent(labelPais1)
                    .addComponent(txtCep, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCamposObrig))
                .addGap(274, 274, 274))
        );

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Inserir");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxCidadeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxCidadeItemStateChanged

    }//GEN-LAST:event_comboBoxCidadeItemStateChanged

    private void comboBoxEstadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxEstadoItemStateChanged
        carregaComboCidade();
    }//GEN-LAST:event_comboBoxEstadoItemStateChanged

    private void comboBoxPaisItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxPaisItemStateChanged
        carregaComboEstado();
    }//GEN-LAST:event_comboBoxPaisItemStateChanged

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        inserir();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonFechar;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JComboBox comboBoxCidade;
    private javax.swing.JComboBox comboBoxEstado;
    private javax.swing.JComboBox comboBoxPais;
    private javax.swing.JLabel labelBairro;
    private javax.swing.JLabel labelCamposObrig;
    private javax.swing.JLabel labelCidade;
    private javax.swing.JLabel labelCompl;
    private javax.swing.JLabel labelEstado;
    private javax.swing.JLabel labelLogradouro;
    private javax.swing.JLabel labelNumero;
    private javax.swing.JLabel labelPais;
    private javax.swing.JLabel labelPais1;
    private javax.swing.JPanel panel1;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JFormattedTextField txtCep;
    private javax.swing.JTextField txtCompl;
    private javax.swing.JTextField txtLogradouro;
    private javax.swing.JFormattedTextField txtNumero;
    // End of variables declaration//GEN-END:variables
}
