package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.OrcamentoDAOJPA;
import br.com.autonomusmecanica.dao.OrcamentoItemDAOJPA;
import br.com.autonomusmecanica.dao.VeiculoDAOJPA;
import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Orcamento;
import br.com.autonomusmecanica.entidades.OrcamentoItem;
import br.com.autonomusmecanica.entidades.Veiculo;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.relatorios.GeraRelatorio;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelOrcamentoItem;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

public class DialogCadastroOrcamento extends javax.swing.JDialog {

    private Orcamento orcamento;
    private Veiculo veiculo;
    private Cliente cliente;
    private TableModelOrcamentoItem model;
    private final TableCellRenderer renderer = new StripedTable();
    private int colAlterar = 5;
    private int colExcluir = 6;
    private int colAlterarParc = 8;
    private int colExcluirParc = 9;

    public void gerarRel() {
        if (orcamento != null) {
            OrcamentoItemDAOJPA dao = new OrcamentoItemDAOJPA();
            List<OrcamentoItem> lista = dao.getItemOrcamento(orcamento);
            GeraRelatorio g = new GeraRelatorio();
            g.geraRelatorio(lista, "Relatório de Orçamento", "reportOrcamentoCliente");
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um orçamento", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void adicionarItens() {
        if (orcamento == null) {
            salvar(true);
        }
        if (orcamento != null && orcamento.getStatus().equals('a')) {
            DialogCadastraOrcamentoItem d = new DialogCadastraOrcamentoItem(null, true, null, orcamento);
            d.setVisible(true);
            atualizaTabela();
            salvar(false);
        } else {
            JOptionPane.showMessageDialog(this, "Não foi possivel incluir. Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void atualizaTabela() {
        OrcamentoItemDAOJPA dao = new OrcamentoItemDAOJPA();
        List<OrcamentoItem> lista = dao.getItemOrcamento(orcamento);
        model = new TableModelOrcamentoItem(lista);
        table.setModel(model);
        table.getColumnModel().getColumn(0).setMinWidth(0);
        table.getColumnModel().getColumn(0).setMaxWidth(0);
        table.getColumnModel().getColumn(0).setPreferredWidth(0);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.getColumnModel().getColumn(2).setPreferredWidth(250);
        table.getColumnModel().getColumn(3).setPreferredWidth(20);
        table.getColumnModel().getColumn(4).setPreferredWidth(20);
        table.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(table, colAlterar, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(table, colExcluir, Singleton.getInstance().iconeJTable("Excluir"));
        table.setRowHeight(20);
        somaTotal();
    }

    private OrcamentoItem getTableItem() {
        OrcamentoItem obj = null;
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            obj = (OrcamentoItem) table.getValueAt(linha, -1);
        } else {
            JOptionPane.showMessageDialog(this, "Selecione um linha.", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }
        return obj;
    }

    private void excluirItens() {
        OrcamentoItem e = getTableItem();
        if (e != null) {
            if (e.getOrcamento().getStatus().equals('a')) {
                OrcamentoItemDAOJPA dao = new OrcamentoItemDAOJPA();
                int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Deseja excluir o item \n" + e.getDescricao() + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                //0=sim e 1=nao 
                if (showConfirmDialog == 0) {
                    dao.remove(OrcamentoItem.class, e.getId());
                    atualizaTabela();
                    salvar(false);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Não foi possivel excluir. Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private void alterarItens() {
        OrcamentoItem obj = getTableItem();
        if (obj != null) {
            if (obj.getOrcamento().getStatus().equals('a')) {
                DialogCadastraOrcamentoItem d = new DialogCadastraOrcamentoItem(null, true, obj, obj.getOrcamento());
                d.setVisible(true);
                atualizaTabela();
                salvar(false);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Não é possível alterar. Orçamento já fechado", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
        }

    }

    private void abreBuscaVeiculo() {
        DialogTabelaVeiculo d = new DialogTabelaVeiculo(null, true, null);
        d.setVisible(true);
        this.veiculo = d.getVeiculo();
        refreshVeiculo();
    }

    private void buscaVeiculo() {
        VeiculoDAOJPA dao = new VeiculoDAOJPA();
        List<Veiculo> veiculoPlaca = dao.getVeiculoPlaca(txtPlaca.getText());
        if (veiculoPlaca != null && !veiculoPlaca.isEmpty()) {
            this.veiculo = veiculoPlaca.get(0);
            if (veiculo.getCliente() != null) {
                refreshVeiculo();
            } else {
                JOptionPane.showMessageDialog(this, "Veículo sem cliente! Vincule o cliente ao veículo primeiro", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Placa não encontrada", "Aviso!!!", JOptionPane.WARNING_MESSAGE);
            limpaTela();
        }
    }

    private void refreshVeiculo() {
        if (this.veiculo != null) {
            txtPlaca.setText(veiculo.getPlaca());
            lblModelo.setText(veiculo.getModelo().getDescricao() + " - " + veiculo.getModelo().getFabricante().getNome());
            lblAno.setText(veiculo.getAno() + "");
            if (veiculo.getCor().getCodigo() != null && !veiculo.getCor().getCodigo().isEmpty()) {
                lblCor.setBackground(Color.decode(veiculo.getCor().getCodigo()));
            } else {
                lblCor.setBackground(null);
            }
            if (veiculo.getCliente() != null) {
                lblNome.setText(veiculo.getCliente().getPessoa().getNome());
                lblCpfCnpj.setText(veiculo.getCliente().getPessoa().getCpfCnpj());
            }
            dChooserValidade.requestFocus();
        }
    }

    private void salvar(Boolean msg) {
        if (veiculo == null) {
            JOptionPane.showMessageDialog(this, "Preencha o veículo");
            txtPlaca.requestFocus();
        } else if (dChooser.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Preencha a data");
            dChooser.requestFocus();
        } else if (dChooserValidade.getDate() == null) {
            JOptionPane.showMessageDialog(this, "Preencha a validade");
            dChooserValidade.requestFocus();
        } else {
            Orcamento s = new Orcamento();
            if (this.orcamento == null) {
                s.setStatus('a');
                //System.out.println("inserir");
            } else {
                //System.out.println("atualizar");
                s = this.orcamento;
            }
            s.setVeiculo(veiculo);
            s.setCliente(veiculo.getCliente());
            s.setDataOrcamento(dChooser.getDate());
            s.setValidade(dChooserValidade.getDate());
            s.setValorTotal(Singleton.getInstance().converteStringParaDouble(lblTotal.getText()));
            s.setValorMO(Singleton.getInstance().converteStringParaDouble(txtMO.getText()));
            s.setDescricao(txtDescricao.getText());
            OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
            this.orcamento = dao.save(s);
            refreshForm();
            if (msg) {
                JOptionPane.showMessageDialog(this, "Registro salvo");
            }
        }
    }

    private void refreshForm() {
        limpaTela();
        refreshVeiculo();
        if (orcamento.getDataOrcamento() != null) {
            dChooser.setDate(orcamento.getDataOrcamento());
        }
        if (orcamento.getValidade() != null) {
            dChooserValidade.setDate(orcamento.getValidade());
        }
        lblCodigo.setText(orcamento.getId() + "");
        lblStatus.setText(orcamento.getStatusString());
        txtDescricao.setText(orcamento.getDescricao());
        txtMO.setText(Singleton.getInstance().converteDoubleParaString(orcamento.getValorMO(), 2));
        lblTotal.setText(Singleton.getInstance().converteDoubleParaString(orcamento.getValorTotal(), 2));
        atualizaTabela();
    }

    private void limpaTela() {
        lblModelo.setText("");
        lblAno.setText("");
        lblCor.setText("");
        lblCpfCnpj.setText("");
        lblNome.setText("");
        lblTotalPecas.setText("0,00");
        lblTotal.setText("0,00");
        txtMO.setText("0,00");
        txtDescricao.setText("");
    }

    private void novo() {
        this.limpaTela();
        dChooser.setDate(new Date());
        dChooserValidade.setDate(Singleton.getInstance().somaDia(new Date(),7));
        this.orcamento = null;
        this.cliente = null;
    }

    private void buscaPrimeiroRegistro() {
        OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
        this.orcamento = (Orcamento) dao.primeiroRegistro(Orcamento.class);
        refreshForm();
    }

    private void buscaUltimoRegistro() {
        OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
        this.orcamento = (Orcamento) dao.ultimoRegistro(Orcamento.class, "id");
        refreshForm();
    }

    private void proximo() {
        if (this.orcamento == null) {
            buscaUltimoRegistro();
        } else {
            OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
            this.orcamento = (Orcamento) dao.getProximo(Orcamento.class, "id", this.orcamento.getId());
            if (this.orcamento != null) {
                refreshForm();
            }
        }
    }

    private void anterior() {
        if (this.orcamento == null) {
            buscaPrimeiroRegistro();
        } else {
            OrcamentoDAOJPA dao = new OrcamentoDAOJPA();
            this.orcamento = (Orcamento) dao.getAnterior(Orcamento.class, "id", this.orcamento.getId());
            if (this.orcamento != null) {
                refreshForm();
            }
        }
    }

    private void somaTotal() {
        Double mo = Singleton.getInstance().converteStringParaDouble(txtMO.getText());
        Double itens = 0d;
        for (int x = 0; x < table.getRowCount(); x++) {
            OrcamentoItem item = (OrcamentoItem) table.getValueAt(x, -1);
            itens += item.getValorTotal();
        }
        lblTotalPecas.setText(Singleton.getInstance().converteDoubleParaString(itens, 2));
        Double tot = (mo + itens);
        lblTotal.setText(Singleton.getInstance().converteDoubleParaString(tot, 2));
    }

    public DialogCadastroOrcamento(java.awt.Frame parent, boolean modal, Orcamento s, int tabPane) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        if (s == null) {
            novo();
        } else {
//            System.out.println("altera");
            this.orcamento = s;
            this.veiculo = s.getVeiculo();
            refreshForm();
        }
        txtPlaca.requestFocus();
        tabbedPane.setSelectedIndex(tabPane);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnBusca = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtPlaca = new javax.swing.JFormattedTextField();
        lblModelo = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblCpfCnpj = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblAno = new javax.swing.JLabel();
        lblCor = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dChooser = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        dChooserValidade = new com.toedter.calendar.JDateChooser();
        lblTotal = new javax.swing.JLabel();
        btnRelatorio = new javax.swing.JButton();
        tabbedPane = new javax.swing.JTabbedPane();
        panel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        btnAddItem = new javax.swing.JButton();
        txtMO = new javax.swing.JFormattedTextField();
        lblTotalPecas = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Orçamentos"));

        lblCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeRecibos64x64.png"))); // NOI18N

        lblStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setText("Status:");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(291, 291, 291))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonPrev)
                    .addComponent(buttonNext)
                    .addComponent(buttonNext1)
                    .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1, lblCodigo});

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Apply.png"))); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Close.png"))); // NOI18N
        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/New document.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));

        btnBusca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/View.png"))); // NOI18N
        btnBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaActionPerformed(evt);
            }
        });

        jLabel5.setText("Placa:");

        try {
            txtPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-#A##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtPlaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlacaKeyPressed(evt);
            }
        });

        lblModelo.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel8.setText("Veículo:");

        jLabel9.setText("Cor:");

        lblCpfCnpj.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Ano:");

        jLabel11.setText("Nome:");

        lblNome.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setText("Cpf/Cnpj:");

        lblAno.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblCor.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lblCor.setOpaque(true);

        jLabel2.setText("Data:");

        jLabel3.setText("Validade:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel5)
                                .addGap(2, 2, 2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel11)))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnBusca))
                    .addComponent(dChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblModelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(dChooserValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblAno, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblCpfCnpj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(lblNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(288, 288, 288))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel5)
                    .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(lblModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(dChooser, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(dChooserValidade, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblAno, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblCor, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblCpfCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(12, 12, 12))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblAno, lblCor, lblCpfCnpj, lblModelo, lblNome, txtPlaca});

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("0,00");
        lblTotal.setToolTipText("");
        lblTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        btnRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Report.png"))); // NOI18N
        btnRelatorio.setText("Relatório");
        btnRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRelatorioActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Objeto", "Qtde", "Descricao", "Valor Unitario", "Valor Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        btnAddItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        btnAddItem.setToolTipText("Adicionar");
        btnAddItem.setBorder(null);
        btnAddItem.setBorderPainted(false);
        btnAddItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddItemActionPerformed(evt);
            }
        });
        btnAddItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnAddItemKeyPressed(evt);
            }
        });

        txtMO.setBorder(javax.swing.BorderFactory.createTitledBorder("M.O"));
        txtMO.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txtMO.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtMO.setText("0,00");
        txtMO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMO.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMOFocusLost(evt);
            }
        });

        lblTotalPecas.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTotalPecas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalPecas.setText("0,00");
        lblTotalPecas.setToolTipText("");
        lblTotalPecas.setBorder(javax.swing.BorderFactory.createTitledBorder("Total Peças"));

        txtDescricao.setColumns(20);
        txtDescricao.setRows(3);
        txtDescricao.setBorder(javax.swing.BorderFactory.createTitledBorder("Descrição:"));
        jScrollPane3.setViewportView(txtDescricao);

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblTotalPecas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMO, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnAddItem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel2Layout.createSequentialGroup()
                .addComponent(btnAddItem, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addComponent(lblTotalPecas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tabbedPane.addTab("Materiais", panel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnRelatorio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tabbedPane))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabbedPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalvar)
                        .addComponent(btnFechar)
                        .addComponent(btnNovo)
                        .addComponent(btnRelatorio))
                    .addComponent(lblTotal))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void btnAddItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddItemActionPerformed
        adicionarItens();
    }//GEN-LAST:event_btnAddItemActionPerformed

    private void btnBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaActionPerformed
        abreBuscaVeiculo();
    }//GEN-LAST:event_btnBuscaActionPerformed

    private void txtPlacaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            buscaVeiculo();
        }
    }//GEN-LAST:event_txtPlacaKeyPressed

    private void btnAddItemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnAddItemKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnAddItemActionPerformed(null);
        }
    }//GEN-LAST:event_btnAddItemKeyPressed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        novo();
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        salvar(true);
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void txtMOFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMOFocusLost
        somaTotal();
    }//GEN-LAST:event_txtMOFocusLost

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colAlterar) {
            alterarItens();
        } else if (table.getSelectedColumn() == colExcluir) {
            excluirItens();
        }
    }//GEN-LAST:event_tableMouseClicked

    private void btnRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatorioActionPerformed
        gerarRel();
    }//GEN-LAST:event_btnRelatorioActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddItem;
    private javax.swing.JButton btnBusca;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnRelatorio;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private com.toedter.calendar.JDateChooser dChooser;
    private com.toedter.calendar.JDateChooser dChooserValidade;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblAno;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblCor;
    private javax.swing.JLabel lblCpfCnpj;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalPecas;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTable table;
    private javax.swing.JTextArea txtDescricao;
    private javax.swing.JFormattedTextField txtMO;
    private javax.swing.JFormattedTextField txtPlaca;
    // End of variables declaration//GEN-END:variables
}
