package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.EmailDAOJPA;
import br.com.autonomusmecanica.dao.EnderecoDAOJPA;
import br.com.autonomusmecanica.dao.PessoaDAOJPA;
import br.com.autonomusmecanica.dao.TelefoneDAOJPA;
import br.com.autonomusmecanica.dao.UsuarioDAOJPA;
import br.com.autonomusmecanica.entidades.Email;
import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Telefone;
import br.com.autonomusmecanica.entidades.Usuario;
import br.com.autonomusmecanica.funcoes.SelectAllTheThings;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelEmail;
import br.com.autonomusmecanica.tableModel.TableModelEndereco;
import br.com.autonomusmecanica.tableModel.TableModelPessoaImagem;
import br.com.autonomusmecanica.tableModel.TableModelTelefone;
import java.awt.Color;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author roger
 */
public final class DialogCadastroUsuario extends javax.swing.JDialog {

    private Pessoa pessoa;
    private Usuario usuario;
    private TableModelEmail modelEmail;
    private TableModelTelefone modelTelefone;
    private TableModelEndereco modelEndereco;
    private TableModelPessoaImagem modelPessoaImagem;
    private JButton b = new JButton();

    private final TableCellRenderer renderer = new StripedTable();

    private boolean pessoaIsNull() {
        Boolean valida;
        if (this.pessoa == null) {
            valida = true;
        } else {
            valida = false;
        }
        return valida;
    }

    private boolean usuarioIsNull() {
        boolean valida;
        if (this.usuario == null) {
            valida = true;
        } else {
            valida = false;
        }
        return valida;
    }

    private void atualizaTabelaEmail() {
        EmailDAOJPA dao = new EmailDAOJPA();
        List<Email> lista = dao.getPessoaEmail(pessoa);
        modelEmail = new TableModelEmail(lista);
        tableEmail.setModel(modelEmail);
        //model.ordenarPorEmail();
        tableEmail.getColumnModel().getColumn(0).setPreferredWidth(400);
        tableEmail.getColumnModel().getColumn(1).setPreferredWidth(30);
        tableEmail.getColumnModel().getColumn(2).setPreferredWidth(30);
        tableEmail.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEmail, 1, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEmail, 2, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void atualizaTabelaTelefone() {
        TelefoneDAOJPA dao = new TelefoneDAOJPA();
        List<Telefone> lista = dao.getPessoaTelefone(pessoa);
        modelTelefone = new TableModelTelefone(lista);
        tableTelefone.setModel(modelTelefone);
        tableTelefone.getColumnModel().getColumn(0).setPreferredWidth(80);
        tableTelefone.getColumnModel().getColumn(1).setPreferredWidth(270);
        tableTelefone.getColumnModel().getColumn(2).setPreferredWidth(30);
        tableTelefone.getColumnModel().getColumn(3).setPreferredWidth(30);
        tableTelefone.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableTelefone, 2, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableTelefone, 3, Singleton.getInstance().iconeJTable("Excluir"));
    }

    public void atualizaTabelaEndereco() {
        EnderecoDAOJPA dao = new EnderecoDAOJPA();
        List<Endereco> lista = dao.getPessoaEndereco(pessoa);
        modelEndereco = new TableModelEndereco(lista);
        tableEndereco.setModel(modelEndereco);
        tableEndereco.getColumnModel().getColumn(0).setPreferredWidth(200);
        tableEndereco.getColumnModel().getColumn(1).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(2).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(3).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(3).setPreferredWidth(0);
        tableEndereco.getColumnModel().getColumn(4).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(5).setPreferredWidth(80);
        tableEndereco.getColumnModel().getColumn(6).setMinWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setMaxWidth(0);
        tableEndereco.getColumnModel().getColumn(6).setPreferredWidth(0);
        tableEndereco.setDefaultRenderer(Object.class, renderer);
        new ButtonColumn(tableEndereco, 7, Singleton.getInstance().iconeJTable("Alterar"));
        new ButtonColumn(tableEndereco, 8, Singleton.getInstance().iconeJTable("Excluir"));
    }

    private void salvar() {
        if (textFieldLogin.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o login");
            textFieldLogin.requestFocus();
            return;
        }
        if (textFieldNome.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o nome:");
            textFieldNome.requestFocus();
            return;
        }
        String mensagem;
        String senha = new String(textFieldSenha.getPassword());
        String senha2 = new String(textFieldRepeteSenha.getPassword());
        if (!senha.equals(senha2)) {
            JOptionPane.showMessageDialog(this, "Senha diferentes", "Alerta", JOptionPane.WARNING_MESSAGE);
            textFieldRepeteSenha.requestFocus();
        } else {
            Pessoa p = new Pessoa();
            Usuario u = new Usuario();
            if (!usuarioIsNull()) {
                mensagem = "Registro atualizado com sucesso";
                p = this.pessoa;
                u = this.usuario;
                if (!senha.trim().equals("")) {
                    u.setSenhaSemCriptografia(senha);
                }
            } else {
                mensagem = "Registro inserido com sucesso";
                if (senha.trim().equals("")) {
                    JOptionPane.showMessageDialog(this, "Preencha a senha");
                    textFieldSenha.requestFocus();
                    return;
                } else {
                    u.setSenhaSemCriptografia(senha);
                }
            }
            p.setNome(textFieldNome.getText());
            p.setTipo('f');
            p.setApelidoNomeFantasia(textFieldApelido.getText());
            u.setLogin(textFieldLogin.getText());
            if (radioButtonAtivo.isSelected()) {
                p.setStatus('a');
            } else {
                p.setStatus('i');
            }
            if (radioButtonAdministrador.isSelected()) {
                u.setAdministrador('a');
            } else {
                u.setAdministrador('u');
            }
            UsuarioDAOJPA dao = new UsuarioDAOJPA();
            List usuarios;
            if (usuario == null) {
                usuarios = dao.getUsuariosExiste(textFieldLogin.getText(), 0);
            } else {
                usuarios = dao.getUsuariosExiste(textFieldLogin.getText(), usuario.getIdUsuario());
            }
            if (usuarios.isEmpty()) {
                PessoaDAOJPA dao2 = new PessoaDAOJPA();
                p = dao2.save(p);
                u.setPessoa(p);
                u= dao.save(u);
                this.pessoa = p;
                this.usuario = u;
                JOptionPane.showMessageDialog(this, mensagem);
                //dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Este usuário já existe", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
            refresh();
        }
    }

    private void refresh() {
        if (!usuarioIsNull()) {
            labelCodigo.setText("" + this.usuario.getIdUsuario());
            textFieldLogin.setText(this.usuario.getLogin());
            textFieldNome.setText(this.usuario.getPessoa().getNome());
            textFieldApelido.setText(this.usuario.getPessoa().getApelidoNomeFantasia());
            if (this.usuario.getAdministrador().equals('a')) {
                radioButtonAdministrador.setSelected(true);
            } else {
                radioButtonUsuario.setSelected(true);
            }
            if (this.usuario.getPessoa().getStatus().equals('a')) {
                radioButtonAtivo.setSelected(true);
            } else {
                radioButtonInativo.setSelected(true);
            }
            colorText();
            atualizaTabelaTelefone();
            atualizaTabelaEmail();
            atualizaTabelaEndereco();
        }
    }

    private void limpaTela() {
        labelCodigo.setText("");
        textFieldLogin.setText("");
        textFieldSenha.setText("");
        textFieldRepeteSenha.setText("");
        radioButtonAdministrador.setSelected(true);
        radioButtonAtivo.setSelected(true);
        textFieldNome.setText("");
        textFieldApelido.setText("");
        new TableModelEmail().limpaLista();
        new TableModelEmail().adiciona(new Email());
        new TableModelTelefone().limpaLista();
        new TableModelTelefone().adiciona(new Telefone());
        new TableModelEndereco().limpaLista();
        new TableModelEndereco().adiciona(new Endereco());
    }

    private void novo() {
        this.limpaTela();
        this.usuario = null;
        this.pessoa = null;
        atualizaTabelaEndereco();
        atualizaTabelaEmail();
        atualizaTabelaTelefone();
    }

    public void colorText() {
        if (radioButtonAtivo.isSelected()) {
            textFieldLogin.setForeground(Color.black);
            textFieldApelido.setForeground(Color.black);
            textFieldNome.setForeground(Color.black);

        } else {
            textFieldLogin.setForeground(Color.red);
            textFieldApelido.setForeground(Color.red);
            textFieldNome.setForeground(Color.red);
        }
    }

    private void adicionaEmail() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail:", "Inserir");
            d.setTitle("Cadastrar Email");
            d.setVisible(true);
            EmailDAOJPA dao = new EmailDAOJPA();
            Email e = new Email();
            e.setPessoa((Pessoa) d.resultado);
            e.setEmail(d.descricao);
            if (e.getEmail() != null) {
                dao.save(e);
                atualizaTabelaEmail();
            }
        }
    }

    private void alteraEmail() {
        if (!pessoaIsNull()) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    DialogCadastroEmail d = new DialogCadastroEmail(null, true, pessoa, "E-mail", "Salvar");
                    d.setDescricao(email.getEmail());
                    d.setTitle("Cadastrar Email");
                    d.setVisible(true);
                    EmailDAOJPA dao = new EmailDAOJPA();
                    email.setPessoa((Pessoa) d.resultado);
                    email.setEmail(d.descricao);
                    dao.save(email);
                    atualizaTabelaEmail();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para alterar");
                    }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEmail() {
        if (!pessoaIsNull()) {
            int linha = tableEmail.getSelectedRow();
            if (linha >= 0) {
                Email email = (Email) tableEmail.getValueAt(linha, -1);
                if (email.getIdEmail() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o e-mail " + tableEmail.getValueAt(linha, 1) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EmailDAOJPA dao = new EmailDAOJPA();
                        dao.remove(Email.class, email.getIdEmail());
                        atualizaTabelaEmail();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem e-mail para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void adicionaTelefone() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            Telefone telefone = new Telefone();
            telefone.setPessoa(pessoa);
            DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Inserir");
            d.setTitle("Cadastrar Telefone");
            d.setVisible(true);
            TelefoneDAOJPA dao = new TelefoneDAOJPA();
            telefone.setPessoa(pessoa);
            telefone.setNumero(d.t.getNumero());
            telefone.setObs(d.t.getObs());
            if (d.t.getNumero() != null) {
                dao.save(telefone);
                atualizaTabelaTelefone();
            }
        }
    }

    private void alteraTelefone() {
        if (!pessoaIsNull()) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    DialogCadastroTelefone d = new DialogCadastroTelefone(null, true, telefone, "Salvar");
                    d.setTitle("Cadastrar Telefone");
                    d.setVisible(true);
                    TelefoneDAOJPA dao = new TelefoneDAOJPA();
                    telefone.setPessoa(telefone.getPessoa());
                    telefone.setNumero(d.t.getNumero());
                    telefone.setObs(d.t.getObs());
                    dao.save(telefone);
                    atualizaTabelaTelefone();
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiTelefone() {
        if (!pessoaIsNull()) {
            int linha = tableTelefone.getSelectedRow();
            if (linha >= 0) {
                Telefone telefone = (Telefone) tableTelefone.getValueAt(linha, -1);
                if (telefone.getIdTelefone() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o telefone " + tableTelefone.getValueAt(linha, 1) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        TelefoneDAOJPA dao = new TelefoneDAOJPA();
                        dao.remove(Telefone.class, telefone.getIdTelefone());
                        atualizaTabelaTelefone();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem telefone para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void adicionaEndereco() {
        if (pessoaIsNull()) {
            salvar();
        } else {
            Endereco e = new Endereco();
            e.setPessoa(this.pessoa);
            DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Inserir");
            d.setTitle("Cadastrar Endereco");
            d.setVisible(true);
            EnderecoDAOJPA dao = new EnderecoDAOJPA();
            e.setPessoa(pessoa);
            e.setLogradouro(d.endereco.getLogradouro());
            e.setNumero(d.endereco.getNumero());
            e.setComplemento(d.endereco.getComplemento());
            e.setBairro(d.endereco.getBairro());
            e.setCep(d.endereco.getCep());
            e.setCidade(d.endereco.getCidade());
            if (e.getLogradouro() != null && e.getCidade() != null) {
                dao.save(e);
            }
        }
        atualizaTabelaEndereco();
    }

    private void alteraEndereco() {
        if (!pessoaIsNull()) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco e = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (e.getIdEndereco() != null) {
                    DialogCadastroEndereco d = new DialogCadastroEndereco(null, true, e, "Salvar");
                    d.setTitle("Alterar Endereco");
                    d.setVisible(true);
                    EnderecoDAOJPA dao = new EnderecoDAOJPA();
                    e.setPessoa(pessoa);
                    e.setPessoa(pessoa);
                    e.setLogradouro(d.endereco.getLogradouro());
                    e.setNumero(d.endereco.getNumero());
                    e.setComplemento(d.endereco.getComplemento());
                    e.setBairro(d.endereco.getBairro());
                    e.setCep(d.endereco.getCep());
                    e.setCidade(d.endereco.getCidade());
                    dao.save(e);
                    atualizaTabelaEndereco();

                } else {
                    JOptionPane.showMessageDialog(this, "Sem endereco para alterar");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }

    private void excluiEndereco() {
        if (!pessoaIsNull()) {
            int linha = tableEndereco.getSelectedRow();
            if (linha >= 0) {
                Endereco endereco = (Endereco) tableEndereco.getValueAt(linha, -1);
                if (endereco.getIdEndereco() != null) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir o endereço " + tableEndereco.getValueAt(linha, 1) + "?", "Excluir", JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == 0) {
                        EnderecoDAOJPA dao = new EnderecoDAOJPA();
                        dao.remove(Endereco.class, endereco.getIdEndereco());
//                        JOptionPane.showMessageDialog(this,"Registro removido");
                        atualizaTabelaEndereco();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Sem Endereco para excluir");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Selecione uma linha");
            }
        }
    }


    private void buscaPrimeiroRegistro() {
        UsuarioDAOJPA dao = new UsuarioDAOJPA();
        this.usuario = (Usuario) dao.primeiroRegistro(Usuario.class);
        this.pessoa = usuario.getPessoa();
        refresh();
    }

    private void buscaUltimoRegistro() {
        UsuarioDAOJPA dao = new UsuarioDAOJPA();
        this.usuario = (Usuario) dao.ultimoRegistro(Usuario.class, "idUsuario");
        this.pessoa = usuario.getPessoa();
        refresh();
    }

    private void proximo() {
        if (usuario == null) {
            buscaUltimoRegistro();
        } else {
            UsuarioDAOJPA dao = new UsuarioDAOJPA();
            this.usuario = (Usuario) dao.getProximo(Usuario.class, "idUsuario", usuario.getIdUsuario());
            this.pessoa = usuario.getPessoa();
            if (usuario != null) {
                refresh();
            }
        }
    }

    private void anterior() {
        if (usuario == null) {
            buscaPrimeiroRegistro();
        } else {
            UsuarioDAOJPA dao = new UsuarioDAOJPA();
            this.usuario = (Usuario) dao.getAnterior(Usuario.class, "idUsuario", usuario.getIdUsuario());
            this.pessoa = usuario.getPessoa();
            if (usuario != null) {
                refresh();
            }
        }
    }


    public DialogCadastroUsuario(java.awt.Frame parent, boolean modal, Usuario user) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panel2);
        SelectAllTheThings.addListeners(panel2);
        Singleton.getInstance().passaCamposComEnter(tabbedPane);
        if (user == null) {
            novo();
        } else {
            this.usuario = user;
            this.pessoa = user.getPessoa();
            refresh();
        }
        textFieldLogin.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupStatus = new javax.swing.ButtonGroup();
        buttonGroupTipo = new javax.swing.ButtonGroup();
        buttonSalvar = new javax.swing.JButton();
        buttonFechar = new javax.swing.JButton();
        panel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        textFieldLogin = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textFieldSenha = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        textFieldRepeteSenha = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        textFieldNome = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        textFieldApelido = new javax.swing.JTextField();
        radioButtonAdministrador = new javax.swing.JRadioButton();
        radioButtonUsuario = new javax.swing.JRadioButton();
        radioButtonAtivo = new javax.swing.JRadioButton();
        radioButtonInativo = new javax.swing.JRadioButton();
        panel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelCodigo = new javax.swing.JLabel();
        buttonPrev = new javax.swing.JButton();
        buttonNext = new javax.swing.JButton();
        buttonPrev1 = new javax.swing.JButton();
        buttonNext1 = new javax.swing.JButton();
        buttonNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();
        panelEnd = new javax.swing.JPanel();
        ScrollPaneEnd = new javax.swing.JScrollPane();
        tableEndereco = new javax.swing.JTable();
        buttonAddEnd = new javax.swing.JButton();
        paneFone = new javax.swing.JPanel();
        ScrollPaneFone = new javax.swing.JScrollPane();
        tableTelefone = new javax.swing.JTable();
        buttonAddFone = new javax.swing.JButton();
        paneEmail = new javax.swing.JPanel();
        ScrollPaneEmail = new javax.swing.JScrollPane();
        tableEmail = new javax.swing.JTable();
        buttonAddEmail = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        buttonSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeGravar32x32.png"))); // NOI18N
        buttonSalvar.setText("Salvar");
        buttonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvarActionPerformed(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.setText("Fechar");
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        panel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados Login"));

        jLabel4.setText("Login:");

        jLabel2.setText("Senha:");

        textFieldSenha.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        jLabel3.setText("Repetir senha:");

        textFieldRepeteSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldRepeteSenhaActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome Completo :");

        jLabel5.setText("Apelido:");

        buttonGroupTipo.add(radioButtonAdministrador);
        radioButtonAdministrador.setSelected(true);
        radioButtonAdministrador.setText("Administrador");

        buttonGroupTipo.add(radioButtonUsuario);
        radioButtonUsuario.setText("Usuário");

        buttonGroupStatus.add(radioButtonAtivo);
        radioButtonAtivo.setSelected(true);
        radioButtonAtivo.setText("Ativo");

        buttonGroupStatus.add(radioButtonInativo);
        radioButtonInativo.setText("Inativo");

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldApelido, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                    .addComponent(textFieldNome)
                    .addComponent(textFieldRepeteSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldLogin, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE))
                .addGap(67, 67, 67))
            .addGroup(panel2Layout.createSequentialGroup()
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioButtonAdministrador)
                    .addComponent(radioButtonAtivo, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioButtonInativo, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioButtonUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(451, Short.MAX_VALUE))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(textFieldLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textFieldRepeteSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(textFieldApelido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButtonAdministrador)
                    .addComponent(radioButtonUsuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioButtonAtivo)
                    .addComponent(radioButtonInativo))
                .addContainerGap())
        );

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario"));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeClientes64x64.png"))); // NOI18N

        labelCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));

        buttonPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_previous24x24.png"))); // NOI18N
        buttonPrev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrevActionPerformed(evt);
            }
        });

        buttonNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_next24x24.png"))); // NOI18N
        buttonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNextActionPerformed(evt);
            }
        });

        buttonPrev1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_first24x24.png"))); // NOI18N
        buttonPrev1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrev1ActionPerformed(evt);
            }
        });

        buttonNext1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/go_last24x24.png"))); // NOI18N
        buttonNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                .addGap(198, 198, 198)
                .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonPrev, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(buttonNext1)
                    .addComponent(buttonNext)
                    .addComponent(buttonPrev)
                    .addComponent(buttonPrev1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonNext, buttonNext1, buttonPrev, buttonPrev1});

        buttonNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/addBlue32x32.png"))); // NOI18N
        buttonNovo.setText("Novo");
        buttonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNovoActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        tableEndereco.setBackground(java.awt.Color.lightGray);
        tableEndereco.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "Logradouro", "numero", "Complemento", "Bairro", "Cidade", "Estado", "Pais", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableEndereco.setFocusable(false);
        tableEndereco.setGridColor(java.awt.Color.lightGray);
        tableEndereco.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEnderecoMouseClicked(evt);
            }
        });
        ScrollPaneEnd.setViewportView(tableEndereco);

        buttonAddEnd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEndActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelEndLayout = new javax.swing.GroupLayout(panelEnd);
        panelEnd.setLayout(panelEndLayout);
        panelEndLayout.setHorizontalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
            .addGroup(panelEndLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddEnd))
        );
        panelEndLayout.setVerticalGroup(
            panelEndLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEndLayout.createSequentialGroup()
                .addComponent(buttonAddEnd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneEnd, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Endereços", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Home.png")), panelEnd); // NOI18N

        tableTelefone.setBackground(java.awt.Color.lightGray);
        tableTelefone.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "Telefone", "Observações", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableTelefone.setFocusable(false);
        tableTelefone.setGridColor(java.awt.Color.lightGray);
        tableTelefone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseReleased(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableTelefoneMouseClicked(evt);
            }
        });
        ScrollPaneFone.setViewportView(tableTelefone);

        buttonAddFone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddFone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddFoneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneFoneLayout = new javax.swing.GroupLayout(paneFone);
        paneFone.setLayout(paneFoneLayout);
        paneFoneLayout.setHorizontalGroup(
            paneFoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneFone, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
            .addGroup(paneFoneLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddFone))
        );
        paneFoneLayout.setVerticalGroup(
            paneFoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneFoneLayout.createSequentialGroup()
                .addComponent(buttonAddFone)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneFone, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Telefones", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Phone number.png")), paneFone); // NOI18N

        tableEmail.setBackground(java.awt.Color.lightGray);
        tableEmail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Object", "E-mail", "Alterar", "Remover"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableEmail.setAutoscrolls(false);
        tableEmail.setFocusable(false);
        tableEmail.setGridColor(java.awt.Color.lightGray);
        tableEmail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableEmailMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tableEmailMouseReleased(evt);
            }
        });
        ScrollPaneEmail.setViewportView(tableEmail);

        buttonAddEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/Create.png"))); // NOI18N
        buttonAddEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddEmailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paneEmailLayout = new javax.swing.GroupLayout(paneEmail);
        paneEmail.setLayout(paneEmailLayout);
        paneEmailLayout.setHorizontalGroup(
            paneEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
            .addGroup(paneEmailLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonAddEmail))
        );
        paneEmailLayout.setVerticalGroup(
            paneEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneEmailLayout.createSequentialGroup()
                .addComponent(buttonAddEmail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Email", new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem24X24/E-mail.png")), paneEmail); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(16, 16, 16)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 227, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(381, 381, 381)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(63, 63, 63)))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void textFieldRepeteSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldRepeteSenhaActionPerformed

    }//GEN-LAST:event_textFieldRepeteSenhaActionPerformed

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void buttonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_buttonSalvarActionPerformed

    private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange

    }//GEN-LAST:event_formPropertyChange

    private void buttonPrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrevActionPerformed
        anterior();
    }//GEN-LAST:event_buttonPrevActionPerformed

    private void buttonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNextActionPerformed
        proximo();
    }//GEN-LAST:event_buttonNextActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
    }//GEN-LAST:event_formWindowActivated

    private void buttonPrev1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrev1ActionPerformed
        buscaPrimeiroRegistro();
    }//GEN-LAST:event_buttonPrev1ActionPerformed

    private void buttonNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNext1ActionPerformed
        buscaUltimoRegistro();
    }//GEN-LAST:event_buttonNext1ActionPerformed

    private void buttonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNovoActionPerformed
        novo();
    }//GEN-LAST:event_buttonNovoActionPerformed

    private void tableEnderecoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableEnderecoMouseReleased

    private void tableEnderecoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEnderecoMouseClicked
        if (tableEndereco.getSelectedColumn() == 7) {
            alteraEndereco();
        } else if (tableEndereco.getSelectedColumn() == 8) {
            excluiEndereco();
        }
    }//GEN-LAST:event_tableEnderecoMouseClicked

    private void buttonAddEndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEndActionPerformed
        adicionaEndereco();
    }//GEN-LAST:event_buttonAddEndActionPerformed

    private void tableTelefoneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tableTelefoneMouseReleased

    private void tableTelefoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTelefoneMouseClicked
        if (tableTelefone.getSelectedColumn() == 2) {
            alteraTelefone();
        } else if (tableTelefone.getSelectedColumn() == 3) {
            excluiTelefone();
        }
    }//GEN-LAST:event_tableTelefoneMouseClicked

    private void buttonAddFoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddFoneActionPerformed
        adicionaTelefone();
    }//GEN-LAST:event_buttonAddFoneActionPerformed

    private void tableEmailMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseReleased

    }//GEN-LAST:event_tableEmailMouseReleased

    private void tableEmailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableEmailMouseClicked
        if (tableEmail.getSelectedColumn() == 1) {
            alteraEmail();
        } else if (tableEmail.getSelectedColumn() == 2) {
            excluiEmail();
        }
    }//GEN-LAST:event_tableEmailMouseClicked

    private void buttonAddEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddEmailActionPerformed
        adicionaEmail();
    }//GEN-LAST:event_buttonAddEmailActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ScrollPaneEmail;
    private javax.swing.JScrollPane ScrollPaneEnd;
    private javax.swing.JScrollPane ScrollPaneFone;
    private javax.swing.JButton buttonAddEmail;
    private javax.swing.JButton buttonAddEnd;
    private javax.swing.JButton buttonAddFone;
    private javax.swing.JButton buttonFechar;
    private javax.swing.ButtonGroup buttonGroupStatus;
    private javax.swing.ButtonGroup buttonGroupTipo;
    private javax.swing.JButton buttonNext;
    private javax.swing.JButton buttonNext1;
    private javax.swing.JButton buttonNovo;
    private javax.swing.JButton buttonPrev;
    private javax.swing.JButton buttonPrev1;
    private javax.swing.JButton buttonSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JPanel paneEmail;
    private javax.swing.JPanel paneFone;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel2;
    private javax.swing.JPanel panelEnd;
    private javax.swing.JRadioButton radioButtonAdministrador;
    private javax.swing.JRadioButton radioButtonAtivo;
    private javax.swing.JRadioButton radioButtonInativo;
    private javax.swing.JRadioButton radioButtonUsuario;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTable tableEmail;
    private javax.swing.JTable tableEndereco;
    private javax.swing.JTable tableTelefone;
    private javax.swing.JTextField textFieldApelido;
    private javax.swing.JTextField textFieldLogin;
    private javax.swing.JTextField textFieldNome;
    private javax.swing.JPasswordField textFieldRepeteSenha;
    private javax.swing.JPasswordField textFieldSenha;
    // End of variables declaration//GEN-END:variables
}
