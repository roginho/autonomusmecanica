package br.com.autonomusmecanica.gui;

import br.com.autonomusmecanica.dao.UsuarioDAOJPA;
import br.com.autonomusmecanica.entidades.Usuario;
import br.com.roger.utils.Singleton;
import br.com.autonomusmecanica.funcoes.StripedTable;
import br.com.autonomusmecanica.tableModel.ButtonColumn;
import br.com.autonomusmecanica.tableModel.TableModelUsuario;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

public final class DialogTabelaUsuario extends javax.swing.JDialog {

    private TableModelUsuario model;
    private TableCellRenderer renderer = new StripedTable();
    private TableRowSorter sorter;
    private final int colunaAlterar = 3; 

    public void atualizaTabela() {
        UsuarioDAOJPA dao = new UsuarioDAOJPA();
        List<Usuario> lista = dao.getAll(Usuario.class);
        model = new TableModelUsuario(lista);
        table.setModel(model);
        model.ordenarPorNome();
        table.getColumnModel().getColumn(0).setPreferredWidth(300);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.getColumnModel().getColumn(2).setPreferredWidth(30);
        table.getColumnModel().getColumn(colunaAlterar).setPreferredWidth(20);
        table.setDefaultRenderer(Object.class, renderer);
        table.setDefaultRenderer(String.class, renderer);
        new ButtonColumn(table, colunaAlterar, Singleton.getInstance().iconeJTable("alterar"));
        sorter = new TableRowSorter<>(model);
        table.setRowSorter(sorter);
        labelTotal.setText("" + table.getRowCount());
    }

    private void alterar() {
        int linha = table.getSelectedRow();
        if (linha >= 0) {
            Usuario u = new Usuario();
            u = (Usuario) model.getValueAt(linha, -1);
            DialogCadastroUsuario d = new DialogCadastroUsuario(null, true, u);
            d.setVisible(true);
            atualizaTabela();
        } else {
            JOptionPane.showMessageDialog(this, "Selecione uma linha para alterar");
        }
    }

    public DialogTabelaUsuario(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Cadastro de Usuario");
        atualizaTabela();
        textFieldPesquisa.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        buttonAdicionar = new javax.swing.JButton();
        textFieldPesquisa = new javax.swing.JTextField();
        buttonFechar = new javax.swing.JButton();
        labelTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        buttonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeAdicionar32x32.png"))); // NOI18N
        buttonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAdicionarActionPerformed(evt);
            }
        });

        textFieldPesquisa.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtro"));
        textFieldPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                textFieldPesquisaKeyReleased(evt);
            }
        });

        buttonFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusmecanica/gui/imagem/iconeFechar32x32.png"))); // NOI18N
        buttonFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFecharActionPerformed(evt);
            }
        });

        labelTotal.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonFechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textFieldPesquisa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldPesquisa)
                    .addComponent(buttonFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAdicionarActionPerformed
        DialogCadastroUsuario d = new DialogCadastroUsuario(null, true, null);
        d.setVisible(true);
        atualizaTabela();
    }//GEN-LAST:event_buttonAdicionarActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
    }//GEN-LAST:event_formWindowActivated

    private void textFieldPesquisaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyTyped

    }//GEN-LAST:event_textFieldPesquisaKeyTyped

    private void textFieldPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldPesquisaKeyReleased
        if (table.getSelectedColumn() > 0) {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), table.getSelectedColumn());
        } else {
            Singleton.getInstance().filtroSorter(sorter, textFieldPesquisa.getText(), 0);
        }

        labelTotal.setText("" + table.getRowCount());
    }//GEN-LAST:event_textFieldPesquisaKeyReleased

    private void buttonFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFecharActionPerformed
        this.dispose();
    }//GEN-LAST:event_buttonFecharActionPerformed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        if (table.getSelectedColumn() == colunaAlterar) {
            alterar();
        }
    }//GEN-LAST:event_tableMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdicionar;
    private javax.swing.JButton buttonFechar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JTable table;
    private javax.swing.JTextField textFieldPesquisa;
    // End of variables declaration//GEN-END:variables
}
