package br.com.autonomusmecanica.entidades;

import br.com.autonomusmecanica.dao.EnderecoDAOJPA;
import br.com.autonomusmecanica.dao.TelefoneDAOJPA;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "pessoas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pessoa.findAll", query = "SELECT p FROM Pessoa p"),
    @NamedQuery(name = "Pessoa.findByIdPessoa", query = "SELECT p FROM Pessoa p WHERE p.idPessoa = :idPessoa"),
    @NamedQuery(name = "Pessoa.findByNome", query = "SELECT p FROM Pessoa p WHERE p.nome = :nome and p.idPessoa <> :idPessoa"),
    @NamedQuery(name = "Pessoa.findByTipo", query = "SELECT p FROM Pessoa p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "Pessoa.findByCpfCnpj", query = "SELECT p FROM Pessoa p WHERE p.cpfCnpj = :cpfCnpj"),
    @NamedQuery(name = "Pessoa.findBySexo", query = "SELECT p FROM Pessoa p WHERE p.sexo = :sexo"),
    @NamedQuery(name = "Pessoa.findByDataNascAbertura", query = "SELECT p FROM Pessoa p WHERE p.dataNascAbertura = :dataNascAbertura")})
public class Pessoa implements Serializable {

    @OneToMany(mappedBy = "pessoa")
    private Collection<Telefone> telefoneCollection;
    @OneToMany(mappedBy = "pessoa")
    private Collection<Email> emailCollection;
    @OneToMany(mappedBy = "pessoa")
    private Collection<Endereco> enderecoCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPessoa")
    private Integer idPessoa;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "tipo")
    private Character tipo;
    @Column(name = "apelidoNomeFantasia")
    private String apelidoNomeFantasia;
    @Column(name = "cpfCnpj")
    private String cpfCnpj;
    @Column(name = "sexo")
    private Character sexo;
    @Column(name = "dataNascAbertura")
    @Temporal(TemporalType.DATE)
    private Date dataNascAbertura;
    @Column(name = "status")
    private Character status;

    public Pessoa() {
    }

    public Pessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Pessoa(Integer idPessoa, String nome, Character tipo) {
        this.idPessoa = idPessoa;
        this.nome = nome;
        this.tipo = tipo;
    }

    public Integer getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascAbertura() {
        return dataNascAbertura;
    }

    public void setDataNascAbertura(Date dataNascAbertura) {
        this.dataNascAbertura = dataNascAbertura;
    }

    public String getApelidoNomeFantasia() {
        return apelidoNomeFantasia;
    }

    public void setApelidoNomeFantasia(String apelidoNomeFantasia) {
        this.apelidoNomeFantasia = apelidoNomeFantasia;
    }

    public Character getStatus() {
        return status;
    }

    public String getEnderecoCompletoPessoa() {
        String dados = "";
        EnderecoDAOJPA end = new EnderecoDAOJPA();
        List<Endereco> listaEnderecos = end.getPessoaEndereco(this);
        if (listaEnderecos != null && !listaEnderecos.isEmpty()) {
            Endereco e = listaEnderecos.get(0);
            if (e.getLogradouro() != null) {
                dados += e.getLogradouro();
            }
            if (e.getNumero() != null && !e.getNumero().isEmpty()) {
                dados += ", " + e.getNumero();
            }
            if (e.getComplemento() != null && !e.getComplemento().isEmpty()) {
                dados += " - Complemento: " + e.getComplemento();
            }
            if (e.getBairro() != null && !e.getBairro().isEmpty()) {
                dados += " - Bairro: " + e.getBairro();
            }
            if (e.getCep() != null && !e.getCep().isEmpty()) {
                dados += " - Cep: " + e.getCep();
            }
            if (e.getCidade() != null) {
                dados += " - " + e.getCidade().getNome() + "/" + e.getCidade().getEstado().getUf();
            }
        }
//        System.out.println("" + dados);
        return dados;
    }
    
    public String getTelefonePessoa() {
        String dados = "";
        TelefoneDAOJPA o = new TelefoneDAOJPA();
        List<Telefone> lista= o.getPessoaTelefone(this);
        for(Telefone t: lista){
            dados+=t.getNumero()+" ";
        }
        
        
//        System.out.println("" + dados);
        return dados;
    }
    public void setStatus(Character status) {
        if (status == null) {
            status = 'a';
        }
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPessoa != null ? idPessoa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        return (this.idPessoa != null || other.idPessoa == null) && (this.idPessoa == null || this.idPessoa.equals(other.idPessoa));
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Pessoa[ idPessoa=" + idPessoa + " ]";
    }

    @XmlTransient
    public Collection<Telefone> getTelefoneCollection() {
        return telefoneCollection;
    }

    public void setTelefoneCollection(Collection<Telefone> telefoneCollection) {
        this.telefoneCollection = telefoneCollection;
    }

    @XmlTransient
    public Collection<Email> getEmailCollection() {
        return emailCollection;
    }

    public void setEmailCollection(Collection<Email> emailCollection) {
        this.emailCollection = emailCollection;
    }

    @XmlTransient
    public Collection<Endereco> getEnderecoCollection() {
        return enderecoCollection;
    }

    public void setEnderecoCollection(Collection<Endereco> enderecoCollection) {
        this.enderecoCollection = enderecoCollection;
    }

}
