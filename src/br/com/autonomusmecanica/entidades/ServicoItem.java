/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bianca
 */
@Entity
@Table(name = "servicositens")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicoItem.findAll", query = "SELECT s FROM ServicoItem s"),
    @NamedQuery(name = "ServicoItem.findByIdServicoItem", query = "SELECT s FROM ServicoItem s WHERE s.idServicoItem = :idServicoItem"),
    @NamedQuery(name = "ServicoItem.findByQuantidade", query = "SELECT s FROM ServicoItem s WHERE s.quantidade = :quantidade"),
    @NamedQuery(name = "ServicoItem.findByDescricao", query = "SELECT s FROM ServicoItem s WHERE s.descricao = :descricao"),
    @NamedQuery(name = "ServicoItem.findByValorUnitario", query = "SELECT s FROM ServicoItem s WHERE s.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "ServicoItem.findByServico", query = "SELECT s FROM ServicoItem s WHERE s.servico = :servico"),
    @NamedQuery(name = "ServicoItem.findByValorTotal", query = "SELECT s FROM ServicoItem s WHERE s.valorTotal = :valorTotal")})
public class ServicoItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idServicoItem")
    private Integer idServicoItem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private Double quantidade;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "valorUnitario")
    private Double valorUnitario;
    @Column(name = "valorTotal")
    private Double valorTotal;
    @ManyToOne(optional = false)
    @JoinColumn(name = "servico", referencedColumnName = "idServico")
    private Servico servico;
    @Column(name = "custoUnitario")
    private Double custoUnitario;
    @Column(name = "codigo")
    private String codigo;
    @Column(name = "fornecedor")
    private String fornecedor;

    public ServicoItem() {
    }

    public ServicoItem(Integer idServicoItem) {
        this.idServicoItem = idServicoItem;
    }

    public Integer getIdServicoItem() {
        return idServicoItem;
    }

    public void setIdServicoItem(Integer idServicoItem) {
        this.idServicoItem = idServicoItem;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public Double getCustoUnitario() {
        if(custoUnitario == null) {
            return 0.0;
        }
        return custoUnitario;
    }

    public void setCustoUnitario(Double custoUnitario) {
        this.custoUnitario = custoUnitario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Double getCustoTotal() {
        return this.custoUnitario * this.quantidade;
    }    

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServicoItem != null ? idServicoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicoItem)) {
            return false;
        }
        ServicoItem other = (ServicoItem) object;
        if ((this.idServicoItem == null && other.idServicoItem != null) || (this.idServicoItem != null && !this.idServicoItem.equals(other.idServicoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.ServicoItem[ idServicoItem=" + idServicoItem + " ]";
    }

}
