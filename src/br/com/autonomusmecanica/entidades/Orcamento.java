/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "orcamentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orcamento.findAll", query = "SELECT o FROM Orcamento o"),
    @NamedQuery(name = "Orcamento.findById", query = "SELECT o FROM Orcamento o WHERE o.id = :id"),
    @NamedQuery(name = "Orcamento.findByDataOrcamento", query = "SELECT o FROM Orcamento o WHERE o.dataOrcamento = :dataOrcamento"),
    @NamedQuery(name = "Orcamento.findByValor", query = "SELECT o FROM Orcamento o WHERE o.valorTotal = :valorTotal"),
    @NamedQuery(name = "Orcamento.findByDesconto", query = "SELECT o FROM Orcamento o WHERE o.desconto = :desconto")})
public class Orcamento implements Serializable {

    @OneToMany(mappedBy = "orcamento")

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "dataOrcamento")
    @Temporal(TemporalType.DATE)
    private Date dataOrcamento;
    @Column(name = "valorTotal")
    private Double valorTotal;
    @Column(name = "desconto")
    private Double desconto;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cliente", referencedColumnName = "idCliente")
    private Cliente cliente;
    @ManyToOne(optional = false)
    @JoinColumn(name = "veiculo", referencedColumnName = "idVeiculo")
    private Veiculo veiculo;
    @Column(name = "valorMO")
    private Double valorMO;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "status")
    private Character status;
    @Column(name = "valorMateriais")
    private Double valorMateriais;
    @Column(name = "validade")
    @Temporal(TemporalType.DATE)
    private Date validade;
    @ManyToOne(optional = false)
    @JoinColumn(name = "servico", referencedColumnName = "idServico")
    private Servico servico;

    public Orcamento() {
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataOrcamento() {
        return dataOrcamento;
    }

    public void setDataOrcamento(Date dataOrcamento) {
        this.dataOrcamento = dataOrcamento;
    }


    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Double getValorMO() {
        return valorMO;
    }

    public void setValorMO(Double valorMO) {
        this.valorMO = valorMO;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatusString() {
        String o = "";
        if (this.status.equals('a')) {
            o = "Aberto";
        } else if (this.status.equals('f')) {
            o = "Fechado";
        } else if (this.status.equals('c')) {
            o = "Cancelado";
        }
        return o;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }


    public Double getValorMateriais() {
        return valorMateriais;
    }

    public void setValorMateriais(Double valorMateriais) {
        this.valorMateriais = valorMateriais;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orcamento)) {
            return false;
        }
        Orcamento other = (Orcamento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Orcamento[ id=" + id + " ]";
    }
}
