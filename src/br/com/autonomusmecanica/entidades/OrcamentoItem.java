/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bianca
 */
@Entity
@Table(name = "orcamentositens")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrcamentoItem.findAll", query = "SELECT s FROM OrcamentoItem s"),
    @NamedQuery(name = "OrcamentoItem.findById", query = "SELECT s FROM OrcamentoItem s WHERE s.id = :id"),
    @NamedQuery(name = "OrcamentoItem.findByQuantidade", query = "SELECT s FROM OrcamentoItem s WHERE s.quantidade = :quantidade"),
    @NamedQuery(name = "OrcamentoItem.findByDescricao", query = "SELECT s FROM OrcamentoItem s WHERE s.descricao = :descricao"),
    @NamedQuery(name = "OrcamentoItem.findByValorUnitario", query = "SELECT s FROM OrcamentoItem s WHERE s.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "OrcamentoItem.findByOrcamento", query = "SELECT s FROM OrcamentoItem s WHERE s.orcamento = :orcamento"),
    @NamedQuery(name = "OrcamentoItem.findByValorTotal", query = "SELECT s FROM OrcamentoItem s WHERE s.valorTotal = :valorTotal")})
public class OrcamentoItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private Double quantidade;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "valorUnitario")
    private Double valorUnitario;
    @Column(name = "valorTotal")
    private Double valorTotal;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "orcamento", referencedColumnName = "id")
    private Orcamento orcamento;

    public OrcamentoItem() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrcamentoItem)) {
            return false;
        }
        OrcamentoItem other = (OrcamentoItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.OrcamentoItem[ id=" + id + " ]";
    }

}
