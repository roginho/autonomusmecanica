/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bianca
 */
@Entity
@Table(name = "servicosparcelas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicoParcela.findAll", query = "SELECT s FROM ServicoParcela s"),
    @NamedQuery(name = "ServicoParcela.findByIdServicoParcela", query = "SELECT s FROM ServicoParcela s WHERE s.idServicoParcela = :idServicoParcela"),
    @NamedQuery(name = "ServicoParcela.findByParcela", query = "SELECT s FROM ServicoParcela s WHERE s.parcela = :parcela"),
    @NamedQuery(name = "ServicoParcela.findByValorParcela", query = "SELECT s FROM ServicoParcela s WHERE s.valorParcela = :valorParcela"),
    @NamedQuery(name = "ServicoParcela.findByDataVcto", query = "SELECT s FROM ServicoParcela s WHERE s.dataVcto = :dataVcto"),
    @NamedQuery(name = "ServicoParcela.findByServico", query = "SELECT s FROM ServicoParcela s WHERE s.servico = :servico"),
    @NamedQuery(name = "ServicoParcela.findByClienteAberto", query = "SELECT s FROM ServicoParcela s WHERE s.servico.cliente = :cliente and s.quitado =0")})
public class ServicoParcela implements Serializable {

    @OneToMany(mappedBy = "servicoParcela")
    private Collection<PagamentoParcela> pagamentoParcelaCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idServicoParcela")
    private Integer idServicoParcela;
    @Column(name = "parcela")
    private Integer parcela;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorParcela")
    private Double valorParcela;
    @Lob
    @Column(name = "obs")
    private String obs;
    @Column(name = "dataVcto")
    @Temporal(TemporalType.DATE)
    private Date dataVcto;
    @Column(name = "dataPgto")
    @Temporal(TemporalType.DATE)
    private Date dataPgto;
    @ManyToOne(optional = false)
    @JoinColumn(name = "servico", referencedColumnName = "idServico")
    private Servico servico;
    @ManyToOne(optional = false)
    @JoinColumn(name = "FormaPgto", referencedColumnName = "idFormaPgto")
    private FormaPgto formaPgto;
    @Column(name = "quitado")
    private Integer quitado;
    @Column(name = "valorPago")
    private Double valorPago;
    
    @Transient
    private Boolean marcado;

    public ServicoParcela() {
    }

    public ServicoParcela(Integer idServicoParcela) {
        this.idServicoParcela = idServicoParcela;
    }

    public Integer getIdServicoParcela() {
        return idServicoParcela;
    }

    public void setIdServicoParcela(Integer idServicoParcela) {
        this.idServicoParcela = idServicoParcela;
    }

    public Integer getParcela() {
        return parcela;
    }

    public void setParcela(Integer parcela) {
        this.parcela = parcela;
    }

    public Double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(Double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Date getDataVcto() {
        return dataVcto;
    }

    public void setDataVcto(Date dataVcto) {
        this.dataVcto = dataVcto;
    }

    public Integer getQuitado() {
        return quitado;
    }

    public void setQuitado(Integer quitado) {
        this.quitado = quitado;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public FormaPgto getFormaPgto() {
        return formaPgto;
    }

    public void setFormaPgto(FormaPgto formaPgto) {
        this.formaPgto = formaPgto;
    }

    public Boolean getMarcado() {
        if (marcado == null) {
            marcado = false;
        }
        return marcado;
    }

    public void setMarcado(Boolean marcado) {
        this.marcado = marcado;
    }


    public Date getDataPgto() {
        return dataPgto;
    }

    public void setDataPgto(Date dataPgto) {
        this.dataPgto = dataPgto;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServicoParcela != null ? idServicoParcela.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicoParcela)) {
            return false;
        }
        ServicoParcela other = (ServicoParcela) object;
        if ((this.idServicoParcela == null && other.idServicoParcela != null) || (this.idServicoParcela != null && !this.idServicoParcela.equals(other.idServicoParcela))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.ServicoParcela[ idServicoParcela=" + idServicoParcela + " ]";
    }

    @XmlTransient
    public Collection<PagamentoParcela> getPagamentoParcelaCollection() {
        return pagamentoParcelaCollection;
    }

    public void setPagamentoParcelaCollection(Collection<PagamentoParcela> pagamentoParcelaCollection) {
        this.pagamentoParcelaCollection = pagamentoParcelaCollection;
    }

}
