package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "pessoasimagens")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PessoaImagem.findAll", query = "SELECT p FROM PessoaImagem p"),
    @NamedQuery(name = "PessoaImagem.findByIdPessoaImagem", query = "SELECT p FROM PessoaImagem p WHERE p.idPessoaImagem = :idPessoaImagem"),
    @NamedQuery(name = "PessoaImagem.findByPessoa", query = "SELECT p FROM PessoaImagem p WHERE p.pessoa = :pessoa")})

public class PessoaImagem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPessoaImagem")
    private Integer idPessoaImagem;
    @ManyToOne(optional = false)
    @JoinColumn(name = "pessoa", referencedColumnName = "idPessoa")
    private Pessoa pessoa;
    @Column(name = "nome")
    private String nome;
    
    public PessoaImagem() {
    }

    public PessoaImagem(Integer idPessoaImagem) {
        this.idPessoaImagem = idPessoaImagem;
    }

    public Integer getIdPessoaImagem() {
        return idPessoaImagem;
    }

    public void setIdPessoaImagem(Integer idPessoaImagem) {
        this.idPessoaImagem = idPessoaImagem;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPessoaImagem != null ? idPessoaImagem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PessoaImagem)) {
            return false;
        }
        PessoaImagem other = (PessoaImagem) object;
        if ((this.idPessoaImagem == null && other.idPessoaImagem != null) || (this.idPessoaImagem != null && !this.idPessoaImagem.equals(other.idPessoaImagem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.PessoaImagem[ idPessoaImagem=" + idPessoaImagem + " ]";
    }
    
}
