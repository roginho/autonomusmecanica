/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.awt.Color;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "cores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cor.findAll", query = "SELECT c FROM Cor c"),
    @NamedQuery(name = "Cor.findByIdCor", query = "SELECT c FROM Cor c WHERE c.idCor = :idCor"),
    @NamedQuery(name = "Cor.findByDescricao", query = "SELECT c FROM Cor c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Cor.findByExiste", query = "SELECT c FROM Cor c WHERE c.descricao = :descricao and c.idCor <> :idCor"),
    @NamedQuery(name = "Cor.findByCodigo", query = "SELECT c FROM Cor c WHERE c.codigo = :codigo")})
public class Cor implements Serializable {

    @OneToMany(mappedBy = "cor")
    private Collection<Veiculo> veiculoCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCor")
    private Integer idCor;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "codigo")
    private String codigo;

    public Cor() {
    }

    public Cor(Integer idCor) {
        this.idCor = idCor;
    }

    public Cor(Integer idCor, String descricao) {
        this.idCor = idCor;
        this.descricao = descricao;
    }

    public Integer getIdCor() {
        return idCor;
    }

    public void setIdCor(Integer idCor) {
        this.idCor = idCor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Color getBuscaCor() {
        Color cor = Color.gray;
        if (this.getCodigo() != null) {
            cor = Color.decode(this.getCodigo());
        }
        return cor;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCor != null ? idCor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cor)) {
            return false;
        }
        Cor other = (Cor) object;
        if ((this.idCor == null && other.idCor != null) || (this.idCor != null && !this.idCor.equals(other.idCor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

    @XmlTransient
    public Collection<Veiculo> getVeiculoCollection() {
        return veiculoCollection;
    }

    public void setVeiculoCollection(Collection<Veiculo> veiculoCollection) {
        this.veiculoCollection = veiculoCollection;
    }

}
