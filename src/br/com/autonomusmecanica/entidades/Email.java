/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import br.com.roger.validador.Validador;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "emails")
@NamedQueries({
    @NamedQuery(name = "Email.findAll", query = "SELECT p FROM Email p"),
    @NamedQuery(name = "Email.findByIdEmail", query = "SELECT p FROM Email p WHERE p.idEmail = :idEmail"),
    @NamedQuery(name = "Email.findByPessoaEmail", query = "SELECT p FROM Email p WHERE p.pessoa = :pessoa"),
    @NamedQuery(name = "Email.findByEmail", query = "SELECT p FROM Email p WHERE p.email = :email")})
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEmail")
    private Integer idEmail;
    @Column(name = "email")
    private String email;
    @ManyToOne(optional = false)
    @JoinColumn(name = "pessoa", referencedColumnName = "idPessoa")
    private Pessoa pessoa;

    public Email() {
    }

    public Email(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        email = email.toLowerCase();
        if (!email.isEmpty()) {
            boolean validaEmail = Validador.validaEmail(email);
            if (validaEmail) {
                this.email = email;
            } else {
                this.email = null;
            }
        } else {
            System.out.println("email vazio");
        }
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmail != null ? idEmail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Email)) {
            return false;
        }
        Email other = (Email) object;
        return (this.idEmail != null || other.idEmail == null) && (this.idEmail == null || this.idEmail.equals(other.idEmail));
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Email[ idEmail=" + idEmail + " ]";
    }

}
