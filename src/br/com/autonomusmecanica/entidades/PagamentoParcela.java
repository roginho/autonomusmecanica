/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bianca
 */
@Entity
@Table(name = "pagamentosparcelas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PagamentoParcela.findAll", query = "SELECT p FROM PagamentoParcela p"),
    @NamedQuery(name = "PagamentoParcela.findByIdPagamentoParcela", query = "SELECT p FROM PagamentoParcela p WHERE p.idPagamentoParcela = :idPagamentoParcela"),
    @NamedQuery(name = "PagamentoParcela.findByServicoParcela", query = "SELECT p FROM PagamentoParcela p WHERE p.servicoParcela = :servicoParcela"),
    @NamedQuery(name = "PagamentoParcela.findByDataPgto", query = "SELECT p FROM PagamentoParcela p WHERE p.dataPgto = :dataPgto"),
    @NamedQuery(name = "PagamentoParcela.findByValorPago", query = "SELECT p FROM PagamentoParcela p WHERE p.valorPago = :valorPago")})
public class PagamentoParcela implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPagamentoParcela")
    private Integer idPagamentoParcela;
    @Column(name = "dataPgto")
    @Temporal(TemporalType.DATE)
    private Date dataPgto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorPago")
    private Double valorPago;

    @ManyToOne(optional = false)
    @JoinColumn(name = "servicoParcela", referencedColumnName = "idServicoParcela")
    private ServicoParcela servicoParcela;

    public PagamentoParcela() {
    }

    public PagamentoParcela(Integer idPagamentoParcela) {
        this.idPagamentoParcela = idPagamentoParcela;
    }

    public Integer getIdPagamentoParcela() {
        return idPagamentoParcela;
    }

    public void setIdPagamentoParcela(Integer idPagamentoParcela) {
        this.idPagamentoParcela = idPagamentoParcela;
    }

    public Date getDataPgto() {
        return dataPgto;
    }

    public void setDataPgto(Date dataPgto) {
        this.dataPgto = dataPgto;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public ServicoParcela getServicoParcela() {
        return servicoParcela;
    }

    public void setServicoParcela(ServicoParcela servicoParcela) {
        this.servicoParcela = servicoParcela;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPagamentoParcela != null ? idPagamentoParcela.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagamentoParcela)) {
            return false;
        }
        PagamentoParcela other = (PagamentoParcela) object;
        if ((this.idPagamentoParcela == null && other.idPagamentoParcela != null) || (this.idPagamentoParcela != null && !this.idPagamentoParcela.equals(other.idPagamentoParcela))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.PagamentoParcela[ idPagamentoParcela=" + idPagamentoParcela + " ]";
    }

}
