/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import br.com.autonomusmecanica.dao.ServicoParcelaDAOJPA;
import br.com.roger.utils.Singleton;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "servicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servico.findAll", query = "SELECT s FROM Servico s"),
    @NamedQuery(name = "Servico.findByIdServico", query = "SELECT s FROM Servico s WHERE s.idServico = :idServico"),
    @NamedQuery(name = "Servico.findByDataServico", query = "SELECT s FROM Servico s WHERE s.dataServico = :dataServico"),
    @NamedQuery(name = "Servico.findByKilometragem", query = "SELECT s FROM Servico s WHERE s.kilometragem = :kilometragem"),
    @NamedQuery(name = "Servico.findByValor", query = "SELECT s FROM Servico s WHERE s.valorTotal = :valorTotal"),
    @NamedQuery(name = "Servico.findByDesconto", query = "SELECT s FROM Servico s WHERE s.desconto = :desconto")})
public class Servico implements Serializable {

    @OneToMany(mappedBy = "servico")
    private Collection<ServicoParcela> servicoParcelaCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idServico")
    private Integer idServico;
    @Column(name = "dataServico")
    @Temporal(TemporalType.DATE)
    private Date dataServico;
    @Column(name = "kilometragem")
    private Integer kilometragem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorTotal")
    private Double valorTotal;
    @Column(name = "desconto")
    private Double desconto;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cliente", referencedColumnName = "idCliente")
    private Cliente cliente;
    @ManyToOne(optional = false)
    @JoinColumn(name = "veiculo", referencedColumnName = "idVeiculo")
    private Veiculo veiculo;
    @Column(name = "valorMO")
    private Double valorMO;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "status")
    private Character status;
    @Column(name = "valorMateriais")
    private Double valorMateriais;
    @Transient
    List<ServicoParcela> listParcelas;
    @Transient
    private Double saldo;

    public Servico() {
    }

    public Servico(Integer idServico) {
        this.idServico = idServico;
    }

    public Integer getIdServico() {
        return idServico;
    }

    public void setIdServico(Integer idServico) {
        this.idServico = idServico;
    }

    public Date getDataServico() {
        return dataServico;
    }

    public void setDataServico(Date dataServico) {
        this.dataServico = dataServico;
    }

    public Integer getKilometragem() {
        if (kilometragem == null) {
            kilometragem = 0;
        }
        return kilometragem;
    }

    public void setKilometragem(Integer kilometragem) {
        this.kilometragem = kilometragem;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Double getValorMO() {
        return valorMO;
    }

    public void setValorMO(Double valorMO) {
        this.valorMO = valorMO;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatusString() {
        String s = "";
        if (this.status.equals('a')) {
            s = "Aberto";
        } else if (this.status.equals('f')) {
            s = "Fechado";
        } else if (this.status.equals('c')) {
            s = "Cancelado";
        }

        return s;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Double getValorMateriais() {
        return valorMateriais;
    }

    public void setValorMateriais(Double valorMateriais) {
        this.valorMateriais = valorMateriais;
    }

    public String getParcelasRelatorio() {
        this.listParcelas = getListParcelas();
        String parcelas = "";
        String dataVcto = "";
        String obs = "";
        for (ServicoParcela p : listParcelas) {
            if (p.getDataVcto() != null) {
                dataVcto = Singleton.getInstance().converteDataParaString(p.getDataVcto());
            }

            if (p.getObs() != null && !p.getObs().trim().isEmpty()) {
                obs = " - OBS: " + p.getObs();
            }
            parcelas += "Parcela: " + p.getParcela() + " Vencto: " + dataVcto + " Valor " + Singleton.getInstance().converteDoubleParaString(p.getValorParcela(), 2) + " "
                    + " Forma Pgto: " + p.getFormaPgto().getDescricao() + obs + "\n";
        }
        getSaldo();
        parcelas += "\nVALOR PAGO R$ " + Singleton.getInstance().converteDoubleParaString(this.getValorTotal() - saldo, 2);
        parcelas += "\nVALOR A RECEBER R$ " + Singleton.getInstance().converteDoubleParaString(saldo, 2);
        return parcelas;
    }

    public Double getSaldo() {
        ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
        this.saldo = dao.getSaldoRestanteServico(this);
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public List<ServicoParcela> getListParcelas() {
        ServicoParcelaDAOJPA dao = new ServicoParcelaDAOJPA();
        this.listParcelas = dao.getParcelasServico(this);
        return listParcelas;
    }

    public void setListParcelas(List<ServicoParcela> listParcelas) {
        this.listParcelas = listParcelas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServico != null ? idServico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servico)) {
            return false;
        }
        Servico other = (Servico) object;
        if ((this.idServico == null && other.idServico != null) || (this.idServico != null && !this.idServico.equals(other.idServico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Servico[ idServico=" + idServico + " ]";
    }

    @XmlTransient
    public Collection<ServicoParcela> getServicoParcelaCollection() {
        return servicoParcelaCollection;
    }

    public void setServicoParcelaCollection(Collection<ServicoParcela> servicoParcelaCollection) {
        this.servicoParcelaCollection = servicoParcelaCollection;
    }

}
