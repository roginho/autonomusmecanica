/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "formapgto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormaPgto.findAll", query = "SELECT f FROM FormaPgto f"),
    @NamedQuery(name = "FormaPgto.findByIdFormaPgto", query = "SELECT f FROM FormaPgto f WHERE f.idFormaPgto = :idFormaPgto"),
    @NamedQuery(name = "FormaPgto.findByExiste", query = "SELECT f FROM FormaPgto f WHERE f.descricao = :descricao and f.idFormaPgto <> :idFormaPgto"),
    @NamedQuery(name = "FormaPgto.findByDescricao", query = "SELECT f FROM FormaPgto f WHERE f.descricao = :descricao")})
public class FormaPgto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFormaPgto")
    private Integer idFormaPgto;
    @Column(name = "descricao")
    private String descricao;

    public FormaPgto() {
    }

    public FormaPgto(Integer idFormaPgto) {
        this.idFormaPgto = idFormaPgto;
    }

    public Integer getIdFormaPgto() {
        return idFormaPgto;
    }

    public void setIdFormaPgto(Integer idFormaPgto) {
        this.idFormaPgto = idFormaPgto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFormaPgto != null ? idFormaPgto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormaPgto)) {
            return false;
        }
        FormaPgto other = (FormaPgto) object;
        if ((this.idFormaPgto == null && other.idFormaPgto != null) || (this.idFormaPgto != null && !this.idFormaPgto.equals(other.idFormaPgto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
