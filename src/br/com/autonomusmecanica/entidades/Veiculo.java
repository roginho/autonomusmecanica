/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "veiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veiculo.findAll", query = "SELECT v FROM Veiculo v"),
    @NamedQuery(name = "Veiculo.findByIdVeiculo", query = "SELECT v FROM Veiculo v WHERE v.idVeiculo = :idVeiculo"),
    @NamedQuery(name = "Veiculo.findByPlaca", query = "SELECT v FROM Veiculo v WHERE v.placa = :placa"),
    @NamedQuery(name = "Veiculo.findByCliente", query = "SELECT v FROM Veiculo v WHERE v.cliente = :cliente"),
    @NamedQuery(name = "Veiculo.findByClienteIsNull", query = "SELECT v FROM Veiculo v WHERE v.cliente is null"),
    @NamedQuery(name = "Veiculo.findByClienteIsNotNull", query = "SELECT v FROM Veiculo v WHERE v.cliente is not null"),
    @NamedQuery(name = "Veiculo.findByExiste", query = "SELECT v FROM Veiculo v WHERE v.placa = :placa and v.idVeiculo <> :idVeiculo")})
public class Veiculo implements Serializable {

    @OneToMany(mappedBy = "veiculo")
    private Collection<Servico> servicoCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idVeiculo")
    private Integer idVeiculo;
    @Column(name = "placa")
    private String placa;
    @ManyToOne(optional = false)
    @JoinColumn(name = "modelo", referencedColumnName = "idModelo")
    private Modelo modelo;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cor", referencedColumnName = "idCor")
    private Cor cor;
    @Column(name = "ano")
    private Integer ano;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cliente", referencedColumnName = "idCliente")
    private Cliente cliente;
    @Transient
    private String descricaoCompleta;
    public Veiculo() {
    }

    public Veiculo(Integer idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public Integer getIdVeiculo() {
        return idVeiculo;
    }

    public void setIdVeiculo(Integer idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public Cor getCor() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getDescricaoCompleta() {
        descricaoCompleta = this.getModelo().getFabricante().getNome()
                +" - "+this.getModelo().getDescricao()+" Ano: "+this.getAno()+" - Cor: "+this.getCor().getDescricao()
                +" - Placa: "+this.getPlaca();
        return descricaoCompleta;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVeiculo != null ? idVeiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veiculo)) {
            return false;
        }
        Veiculo other = (Veiculo) object;
        if ((this.idVeiculo == null && other.idVeiculo != null) || (this.idVeiculo != null && !this.idVeiculo.equals(other.idVeiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Veiculo[ idVeiculo=" + idVeiculo + " ]";
    }

    @XmlTransient
    public Collection<Servico> getServicoCollection() {
        return servicoCollection;
    }

    public void setServicoCollection(Collection<Servico> servicoCollection) {
        this.servicoCollection = servicoCollection;
    }

}
