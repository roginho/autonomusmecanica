/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusmecanica.entidades;

import java.io.File;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "configuracoes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Configuracao.findAll", query = "SELECT c FROM Configuracao c"),
    @NamedQuery(name = "Configuracao.findByIdConfiguracoes", query = "SELECT c FROM Configuracao c WHERE c.idConfiguracoes = :idConfiguracoes")})
public class Configuracao implements Serializable {

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idConfiguracoes")
    private Integer idConfiguracoes;
    @ManyToOne(optional = false)
    @JoinColumn(name = "cidade", referencedColumnName = "idCidade")
    private Cidade cidade;
    @ManyToOne(optional = false)
    @JoinColumn(name = "estado", referencedColumnName = "idEstado")
    private Estado estado;
    @ManyToOne(optional = false)
    @JoinColumn(name = "pais", referencedColumnName = "idPais")
    private Pais pais;
    @ManyToOne(optional = false)
    @JoinColumn(name = "formapgto", referencedColumnName = "idFormaPgto")
    private FormaPgto formaPgto;
    @Column(name = "versaoDB")
    private Double versaoDB;
   
    @Transient
    private String imagemUsuario;

    public Configuracao() {
    }

    public Configuracao(Integer idConfiguracoes) {
        this.idConfiguracoes = idConfiguracoes;
    }

    public Integer getIdConfiguracoes() {
        return idConfiguracoes;
    }

    public void setIdConfiguracoes(Integer idConfiguracoes) {
        this.idConfiguracoes = idConfiguracoes;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public FormaPgto getFormaPgto() {
        return formaPgto;
    }

    public void setFormaPgto(FormaPgto formaPgto) {
        this.formaPgto = formaPgto;
    }

    public String getImagemUsuario() {
        String path = System.getProperty("user.dir")+"/imagem";
        System.out.println(path);
        if(path==null){
            File f = new File(path);
            f.mkdir();
        }
        return imagemUsuario;
    }

    public void setImagemUsuario(String imagemUsuario) {
        this.imagemUsuario = imagemUsuario;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConfiguracoes != null ? idConfiguracoes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Configuracao)) {
            return false;
        }
        Configuracao other = (Configuracao) object;
        if ((this.idConfiguracoes == null && other.idConfiguracoes != null) || (this.idConfiguracoes != null && !this.idConfiguracoes.equals(other.idConfiguracoes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.Configuracao[ idConfiguracoes=" + idConfiguracoes + " ]";
    }

    public Double getVersaoDB() {
        return versaoDB;
    }

    public void setVersaoDB(Double versaoDB) {
        this.versaoDB = versaoDB;
    }

}
