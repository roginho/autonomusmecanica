/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.autonomusmecanica.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roger
 */
@Entity
@Table(name = "usuarioslog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioLog.findAll", query = "SELECT u FROM UsuarioLog u"),
    @NamedQuery(name = "UsuarioLog.findByIdUsuarioLog", query = "SELECT u FROM UsuarioLog u WHERE u.idUsuarioLog = :idUsuarioLog")})
public class UsuarioLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUsuarioLog")
    private Integer idUsuarioLog;
    @Column(name = "dataHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHora;
    @Column(name = "host")
    private String host;
    @Column(name = "ip")
    private String ip;
    @ManyToOne(optional = false)
    @JoinColumn(name = "usuario", referencedColumnName = "idUsuario")
    private Usuario usuario;
    
    public UsuarioLog() {
    }

    public UsuarioLog(Integer idUsuarioLog) {
        this.idUsuarioLog = idUsuarioLog;
    }

    public Integer getIdUsuarioLog() {
        return idUsuarioLog;
    }

    public void setIdUsuarioLog(Integer idUsuarioLog) {
        this.idUsuarioLog = idUsuarioLog;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarioLog != null ? idUsuarioLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioLog)) {
            return false;
        }
        UsuarioLog other = (UsuarioLog) object;
        return (this.idUsuarioLog != null || other.idUsuarioLog == null) && (this.idUsuarioLog == null || this.idUsuarioLog.equals(other.idUsuarioLog));
    }

    @Override
    public String toString() {
        return "br.com.roger.entidades.UsuarioLog[ idUsuarioLog=" + idUsuarioLog + " ]";
    }
    
}
