package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pessoa;

/**
 *
 * @author roger
 */
public interface PessoaDAO extends DAO<Pessoa, Integer>{
}
