package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.PessoaImagem;

/**
 *
 * @author roger
 */
public interface PessoaImagemDAO extends DAO<PessoaImagem, Integer>{
}
