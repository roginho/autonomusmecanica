package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Servico;
/**
 *
 * @author roger
 */
public interface ServicoDAO extends DAO<Servico, Integer>{
}
