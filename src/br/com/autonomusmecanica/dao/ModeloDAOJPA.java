package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Fabricante;
import br.com.autonomusmecanica.entidades.Modelo;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class ModeloDAOJPA extends DAOJPA<Modelo, Integer> implements ModeloDAO {

    public List<Modelo> getExisteModelo(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Modelo.findByExiste");
        query.setParameter("descricao", nome).
                setParameter("idModelo", id);
        return query.getResultList();
    }

    public List<Modelo> getModeloFabricante(Fabricante fabricante) {
        Query query = getEntityManager().createNamedQuery("Modelo.findByFabricante");
        query.setParameter("fabricante", fabricante);
        return query.getResultList();
    }

}
