package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Orcamento;
/**
 *
 * @author roger
 */
public interface OrcamentoDAO extends DAO<Orcamento, Integer>{
}
