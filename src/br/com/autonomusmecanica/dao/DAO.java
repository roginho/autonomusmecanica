/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.autonomusmecanica.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author roger
 */
public interface DAO <T,I extends Serializable>{
    
    T save (T entity);
    
    boolean remove(Class <T> classe, I pk);
    
    T getById(Class<T> classe, I pk);
    
    List<T> getAll(Class<T> classe);
    
    EntityManager getEntityManager();
  
    
    
}
