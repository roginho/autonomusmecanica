package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Configuracao;

/**
 *
 * @author roger
 */
public interface ConfiguracaoDAO extends DAO<Configuracao, Integer>{
}
