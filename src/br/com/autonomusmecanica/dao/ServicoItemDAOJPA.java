package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.ServicoItem;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class ServicoItemDAOJPA extends DAOJPA<ServicoItem, Integer> implements ServicoItemDAO {

    public List<ServicoItem> getItemServico(Servico servico) {
        Query query = getEntityManager().createNamedQuery("ServicoItem.findByServico");
        query.setParameter("servico", servico);
        return query.getResultList();
    }  
}
