package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Usuario;
import br.com.autonomusmecanica.funcoes.SHACheckSum;
import br.com.roger.utils.Singleton;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class UsuarioDAOJPA extends DAOJPA<Usuario, Integer> implements UsuarioDAO {

    public Object getUsuariosByLogin(String login, char[] senhaCripto) {
        try {
            String senha  = Singleton.getInstance().criptografaSenha(senhaCripto);
            Query query = getEntityManager().createNamedQuery("Usuario.findByUsuarioLogin");
            query.setParameter("login", login).
                    setParameter("senha", senha);
            Usuario u = (Usuario) query.getSingleResult();
            return u;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Usuario> getUsuariosExiste(String login, Integer idUsuario) {
        Query query = getEntityManager().createNamedQuery("Usuario.findByExiste");
        query.setParameter("login", login).
                setParameter("idUsuario", idUsuario);
        return query.getResultList();
    }
}
