package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Fabricante;
import br.com.autonomusmecanica.entidades.Modelo;
import br.com.autonomusmecanica.entidades.Orcamento;
import br.com.autonomusmecanica.entidades.Veiculo;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class OrcamentoDAOJPA extends DAOJPA<Orcamento, Integer> implements OrcamentoDAO {

    public List<Orcamento> getOrcamentos(String status) {
        List<Orcamento> listaOrcamentos = getOrcamentos(null, null, status);
        return listaOrcamentos;
    }

    public List<Orcamento> getOrcamentos(Date ini, Date fim, String status) {
        List<Orcamento> listaOrcamentos = getOrcamentos(ini, fim, status, null, null, null, null);
        return listaOrcamentos;
    }

    public List<Orcamento> getOrcamentos(Date ini, Date fim, String status, Cliente cliente, Veiculo veiculo, Fabricante fabricante, Modelo modelo) {
        Map<String, Object> param = new HashMap<String, Object>();
        String sql = "SELECT l FROM " + Orcamento.class.getSimpleName() + " l WHERE 1=1 ";
        if (status != null && !status.trim().isEmpty()) {
            sql += " and l.status = '" + status + "'";
        }
        if (ini != null && fim != null) {
            sql += " and l.dataOrcamento between :dataIni and :dataFim ";
            param.put("dataIni", ini);
            param.put("dataFim", fim);
        }
        if (cliente != null) {
            sql += " and l.cliente = :cliente ";
            param.put("cliente", cliente);
        }
        if (veiculo != null) {
            sql += " and l.veiculo = :veiculo";
            param.put("veiculo", veiculo);
        }
        if (fabricante != null) {
            sql += " and l.veiculo.modelo.fabricante = :fabricante";
            param.put("fabricante", fabricante);
        }
        if (modelo != null) {
            sql += " and l.veiculo.modelo = :modelo";
            param.put("modelo", modelo);
        }

        Query query = getEntityManager().createQuery(sql);
        for (Map.Entry<String, Object> valor : param.entrySet()) {
            query.setParameter(valor.getKey(), valor.getValue());
        }
        return query.getResultList();
    }
}
