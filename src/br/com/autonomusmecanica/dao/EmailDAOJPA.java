package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Email;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class EmailDAOJPA extends DAOJPA<Email, Integer> implements EmailDAO {

    public List<Email> getPessoaEmail(Pessoa pessoa) {
        Query query = getEntityManager().createNamedQuery("Email.findByPessoaEmail");
        query.setParameter("pessoa", pessoa);
        return query.getResultList();
    }
}
