package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cliente;

/**
 *
 * @author roger
 */
public interface ClienteDAO extends DAO<Cliente, Integer>{
}
