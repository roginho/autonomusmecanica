package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.UsuarioLog;

/**
 *
 * @author roger
 */
public interface UsuarioLogDAO extends DAO<UsuarioLog, Integer>{
}
