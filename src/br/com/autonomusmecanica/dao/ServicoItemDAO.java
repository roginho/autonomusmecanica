package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.ServicoItem;
/**
 *
 * @author roger
 */
public interface ServicoItemDAO extends DAO<ServicoItem, Integer>{
}
