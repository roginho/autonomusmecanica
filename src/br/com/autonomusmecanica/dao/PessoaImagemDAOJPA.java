package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.PessoaImagem;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class PessoaImagemDAOJPA extends DAOJPA<PessoaImagem, Integer> implements PessoaImagemDAO {

    public List<PessoaImagem> getPessoaImagem(Pessoa pessoa) {
        Query query = getEntityManager().createNamedQuery("PessoaImagem.findByPessoa");
        query.setParameter("pessoa", pessoa);
        return query.getResultList();
    }

}
