package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cidade;

/**
 *
 * @author roger
 */
public interface CidadeDAO extends DAO<Cidade, Integer>{
}
