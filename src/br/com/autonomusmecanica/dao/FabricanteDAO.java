package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Fabricante;
/**
 *
 * @author roger
 */
public interface FabricanteDAO extends DAO<Fabricante, Integer>{
}
