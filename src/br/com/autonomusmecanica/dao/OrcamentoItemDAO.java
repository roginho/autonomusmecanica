package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.OrcamentoItem;
/**
 *
 * @author roger
 */
public interface OrcamentoItemDAO extends DAO<OrcamentoItem, Integer>{
}
