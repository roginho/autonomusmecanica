package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cor;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class CorDAOJPA extends DAOJPA<Cor, Integer> implements CorDAO {

    public List<Cor> getExisteCor(String descricao, Integer id) {
        Query query = getEntityManager().createNamedQuery("Cor.findByExiste");
        query.setParameter("descricao", descricao).
                setParameter("idCor", id);
        return query.getResultList();
    }
}
