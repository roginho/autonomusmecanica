package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pais;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class PaisDAOJPA extends DAOJPA<Pais, Integer> implements PaisDAO {

public List<Pais> getExistePais(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Pais.findByExiste");
        query.setParameter("nome", nome).
                setParameter("idPais", id);
        return query.getResultList();
    }
}
