package br.com.autonomusmecanica.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;

/**
 *
 * @author roger
 * @param <T>
 * @param <I>
 */
public abstract class DAOJPA<T, I extends Serializable> implements DAO<T, I> {

    private static DAOConexao conexao;

    @Override
    public T save(T entity) {
        T saved = null;
        try {
            getEntityManager().getTransaction().begin();
            saved = getEntityManager().merge(entity);
            getEntityManager().getTransaction().commit();
        } catch (Exception e) {
            if (getEntityManager().getTransaction().isActive() == false) {
                getEntityManager().getTransaction().begin();
            }
            getEntityManager().getTransaction().rollback();
            JOptionPane.showMessageDialog(null, "Erro ao salvar" + e.getMessage(), "erro", JOptionPane.ERROR_MESSAGE);
        }
        return saved;
    }

    @Override
    public boolean remove(Class<T> classe, I pk) {
        boolean estado = false;
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().remove(getEntityManager().getReference(classe, pk));
            getEntityManager().getTransaction().commit();
        } catch (Exception e) {
            if (getEntityManager().getTransaction().isActive() == false) {
                getEntityManager().getTransaction().begin();
            }
            getEntityManager().getTransaction().rollback();
            JOptionPane.showMessageDialog(null, "Erro ao excluir" + e.getMessage(), "erro", JOptionPane.ERROR_MESSAGE);
        }
        return estado;
    }

    @Override
    public T getById(Class<T> classe, I pk) {

        try {
            return getEntityManager().find(classe, pk);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao buscar" + e.getMessage(), "erro", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    @Override
    public List<T> getAll(Class<T> classe) {
        return getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x").getResultList();

    }

    public List<T> getAllCrescente(Class<T> classe, String campo) {
        return getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x order by x." + campo).getResultList();

    }

    public List<T> getLimiteDecrescente(Class<T> classe, Integer limit) {
        return getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x" + " order by x desc").
                setMaxResults(limit).getResultList();
    }

    public List<T> getLimiteCrescente(Class<T> classe, Integer limit) {
        return getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x" + " order by x asc").
                setMaxResults(limit).getResultList();
    }

    public Object primeiroRegistro(Class<T> classe) {
        Object object = null;
        try {
            object = getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x").
                    setMaxResults(1).getResultList().iterator().next();
        } catch (Exception e) {
            e.getMessage();
        }
        return object;
    }

    public Object ultimoRegistro(Class<T> classe, String chavePrimaria) {
        Object object = getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x order by x." + chavePrimaria + " desc").
                setMaxResults(1).getResultList().iterator().next();
        return object;
    }

    public Object getProximo(Class<T> classe, String chavePrimaria, I pk) {
        List<T> all = getAll(classe);
        Object object = getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x where x." + chavePrimaria + " = " + pk).
                setMaxResults(1).getResultList().listIterator().next();
        int indexOf = all.indexOf(object);
        if (indexOf < all.size() - 1) {
            object = all.get(indexOf + 1);
        }
        return object;

    }

    public Object getAnterior(Class<T> classe, String chavePrimaria, I pk) {
        List<T> all = getAll(classe);
        Object object = getEntityManager().createQuery("select x from " + classe.getSimpleName() + " x where x." + chavePrimaria + " = " + pk).
                setMaxResults(1).getResultList().listIterator().next();
        int indexOf = all.indexOf(object);
        if (indexOf > 0) {
            object = all.get(indexOf - 1);
        }
        return object;
    }

    @Override
    public EntityManager getEntityManager() {
        if (conexao == null) {
            conexao = new DAOConexao();
        }
        return conexao.getEntityManager();
    }
}
