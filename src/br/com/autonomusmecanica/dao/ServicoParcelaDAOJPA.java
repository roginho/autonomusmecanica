package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.FormaPgto;
import br.com.autonomusmecanica.entidades.Servico;
import br.com.autonomusmecanica.entidades.ServicoParcela;
import br.com.autonomusmecanica.entidades.Veiculo;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class ServicoParcelaDAOJPA extends DAOJPA<ServicoParcela, Integer> implements ServicoParcelaDAO {

    public List<ServicoParcela> getParcelasServico(Servico servico) {
        Query query = getEntityManager().createNamedQuery("ServicoParcela.findByServico");
        query.setParameter("servico", servico);
        return query.getResultList();
    }

    public List<ServicoParcela> getParcelasCliente(Cliente cliente) {
        Query query = getEntityManager().createNamedQuery("ServicoParcela.findByClienteAberto");
        query.setParameter("cliente", cliente);
        return query.getResultList();
    }

    public List<ServicoParcela> getServicosParcelas(Date ini, Date fim, Cliente cliente, Veiculo veiculo, String pg, FormaPgto formaPgto) {
        Map<String, Object> param = new HashMap<>();
        String sql = "SELECT l FROM " + ServicoParcela.class.getSimpleName() + " l WHERE 1=1 ";
        if (pg != null) {
            if (pg.equalsIgnoreCase("a")) {
                sql += " and l.dataPgto is null ";
            } else if (pg.equalsIgnoreCase("q")) {
                sql += " and l.dataPgto is not null ";
            }
        }
        if (ini != null && fim != null) {
            sql += " and l.dataVcto between :dataIni and :dataFim ";
            param.put("dataIni", ini);
            param.put("dataFim", fim);
        }
        if (cliente != null) {
            sql += " and l.cliente = :cliente ";
            param.put("cliente", cliente);
        }
        if (veiculo != null) {
            sql += " and l.veiculo = :veiculo";
            param.put("veiculo", veiculo);
        }
        if (formaPgto != null) {
            sql += " and l.formaPgto = :formaPgto";
            param.put("formaPgto", formaPgto);
        }
        Query query = getEntityManager().createQuery(sql);
        for (Map.Entry<String, Object> valor : param.entrySet()) {
            query.setParameter(valor.getKey(), valor.getValue());
        }
        return query.getResultList();
    }

    public Double getSaldoRestanteServico(Servico s) {
        Double saldo = s.getValorTotal();
        List<ServicoParcela> listaItens = getParcelasServico(s);
        for (ServicoParcela sp : listaItens) {
            saldo -= sp.getValorPago();
        }
        return saldo;
    }

}
