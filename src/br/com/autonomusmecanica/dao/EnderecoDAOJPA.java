package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Endereco;
import br.com.autonomusmecanica.entidades.Pessoa;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class EnderecoDAOJPA extends DAOJPA<Endereco, Integer> implements EnderecoDAO {

    public List<Endereco> getPessoaEndereco(Pessoa pessoa) {
        Query query = getEntityManager().createNamedQuery("Endereco.findByPessoaEndereco");
        query.setParameter("pessoa", pessoa);
        return query.getResultList();
    }
}
