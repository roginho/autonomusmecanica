package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cliente;
import br.com.autonomusmecanica.entidades.Fabricante;
import br.com.autonomusmecanica.entidades.Veiculo;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class VeiculoDAOJPA extends DAOJPA<Veiculo, Integer> implements VeiculoDAO {

    public List<Veiculo> getExisteVeiculo(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Veiculo.findByExiste");
        query.setParameter("placa", nome).
                setParameter("idVeiculo", id);
        return query.getResultList();
    }

    public List<Veiculo> getVeiculosCliente(Cliente cliente) {
        Query query = getEntityManager().createNamedQuery("Veiculo.findByCliente");
        query.setParameter("cliente", cliente);
        return query.getResultList();
    }

    public List<Veiculo> getVeiculosNaoVinculados() {
        Query query = getEntityManager().createNamedQuery("Veiculo.findByClienteIsNull");
        return query.getResultList();
    }

    public List<Veiculo> getVeiculosVinculados() {
        Query query = getEntityManager().createNamedQuery("Veiculo.findByClienteIsNotNull");
        return query.getResultList();
    }

    public List<Veiculo> getVeiculoPlaca(String placa) {
        Query query = getEntityManager().createNamedQuery("Veiculo.findByPlaca");
        query.setParameter("placa", placa);
        return query.getResultList();
    }

}
