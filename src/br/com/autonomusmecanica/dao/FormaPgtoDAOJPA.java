package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.FormaPgto;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class FormaPgtoDAOJPA extends DAOJPA<FormaPgto, Integer> implements FormaPgtoDAO {

    public List<FormaPgto> getExisteFormaPgto(String descricao, Integer id) {
        Query query = getEntityManager().createNamedQuery("FormaPgto.findByExiste");
        query.setParameter("descricao", descricao).
                setParameter("idFormaPgto", id);
        return query.getResultList();
    }
}
