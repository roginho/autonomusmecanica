package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.ServicoParcela;
/**
 *
 * @author roger
 */
public interface ServicoParcelaDAO extends DAO<ServicoParcela, Integer>{
}
