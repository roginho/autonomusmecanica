package br.com.autonomusmecanica.dao;

import br.com.autonomusconfig.AutonomusConfig;
import br.com.autonomusconfig.classes.Configuracao;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author roger
 */
public class DAOConexao {

    private static final String UNIT_NAME = "AutonomusMecanicaPU";
    private EntityManagerFactory emf = null;
    private EntityManager em = null;
    private String user = "mecanica"; // usuario  
    private String password = "bobpai"; // senha  

    public EntityManager getEntityManager() {
        Configuracao conf = AutonomusConfig.getInstance().getArquivoConfiguracao();
        if (emf == null) {
            if (conf != null) {
                String nomeBanco = conf.getConfiguracaoGeral().getNomeBanco();
                String porta = conf.getConfiguracaoGeral().getPorta();
                String ipServidor = conf.getConfiguracaoGeral().getIpServidor();
                Map configProperties = configProperties(nomeBanco, porta, ipServidor);
                emf = Persistence.createEntityManagerFactory(UNIT_NAME, configProperties);
            }else{
                System.out.println("Configuracao null");
                        
            }
        }
        if (em == null) {
            em = emf.createEntityManager();
        }
        return em;
    }

    private Map configProperties(String banco, String porta, String ip) {
        Map<String, String> persistenceProperties = new HashMap<>();
        String url = "jdbc:mysql://" + ip + ":" + porta + "/" + banco + "?zeroDateTimeBehavior=convertToNull";
        persistenceProperties.put("javax.persistence.jdbc.url", url);
        persistenceProperties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
        persistenceProperties.put("javax.persistence.jdbc.user", user);
        persistenceProperties.put("javax.persistence.jdbc.password", password);
        return persistenceProperties;
    }
}
