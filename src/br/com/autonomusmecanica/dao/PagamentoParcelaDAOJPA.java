package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.PagamentoParcela;
import br.com.autonomusmecanica.entidades.ServicoParcela;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class PagamentoParcelaDAOJPA extends DAOJPA<PagamentoParcela, Integer> implements PagamentoParcelaDAO {

    public List<PagamentoParcela> getPagamentosParcelas(ServicoParcela servicoParcela) {
        Query query = getEntityManager().createNamedQuery("PagamentoParcela.findByServicoParcela");
        query.setParameter("servicoParcela", servicoParcela);
        return query.getResultList();
    }

    public Double getTotalPagamentosParcelas(ServicoParcela servicoParcela) {
        Double valor = 0d;
        Query query = getEntityManager().createNamedQuery("PagamentoParcela.findByServicoParcela");
        query.setParameter("servicoParcela", servicoParcela);
        List<PagamentoParcela> list = query.getResultList();
        for (PagamentoParcela p : list) {
            valor+=p.getValorPago();
        }
        return valor;
    }
}
