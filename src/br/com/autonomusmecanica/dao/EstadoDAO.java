package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Estado;
/**
 *
 * @author roger
 */
public interface EstadoDAO extends DAO<Estado, Integer>{
}
