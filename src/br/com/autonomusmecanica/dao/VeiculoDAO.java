package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Veiculo;
/**
 *
 * @author roger
 */
public interface VeiculoDAO extends DAO<Veiculo, Integer>{
}
