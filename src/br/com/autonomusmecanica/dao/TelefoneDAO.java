package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Telefone;


/**
 *
 * @author roger
 */
public interface TelefoneDAO extends DAO<Telefone, Integer>{
}
