package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cidade;
import br.com.autonomusmecanica.entidades.Estado;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class CidadeDAOJPA extends DAOJPA<Cidade, Integer> implements CidadeDAO {

public List<Cidade> getExisteCidade(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Cidade.findByExiste");
        query.setParameter("nome", nome).
                setParameter("idCidade", id);
        return query.getResultList();
    }

 public List<Cidade> getCidadeEstado(Estado estado) {
        Query query = getEntityManager().createNamedQuery("Cidade.findByEstado");
        query.setParameter("estado", estado);
        return query.getResultList();
    }
}
