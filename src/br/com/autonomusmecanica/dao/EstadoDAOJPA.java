package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Estado;
import br.com.autonomusmecanica.entidades.Pais;
import br.com.autonomusmecanica.entidades.Usuario;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class EstadoDAOJPA extends DAOJPA<Estado, Integer> implements EstadoDAO {

public List<Estado> getExisteEstado(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Estado.findByExiste");
        query.setParameter("nome", nome).
                setParameter("idEstado", id);
        return query.getResultList();
    }

 public List<Estado> getEstadoPais(Pais pais) {
        Query query = getEntityManager().createNamedQuery("Estado.findByPais");
        query.setParameter("pais", pais);
        return query.getResultList();
    }

}
