package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Email;


/**
 *
 * @author roger
 */
public interface EmailDAO extends DAO<Email, Integer>{
}
