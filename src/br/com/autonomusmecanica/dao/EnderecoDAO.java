package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Endereco;


/**
 *
 * @author roger
 */
public interface EnderecoDAO extends DAO<Endereco, Integer>{
}
