package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Cor;
/**
 *
 * @author roger
 */
public interface CorDAO extends DAO<Cor, Integer>{
}
