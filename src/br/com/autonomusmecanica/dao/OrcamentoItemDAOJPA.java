package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Orcamento;
import br.com.autonomusmecanica.entidades.OrcamentoItem;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class OrcamentoItemDAOJPA extends DAOJPA<OrcamentoItem, Integer> implements OrcamentoItemDAO {

    public List<OrcamentoItem> getItemOrcamento(Orcamento orcamento) {
        Query query = getEntityManager().createNamedQuery("OrcamentoItem.findByOrcamento");
        query.setParameter("orcamento", orcamento);
        return query.getResultList();
    }  
}
