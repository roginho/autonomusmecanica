package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pais;
/**
 *
 * @author roger
 */
public interface PaisDAO extends DAO<Pais, Integer>{
}
