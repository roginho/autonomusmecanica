package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Fabricante;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class FabricanteDAOJPA extends DAOJPA<Fabricante, Integer> implements FabricanteDAO {

    public List<Fabricante> getExisteFabricante(String nome, Integer id) {
        Query query = getEntityManager().createNamedQuery("Fabricante.findByExiste");
        query.setParameter("nome", nome).
                setParameter("idFabricante", id);
        return query.getResultList();
    }
}
