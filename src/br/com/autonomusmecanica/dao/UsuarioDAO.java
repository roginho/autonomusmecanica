package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Usuario;

/**
 *
 * @author roger
 */
public interface UsuarioDAO extends DAO<Usuario, Integer>{
}
