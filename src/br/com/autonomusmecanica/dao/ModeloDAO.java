package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Modelo;
/**
 *
 * @author roger
 */
public interface ModeloDAO extends DAO<Modelo, Integer>{
}
