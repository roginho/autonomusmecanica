package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.FormaPgto;
/**
 *
 * @author roger
 */
public interface FormaPgtoDAO extends DAO<FormaPgto, Integer>{
}
