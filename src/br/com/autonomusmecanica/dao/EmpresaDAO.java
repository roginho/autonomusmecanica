package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Empresa;

/**
 *
 * @author roger
 */
public interface EmpresaDAO extends DAO<Empresa, Integer>{
}
