package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.Pessoa;
import br.com.autonomusmecanica.entidades.Telefone;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author roger
 */
public class TelefoneDAOJPA extends DAOJPA<Telefone, Integer> implements TelefoneDAO {

    public List<Telefone> getPessoaTelefone(Pessoa pessoa) {
        Query query = getEntityManager().createNamedQuery("Telefone.findByPessoaTelefone");
        query.setParameter("pessoa", pessoa);
        return query.getResultList();
    }
}
