package br.com.autonomusmecanica.dao;

import br.com.autonomusmecanica.entidades.PagamentoParcela;
/**
 *
 * @author roger
 */
public interface PagamentoParcelaDAO extends DAO<PagamentoParcela, Integer>{
}
