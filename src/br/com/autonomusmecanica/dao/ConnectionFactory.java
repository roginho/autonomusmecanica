package br.com.autonomusmecanica.dao;

import br.com.autonomusconfig.AutonomusConfig;
import br.com.roger.utils.Singleton;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ConnectionFactory {

    private String driver ="com.mysql.jdbc.Driver"; // driver de conex�o (java/mysql)  
    private String url = "jdbc:mysql://"; // endere�o de conex�o (driver + servidor + banco )  
    private String server = AutonomusConfig.getInstance().getArquivoConfiguracao().getConfiguracaoGeral().getIpServidor(); // servidor  
    private String bd =AutonomusConfig.getInstance().getArquivoConfiguracao().getConfiguracaoGeral().getNomeBanco(); // banco de dados  
    private String userName = "mecanica"; // usu�rio  
    private String password = "bobpai"; // senha  
    private Connection connection; // objeto para conex�o   
    private Integer returnCode;
    private String sistema = "";

    public Connection getConnection() throws SQLException {
        try {
            setDriver(driver);
            setURL(url);
            setUserName(userName);  //usuario
            setPassword(password);  //Senha
            setServer(server);
            setBd(bd);  //Nome Banco
            Class.forName(driver);
            url = url + "" + server + "/" + bd;
            connection = DriverManager.getConnection(url, userName, password);
        } catch (ClassNotFoundException erroClass) {
            mensagemErro("Nao foi possivel conectar ao banco de dados \n"
                    + "Driver: " + driver + "\n"
                    + "url: " + url + "\n"
                    + "User: " + userName + "\n"
                    + erroClass.getMessage());
        } catch (SQLException erroSQL) {
            mensagemErro("Nao foi possivel conectar ao banco de dados\n"
                    + "Driver: " + driver + "\n"
                    + "url: " + url + "\n"
                    + "User:" + userName + "\n"
                    + erroSQL.getMessage());
        }
        return connection = DriverManager.getConnection(url, userName, password);
    }

    public void setDriver(String valor) {
        driver = valor;
    }

    public void setURL(String valor) {
        url = valor;
    }

    public void setUserName(String valor) {
        userName = valor;
    }

    public void setPassword(String valor) {
        password = valor;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public void setServer(String server) {
        this.server = server;
    }

    private void mensagemErro(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    public boolean existeDados(String SQL) throws Exception {
        boolean ret = false;
        PreparedStatement smtm;
        smtm = this.connection.prepareStatement(SQL);
        ResultSet rs = smtm.executeQuery();
        if (rs.next()) {
            ret = true;
        } else {
            ret = false;
        }
        rs.close();
        smtm.close();
        return ret;
    }

    public Integer fazerBackUp() {
        JFileChooser jc = new JFileChooser("");
        jc.setDialogTitle("Fazer backup");
        jc.setAcceptAllFileFilterUsed(false);
        jc.addChoosableFileFilter(new FileNameExtensionFilter("SQL Script (*.sql)", "sql"));
        SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd_hh_mm_ss");
        Date minhaDate = new Date();
        String arquivo = formatador.format(minhaDate);
        File nome = new File("BackUp_" + arquivo);
        jc.setSelectedFile(nome);
        int result = jc.showSaveDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            arquivo = jc.getSelectedFile().toString().concat(".sql");
            File file = new File(arquivo);
            if (!file.exists()) {
                executaBackUp(arquivo);
            } else {
                int opcao = JOptionPane.showConfirmDialog(null, "Este arquivo já existe. Quer sobreescrever este arquivo? Atenção!!!");
                if (opcao == JOptionPane.YES_NO_OPTION) {
                    executaBackUp(arquivo);
                }
            }
        }
        return returnCode;
    }

    public void executaBackUp(String nomeDb) {
        String comando = null;
        try {
            long antes = System.currentTimeMillis();
            if (sistema.equalsIgnoreCase("linux")) {
                comando = "mysqldump -u " + userName + " -p" + password + " --database " + bd + " -r " + nomeDb + "";
            } else {
                String path = Singleton.getInstance().caminhoMysqldump();
                comando = path+" -h" + server + " -u " + userName + " -p" + password + " --databases " + bd + " -r " + nomeDb + "";
                System.out.println(comando);
            }
            System.out.println(comando);
            Process process = Runtime.getRuntime().exec(comando);
            returnCode = process.waitFor();
            System.out.println("codigo wait for " + returnCode);
            if (returnCode == 0) {
                double tempo = (System.currentTimeMillis() - antes) / 1000.0;
                JOptionPane.showMessageDialog(null, String.format("O Backup foi gerado com sucesso em %.2f segundos!", tempo));
            } else {
                JOptionPane.showMessageDialog(null, "Atenção o backup não foi gerado !!!");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Babckup não realizado:\n\n" + ex, "Backup", JOptionPane.ERROR_MESSAGE);
        }
    }
}
